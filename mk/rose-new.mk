# Filename: rose-new.mk
# Date: March 15, 2014
# Updated: June 18, 2014
# Author: Mohammed Sourouri (mohamso@simula.no) 
# Comment: A basic Makefile to build the Panda Translator

# OS name
OSUPPER = $(shell uname -s 2>/dev/null | tr [:lower:] [:upper:])
OSLOWER = $(shell uname -s 2>/dev/null | tr [:upper:] [:lower:])

# Detect OS
DARWIN = $(strip $(findstring DARWIN, $(OSUPPER)))

ifneq ($(DARWIN),)
	echo "Panda is not supported on Mac OS X"
endif	

#Location of the ROSE installation and source  directory
ROSE_INSTALL_DIR = /home/mohamso/rose
ROSE_SRC_DIR = /home/mohamso/Downloads/rose-0.9.5a-19276/src

# Location of include directory after "make install"
ROSE_INCLUDE_DIR = -I$(ROSE_INSTALL_DIR)/include

# Location of Boost Install Dir 
BOOST_INSTALL_DIR = /usr/local/lib/boost

# Location of Libtool
LIB_TOOL = /usr/bin/libtool
#LIB_TOOL = $(ROSE_INSTALL_DIR)/libtool

# Location of Boost include directory
BOOST_INCLUDES =  $(BOOST_INSTALL_DIR)/include
JAVA_INCLUDE   = -I/usr/lib/jdk1.7.0_51/include -I/usr/lib/jdk1.7.0_51/include/linux

# Location of Dwarf include and lib (if ROSE is configured to use Dwarf)
ROSE_DWARF_INCLUDES = 
ROSE_DWARF_LIBS_WITH_PATH = 
ROSE_INCLUDE_DIR += $(ROSE_DWARF_INCLUDES)
ROSE_LIBS += $(ROSE_DWARF_LIBS_WITH_PATH)

CC                    = gcc
CXX                   = g++
INCLUDE               += $(BOOST_INCLUDES) $(JAVA_INCLUDE) $(ROSE_INCLUDE_DIR) $(STDDEF_INCLUDE)
CXXFLAGS              += -fopenmp -pthread -fno-strict-aliasing
LDFLAGS               =  -fopenmp -pthread 

# Location of library directory after "make install"
ROSE_LIB_DIR = $(ROSE_INSTALL_DIR)/lib
ROSE_LIBS = $(ROSE_LIB_DIR)/librose.la
ROSE_LIBS_so = $(ROSE_LIB_DIR)/librose.so

