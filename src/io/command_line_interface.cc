#include "command_line_interface.h"
#include "../system/diagnostics/version.h"

namespace IO
{
translator::TranslatorMode* CommandLineInterface::GetTranslatorMode(int argc, char* argv[]) {

  translator::TranslatorMode* mode = new translator::TranslatorMode();

  for (int i = 1; i < argc; i++) {
    if ((strcmp(argv[i], "--gpu") == 0)) {
      mode->set_gpu_mode(true);
      break;
    }

    if ((strcmp(argv[i], "--hybrid") == 0)) {
      mode->set_hybrid_mode(true);
      break;
    }

    if ((strcmp(argv[i], "--mpi") == 0)) {
      mode->set_mpi_mode(true);
      break;
    }
  }

  return mode;
}

void CommandLineInterface::StartOrDie(int argc, char* argv[])
{
  if (argc <= 1) {
    printf("%s: no input file specified; use -Help (-h) for more information.\n", argv[0]);
    exit(1);
  }

  for (int i = 1; i < argc; i++) {
    if ((strcmp(argv[i], "--Help") == 0) || (strcmp(argv[i], "-h") == 0)) {
      HelpOrDie(argv);
    }

    if ((strcmp(argv[i], "--version") == 0) || (strcmp(argv[i], "-V") == 0)) {
      VersionOrDie(argv);
    }
  }
}

void CommandLineInterface::VersionOrDie(char* argv[])
{
  std::cout << "Panda v" << PANDA_MAJOR_VERSION << "." << PANDA_MINOR_VERSION << "." << PANDA_MICRO_VERSION << \
            " build (" << BUILD_NUM << "." << BUILD_DATE << ")" << std::endl;
  exit(1);
}

void CommandLineInterface::HelpOrDie(char* argv[])
{
  std::cout << "Usage: ./panda [options] <inputfile>" << std::endl;
  std::cout << "Generic options" << std::endl;
  std::cout << "===============" << std::endl;
  std::cout << "--Help (-h)" << std::endl;
  std::cout << "\tPrints this help information" << std::endl;
  std::cout << "--version (-V)" << std::endl;
  std::cout << "\tPrint version information" << std::endl;

  exit(1);
}

} //namespace IO
