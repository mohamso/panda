#ifndef PANDA_IO_CLI_H
#define PANDA_IO_CLI_H

#include "../panda.h"
#include "../translator/modes.h"

namespace IO {

class CommandLineInterface {
public:

  CommandLineInterface() {}

  // Functions
  void StartOrDie(int argc, char* argv[]);
  void HelpOrDie(char* argv[]);
  void VersionOrDie(char* argv[]);
  translator::TranslatorMode* GetTranslatorMode(int argc, char* argv[]);

};

} //namespace IO


#endif //PANDA_IO_CLI_H
