#include "filemanager.h"

#include <boost/filesystem.hpp>

namespace IO
{
std::string FileManager::CanonizeInputFilename(std::string& filename_with_full_path) {
  std::string canonical_filename;
  std::string filename_suffix = StringUtility::fileNameSuffix(filename_with_full_path);

  if (CommandlineProcessing::isCFileNameSuffix(filename_suffix)) {
    std::string filename_without_full_path = StringUtility::stripPathFromFileName(filename_with_full_path);
    canonical_filename = StringUtility::stripFileSuffixFromFileName(filename_without_full_path);
  }
  else
  {
    std::cerr << "Panda error: Input source file is not a C file. Please use a C file and try again. " << std::endl;
    ROSE_ABORT();
  }

  return canonical_filename;
}

void FileManager::ValidateInputSourceFileOrDie(std::string& input_source_file) {
  std::string filename_suffix = StringUtility::fileNameSuffix(input_source_file);

  if (CommandlineProcessing::isCFileNameSuffix(filename_suffix) == false) {
    std::cerr << "Panda error: File not recognized. Please use a valid C file and try again." << std::endl;
    exit(1);
  }
}

void FileManager::SetTranslatedFilename(SgFile* current_file) {
  current_file->set_unparse_output_filename(this->output_filename());
}

void FileManager::DeleteFiles() {
  boost::filesystem::directory_iterator iterator(std::string("."));

  for (; iterator != boost::filesystem::directory_iterator(); ++iterator) {
    if (iterator->path().extension() == ".dat") {
      boost::filesystem::remove(iterator->path().filename().string());
    }
  }
}

} // namespace IO
