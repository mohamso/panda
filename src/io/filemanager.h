#ifndef PANDA_IO_FILEMANAGER_H_
#define PANDA_IO_FILEMANAGER_H_

#include "../panda.h"

namespace IO {

class FileManager {
public:
  FileManager() : output_filename_prefix_("panda_"), output_filename_postfix_(".c") {}

  std::string output_filename() const { return output_filename_; }
  std::string output_filename_prefix() const { return output_filename_prefix_; }
  std::string output_filename_postfix() const { return output_filename_postfix_; }

  void set_output_filename(const std::string& output_filename) { output_filename_ = output_filename_prefix_ + output_filename + output_filename_postfix_; }
  void set_output_filename_prefix(const std::string& output_filename_prefix) { output_filename_prefix_ = output_filename_prefix; }
  void set_output_filename_postfix(const std::string& output_filename_postfix) { output_filename_postfix_ = output_filename_postfix; }

  std::string CanonizeInputFilename(std::string& input_source_file);
  void ValidateInputSourceFileOrDie(std::string& input_source_file);
  void SetTranslatedFilename(SgFile* current_file);
  void DeleteFiles();

private:
  std::string output_filename_;
  std::string output_filename_prefix_;
  std::string output_filename_postfix_;

};

}  // namespace IO

#endif //PANDA_IO_FILEMANAGER_H_
