#ifndef PANDA_REDUCTION_REDUCTION_H
#define PANDA_REDUCTION_REDUCTION_H

#include "../panda.h"
#include "../loop/loop_manager.h"
#include "../domain/domain_manager.h"
#include "../translator/pragma_manager.h"

#include "../mpi/mpi_datatype.h"
#include "../mpi/mpi_op.h"
#include "../mpi/mpi_sum.h"
#include "../mpi/mpi_min.h"
#include "../mpi/mpi_prod.h"
#include "../mpi/mpi_allreduce.h"

#include <boost/regex.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>

namespace reduction
{
class Reduction {
public:
  Reduction();

  // Getter
  std::vector<std::string> const &operators() const { return operators_; }
  std::vector<std::string> const &list() const { return list_; }
  const std::string &reduction_operator() const { return operator_; }
  std::vector<SgVariableDeclaration*> const &global_variables() const { return global_variables_; }
  std::map<std::string, std::string> const &local_to_global_list_map() const { return local_to_global_list_map_; }
  std::map<std::string, mpi::MPIDatatype*> const &type_map() const { return type_map_; }
  mpi::MPIOp* const &mpi_op() const { return mpi_op_; }

  // Setter
  void set_operator(const std::string &value) { operator_ = value; }
  void set_mpi_op(mpi::MPIOp* value) { mpi_op_ = value; }
  void set_list(const std::vector<std::string> &value) { list_ = value; }
  void set_global_variables(const std::vector<SgVariableDeclaration*> &value) { global_variables_ = value; }
  void set_local_to_global_list_map(const std::map<std::string, std::string> &value) { local_to_global_list_map_ = value; }
  void set_type_map(const std::map<std::string, mpi::MPIDatatype*> &value) { type_map_ = value; }

  // Functions
  void ExtractOperatorOrDie(std::vector<std::string> reduction_clauses);
  void ExtractListOrDie(std::vector<std::string> reduction_clauses);
  void VerifyExtractedVariablesOrDie(SgProject* project);
  void VerifyPresenceOfExtractedVariablesOrDie(loop::LoopManager* loop_manager, translator::PragmaManager* pragma_manager);

  void PerformReductionTransformations(SgProject* project, loop::LoopManager* loop_manager, domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager);
  std::vector<SgStatement*> CreateReduceStatementOrDie(SgProject* project);

  ~Reduction() {}

private:
  bool VerifyExtractedOperatorOrDie(const std::string& operator_candidate);
  SgVariableDeclaration* CreateGlobalListVariable(const std::string& variable_name, SgType* type);
  std::map<std::string, std::string> MapLocalToGlobalListVariables(SgProject* project);
  mpi::MPIOp* MapOperatorToMPIOp(const std::string& operator_type);
  mpi::MPIDatatype* MapListVariableTypeToMPIDatatype(SgType* type);

  std::string operator_;
  std::vector<std::string> operators_;
  std::vector<std::string> list_;
  std::vector<SgVariableDeclaration*> global_variables_;
  mpi::MPIOp* mpi_op_;
  std::map<std::string, mpi::MPIDatatype*> type_map_;
  std::map<std::string, std::string> local_to_global_list_map_;

};

} // namespace

#endif // PANDA_REDUCTION_REDUCTION_H

