#include "reduction.h"

namespace reduction
{

/**
 * @brief Constructor that initializes the different supported
 * reduction operators.
 */
Reduction::Reduction() {
  this->operators_.push_back("+");
  this->operators_.push_back("-");
  this->operators_.push_back("*");
  this->operators_.push_back("/");
}

/**
 * @brief A function that extracts the reduction operator specified
 * in the reduction clause.
 *
 * @param[in] reduction_clauses Arguments specified in the reduction clause
 */
void Reduction::ExtractOperatorOrDie(std::vector<std::string> reduction_clauses) {

  for (auto i = reduction_clauses.begin(); i != reduction_clauses.end(); i++) {
    if (!(*i).empty()) {
      std::string op_candidate(1, (*i)[0]);
      this->VerifyExtractedOperatorOrDie(op_candidate);
    } else {
      std::cerr << "Panda: Erroneous reduction operator specified." << std::endl;
      std::cerr << "Panda error: Translation will abort." << std::endl;
      exit(1);
    }
  }
}

/**
 * @brief A function that verifies if the extracted operator is supported.
 * If a supported operator type is found, it is mapped to a corresponding
 * MPI_Op type.
 *
 * @param[in] operator_candidate  Possible reduction operator
 */
bool Reduction::VerifyExtractedOperatorOrDie(const std::string& operator_candidate) {

  bool IsSupportedOperator = (std::find(this->operators().begin(), this->operators().end(), operator_candidate) != this->operators().end());

  if (IsSupportedOperator) {
    this->set_operator(operator_candidate);
    this->set_mpi_op(this->MapOperatorToMPIOp(operator_candidate));
    return true;
  } else {
    std::cerr << "Panda: Could not find a supported reduction operator" << std::endl;
    std::cerr << "Panda error: Translation will abort." << std::endl;
    exit(1);
  }
}

/**
 * @brief A function that extracts the variables listed in the reduction
 * clause.
 *
 * @param[in] reduction_clauses Arguments specified in the reduction clause
 */
void Reduction::ExtractListOrDie(std::vector<std::string> reduction_clauses) {

  std::vector<std::string> list;

  for (auto i = reduction_clauses.begin(); i != reduction_clauses.end(); i++) {
    boost::regex expr{"[^\\:](?=\\w).*"};
    boost::smatch string_match;

    if (boost::regex_search((*i), string_match, expr)) {
      std::string variables = string_match[0];
      boost::split(list, variables, boost::is_any_of(", "), boost::token_compress_on);
    } else {
      std::cerr << "Panda: Invalid list specified." << std::endl;
      std::cerr << "Panda error: Translation will abort." << std::endl;
      exit(1);
    }
  }

  this->set_list(list);
}

/**
 * @brief  A function that verifies that the extracted list
 * maps to variables that exist in the original source code.
 *
 * Translation will abort otherwise.
 *
 * @param[in] project ROSE Project Object
 */
void Reduction::VerifyExtractedVariablesOrDie(SgProject* project) {

  Rose_STL_Container<SgNode*> global_var_decls = NodeQuery::querySubTree(project, V_SgVariableDeclaration);
  std::vector<std::string> global_var_decls_as_string;

  for (auto i = global_var_decls.begin(); i != global_var_decls.end(); i++) {
    global_var_decls_as_string.push_back(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString());
  }

  for (auto i = this->list().begin(); i != this->list().end(); i++) {

    bool IsPresent = (std::find(global_var_decls_as_string.begin(), global_var_decls_as_string.end(), (*i)) != global_var_decls_as_string.end());

    if (!IsPresent) {
      std::cerr << "Panda: Variable(s) listed in the reduction clause do not exist." << std::endl;
      std::cerr << "Panda error: Translation will abort." << std::endl;
      exit(1);
    }
  }
}

/**
* @brief  A function that verifies that the extracted list
* variables exist in the target region.
*
* Translation will abort otherwise.
*
* @param[in]  loop_manager  Object to manipulate loops
* @param[in]  pragma_manager  Object to manipulate directives
*/
void Reduction::VerifyPresenceOfExtractedVariablesOrDie(loop::LoopManager* loop_manager, translator::PragmaManager* pragma_manager) {
  std::vector<SgForStatement*> for_loop_list = loop_manager->CollectLoopStatements(pragma_manager->pragma_declaration_list(), pragma_manager->reduction());

  bool IsPresent = false;
  unsigned int count = 0;

  for (auto i = for_loop_list.begin(); i != for_loop_list.end(); i++) {
    if (loop_manager->IsInnerMostLoop(isSgForStatement(*i))) {
      Rose_STL_Container<SgNode*> local_var_decls = NodeQuery::querySubTree(isSgForStatement(*i), V_SgVarRefExp);
      for (auto j = local_var_decls.begin(); j != local_var_decls.end(); j++) {
        IsPresent = (std::find(this->list().begin(), this->list().end(), isSgVarRefExp(*j)->unparseToString()) != this->list().end());

        if (IsPresent) {
          count++;
        }
      }
    }
  }

  if (this->list().size() != count) {
    std::cerr << "Panda: Variable(s) listed in the reduction not present in the annotated region." << std::endl;
    std::cerr << "Panda error: Translation will abort." << std::endl;
    exit(1);
  }
}

/**
* @brief  The function responsible for performing the actual reduction
* transformations. It depends on several other functions to realize
* the actual transformation.
*
* The transformation process consist of the following steps:
*
* 1. Transform the loop iterators
* 2. Find and replace references to the global domain sizes with
*    corresponding subdomain sizes inside the inner most loop.
* 3. At the end of the outer most loop, an MPI_Allreduce function call
*    is generated.
*
* @param[in]  loop_manager  Object to manipulate loops
* @param[in]  pragma_manager  Object to manipulate directives
*/
void Reduction::PerformReductionTransformations(SgProject* project, loop::LoopManager* loop_manager, domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager) {

  std::cout << "Panda: Generating MPI_Allreduce function call...";
  std::vector<SgForStatement*> for_loops = loop_manager->CollectLoopStatements(pragma_manager->pragma_declaration_list(), pragma_manager->reduction());

  loop_manager->loop_transformer()->TransformLoopIteratorsOrDie(for_loops, decomposer->subdomain_size()->global_to_local_size_map());

  for (auto i = for_loops.begin(); i != for_loops.end(); i++) {
    if (loop_manager->IsInnerMostLoop(isSgForStatement(*i))) {
      if (isSgForStatement(*i)->get_loop_body() != NULL) {
        loop_manager->loop_transformer()->TransformLoopBody(isSgForStatement(*i)->get_loop_body(), decomposer->subdomain_size()->global_to_local_size_map());
      }
    }
  }

  std::vector<SgStatement*> mpi_allreduce_stmt = this->CreateReduceStatementOrDie(project);

  if (mpi_allreduce_stmt.size() > 0) {
    for (auto i = this->global_variables().begin(), j = mpi_allreduce_stmt.rbegin(); i != this->global_variables().end(), j != mpi_allreduce_stmt.rend(); i++, j++) {
      SageInterface::insertStatementAfter(isSgForStatement(for_loops.front()), isSgVariableDeclaration(*i));
      SageInterface::insertStatementAfter(isSgVariableDeclaration(*i), isSgStatement(*j));
    }
  } else {
    std::cerr << "Panda: Unable to generate MPI_Allreduce function call." << std::endl;
    std::cerr << "Panda error: Translation will abort." << std::endl;
    exit(1);
  }

  std::cout << "done" << std::endl;
}

/**
* @brief  A function that attempts to create a reduction statement.
*
* First a local to global mapping for the listed variables are created.
* Next, the mappings are used to generate the MPI_Allreduce function call.
*
* The translation will abort otherwise if the attempts are unsuccessful.
*
* @param[in]  project  ROSE document project
*/
std::vector<SgStatement*> Reduction::CreateReduceStatementOrDie(SgProject* project) {

  this->set_local_to_global_list_map(this->MapLocalToGlobalListVariables(project));

  mpi::MPIAllreduce* mpi_allreduce = new mpi::MPIAllreduce();

  std::vector<SgStatement*> mpi_allreduce_stmts;

  for (auto i = this->local_to_global_list_map().begin(); i != this->local_to_global_list_map().end(); i++) {

    auto p = this->type_map().find(i->first);

    if (p != this->type_map().end()) {
      mpi_allreduce_stmts.push_back(mpi_allreduce->CreateReduceStatement(i->first, i->second, this->mpi_op(), p->second));
    } else {
      std::cerr << "Panda: Unable to locate MPI_Datatype for MPI_Allreduce." << std::endl;
      std::cerr << "Panda error: Translation will abort." << std::endl;
      exit(1);
    }
  }

  return mpi_allreduce_stmts;
}

/**
* @brief  A function that generates global variables that are later
* mapped to the local listed variables. Moreover, the function also
* maps the data type of the local variables with a corresponding
* MPI_Datatype that is needed when constructing an MPIAllreduce function call.
*
* @param[in]  project  ROSE document project
*/
std::map<std::string, std::string> Reduction::MapLocalToGlobalListVariables(SgProject* project) {

  Rose_STL_Container<SgNode*> global_var_decls = NodeQuery::querySubTree(project, V_SgVariableDeclaration);
  std::vector<SgVariableDeclaration*> global_list_vars;
  std::map<std::string, std::string> global_to_local_list_var_map;
  std::map<std::string, mpi::MPIDatatype*> type_map;

  for (auto i = global_var_decls.begin(); i != global_var_decls.end(); i++) {
    std::string global_variable_name = isSgVariableDeclaration(*i)->get_variables().front()->unparseToString();

    bool IsPresent = (std::find(this->list().begin(), this->list().end(), isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()) != this->list().end());

    if (IsPresent) {
      SgVariableDeclaration* global_variable = this->CreateGlobalListVariable(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString(), isSgVariableDeclaration(*i)->get_variables().front()->get_type());
      global_list_vars.push_back(global_variable);
      type_map[isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()] = this->MapListVariableTypeToMPIDatatype(isSgVariableDeclaration(*i)->get_variables().front()->get_type());
      global_to_local_list_var_map[isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()] = global_variable->get_variables().front()->unparseToString();
    }
  }

  this->set_type_map(type_map);
  this->set_global_variables(global_list_vars);

  return global_to_local_list_var_map;
}

/**
* @brief  A function that maps an SgType to a MPI_Datatype.
*
* @param[in]  type  Data type for the local list variable
*/
mpi::MPIDatatype* Reduction::MapListVariableTypeToMPIDatatype(SgType* type) {

  mpi::MPIDatatype* mpi_type;

  if (type->variantT() == V_SgTypeDouble) {
    mpi_type = new mpi::MPIDouble();
  }

  if (type->variantT() == V_SgTypeFloat) {
    mpi_type = new mpi::MPIFloat();
  }

  return mpi_type;
}

/**
* @brief  A function that maps an SgType to a MPI_Datatype.
*
* @param[in]  type  Data type for the local list variable
* @param[out] mpi_op  An MPI_OP object
*/
mpi::MPIOp* Reduction::MapOperatorToMPIOp(const std::string& operator_type) {

  mpi::MPIOp* mpi_op;

  if (operator_type.compare("+") == 0) {
    mpi_op = new mpi::MPISum();
  }

  if (operator_type.compare("-") == 0) {
    mpi_op = new mpi::MPIMin();
  }

  if (operator_type.compare("*") == 0) {
    mpi_op = new mpi::MPIProd();
  }

  return mpi_op;
}

/**
* @brief  A function that builds a global variable declaration.
*
* @param[in]  variable_name  Data type for the local list variable
* @param[in]  type  Data type that matches
* @param[out] global_variable The created variable is returned.
*/
SgVariableDeclaration* Reduction::CreateGlobalListVariable(const std::string& variable_name, SgType* type) {
  return SageBuilder::buildVariableDeclaration("global_" + variable_name, type, SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0)));
}

} // namespace
