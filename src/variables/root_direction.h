#ifndef PANDA_VARIABLES_ROOTDIRECTION_H
#define PANDA_VARIABLES_ROOTDIRECTION_H

#include "../panda.h"
#include "rank.h"
#include "p.h"

namespace variables
{
  class RootDirection {
  public:
    virtual std::string name() const = 0;
    virtual P* p() const { return p_; }
    virtual SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) = 0;

    void set_p(P* p) { p_ = p; }

    virtual bool IsEast() const { return false; }
    virtual bool IsWest() const { return false; }
    virtual bool IsNorth() const { return false; }
    virtual bool IsSouth() const { return false; }
    virtual bool IsFront() const { return false; }
    virtual bool IsBack() const { return false; }
    virtual bool IsTop() const { return false; }
    virtual bool IsBottom() const { return false; }

    virtual std::string merged_direction_name() const { return merged_name_; }
    virtual SgVariableDeclaration* plane_one() const { return plane_one_; }
    virtual SgVariableDeclaration* plane_two() const { return plane_two_; }
    virtual SgVariableDeclaration* boundary_direction() const { return boundary_dir_; }

    virtual void set_plane_one(SgVariableDeclaration* value) { plane_one_ = value; }
    virtual void set_plane_two(SgVariableDeclaration* value) { plane_two_ = value; }
    virtual void set_boundary_direction(SgVariableDeclaration* value) { boundary_dir_ = value; }
    virtual void set_merged_name(const std::string& value) { merged_name_ = value; }

    virtual ~RootDirection();

  protected:
    P* p_;
    SgVariableDeclaration* plane_one_;
    SgVariableDeclaration* plane_two_;
    SgVariableDeclaration* boundary_dir_;
    std::string merged_name_;

  };

  class EastDirection: public RootDirection {
  public:
	EastDirection() {this->set_p(new P(0)); }

    std::string name() const { return "east"; }

    bool IsEast() const { return true; }

    SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);

    ~EastDirection() {}

  };

  class WestDirection: public RootDirection {
    public:
	  WestDirection() { this->set_p(new P(1));}

	  std::string name() const { return "west"; }

	  bool IsWest() const { return true; }

      SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);

      ~WestDirection() {}
  };

  class NorthDirection: public RootDirection {
    public:
	  NorthDirection() { this->set_p(new P(0)); }

	  std::string name() const { return "north"; }

	  bool IsNorth() const { return true; }

      SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);

      ~NorthDirection() {}
  };

  class SouthDirection: public RootDirection {
    public:
	  SouthDirection() { this->set_p(new P(1));}

	  std::string name() const { return "south"; }

	  bool IsSouth() const { return true; }

      SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);

      ~SouthDirection() {}
  };

  class TopDirection: public RootDirection {
    public:
	  TopDirection() { this->set_p(new P(0)); }

	  std::string name() const { return "top"; }

	  bool IsTop() const { return true; }

      SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);

      ~TopDirection() {}
  };

  class BottomDirection: public RootDirection {
    public:
	  BottomDirection() { this->set_p(new P(1)); }

	  std::string name() const { return "bottom"; }

	  bool IsBottom() const { return true; }

      SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);
      
      ~BottomDirection() {}
  };

  class FrontDirection: public RootDirection {
    public:
	  FrontDirection() { this->set_p(new P(0)); }

	  std::string name() const { return "front"; }

	  bool IsFront() const { return true; }

      SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);

      ~FrontDirection() {}
  };

  class BackDirection: public RootDirection {
    public:
	  BackDirection() { this->set_p(new P(1)); }

	  std::string name() const { return "back"; }

	  bool IsBack() const { return true; }

      SgExprStatement* BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables);

      ~BackDirection() {}
  };

} // namespace variables

#endif //PANDA_VARIABLES_ROOTDIRECTION_H
