#include "index.h"

namespace variables
{

SgVariableDeclaration* Index::CreateIndex2DVariable() {
  return SageBuilder::buildVariableDeclaration("index2D", SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0)));
}

SgVariableDeclaration* Index::CreateParallelIndex2DVariable(RootDirection* direction) {

	SgVariableDeclaration* parallel_index_variable;

	// int index2D = (j-1)*nsdx + i - 1; // front-back
	/*if (direction->IsFront() || direction->IsBack()) {
	}*/

	// int index2D = (k-1)*nsdx + i - 1; // north-south
	if (direction->IsNorth() || direction->IsSouth()) {
		auto i_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp("i"), SageBuilder::buildIntVal(1));
		auto k_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(("k")), SageBuilder::buildIntVal(1));
		auto k_minus_mult_nsdx = SageBuilder::buildMultiplyOp(k_minus_one, SageBuilder::buildVarRefExp(direction->plane_one()));
		parallel_index_variable = SageBuilder::buildVariableDeclaration("index2D", SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildAddOp(k_minus_mult_nsdx, i_minus_one)));
	}

	// int index2D = (k-1)*nsdy + j - 1; // east-west
	if (direction->IsEast() || direction->IsWest()) {
		auto j_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp("j"), SageBuilder::buildIntVal(1));
		auto k_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(("k")), SageBuilder::buildIntVal(1));
		auto k_minus_mult_nsdy = SageBuilder::buildMultiplyOp(k_minus_one, SageBuilder::buildVarRefExp(direction->plane_one()));
		parallel_index_variable = SageBuilder::buildVariableDeclaration("index2D", SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildAddOp(k_minus_mult_nsdy, j_minus_one)));
	}

	return parallel_index_variable;
}

SgVariableDeclaration* Index::CreateIndex3DVariable(const std::string& variable_name, SgVariableDeclaration* joff_variable, SgVariableDeclaration* koff_variable) {

  // Creates i + j*j_off
  SgExpression* ij_expression = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(SgName("i")),
                                SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(SgName("j")), SageBuilder::buildVarRefExp(joff_variable)));

  // Creates k*k_off
  SgExpression* k_koff_expression = SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(SgName("k")), SageBuilder::buildVarRefExp(koff_variable));

  SgExpression* assembled_expression = SageBuilder::buildAddOp(ij_expression, k_koff_expression);

  // Assigns int index3D = i + j*j_off + k*k_off;
  return SageBuilder::buildVariableDeclaration(variable_name, SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(assembled_expression));
}

SgVariableDeclaration* Index::GenerateJoffVariables(domain::Decomposer* decomposer) {

  std::vector<SgVariableDeclaration*> subdomain_variables = decomposer->subdomain_size()->variables();

  SgVariableDeclaration* j_off;
  for (auto i = subdomain_variables.begin(); i != subdomain_variables.end(); i++) {
    if (isSgVariableDeclaration(*i)->get_variables().front()->unparseToString() == decomposer->subdomain_size()->nsdx()) {
      j_off = this->CreateJoffVariable(isSgVariableDeclaration(*i));
    }
  }

  subdomain_variables.clear();

  return j_off;
}

SgVariableDeclaration* Index::GenerateKoffVariable(domain::Decomposer* decomposer) {

  std::vector<SgVariableDeclaration*> subdomain_variables = decomposer->subdomain_size()->variables();

  SgVariableDeclaration* k_off;
  for (auto i = subdomain_variables.begin(); i != subdomain_variables.end(); i++) {
    if (isSgVariableDeclaration(*i)->get_variables().front()->unparseToString() == decomposer->subdomain_size()->nsdy()) {
      k_off = this->CreateKoffVariable(isSgVariableDeclaration(*i));
    }
  }

  subdomain_variables.clear();

  return k_off;
}

SgVariableDeclaration* Index::CreateJoffVariable(SgVariableDeclaration* nsdx_variable) {
  SgExpression* initial_expression = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(nsdx_variable), SageBuilder::buildIntVal(2));
  return SageBuilder::buildVariableDeclaration("j_off", SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(initial_expression));
}

SgVariableDeclaration* Index::CreateKoffVariable(SgVariableDeclaration* nsdy_variable) {
  SgExpression* initial_expression = SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(this->get_joff()),
                                     SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(nsdy_variable), SageBuilder::buildIntVal(2)));
  return SageBuilder::buildVariableDeclaration("k_off", SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(initial_expression));
}


} // namespace
