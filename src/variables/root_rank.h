#ifndef PANDA_VARIABLES_ROOTRANK_H
#define PANDA_VARIABLES_ROOTRANK_H

#include "../panda.h"

namespace variables
{
  class RootRank {
  public:
    virtual std::string name() const = 0;
    virtual SgVariableDeclaration* CreateRankMappingVariable(SgVariableDeclaration* rank_variable, std::vector<SgVariableDeclaration*> p_variable_list) = 0;
    virtual ~RootRank();
  };

  class RankX : public RootRank {
  public:

    std::string name() const;

    SgVariableDeclaration* CreateRankMappingVariable(SgVariableDeclaration* rank_variable, std::vector<SgVariableDeclaration*> p_variable_list);

    ~RankX() {}
  };

  class RankY : public RootRank {
  public:
    std::string name() const;
    
    SgVariableDeclaration* CreateRankMappingVariable(SgVariableDeclaration* rank_variable, std::vector<SgVariableDeclaration*> p_variable_list);

    ~RankY() {}
  };

  class RankZ : public RootRank {
  public:
    std::string name() const;

    SgVariableDeclaration* CreateRankMappingVariable(SgVariableDeclaration* rank_variable, std::vector<SgVariableDeclaration*> p_variable_list);
    
    ~RankZ() {}
  };

  } // namespace variables

#endif // PANDA_VARIABLES_ROOTRANK_H
