#ifndef PANDA_VARIABLES_INDEX_H_
#define PANDA_VARIABLES_INDEX_H_

#include "../panda.h"
#include "../domain/decomposer.h"
#include "root_direction.h"

namespace variables
{
class Index {
public:

  SgVariableDeclaration* get_index3D() const { return index3D_; }
  SgVariableDeclaration* get_index2D() const { return index2D_; }

  SgVariableDeclaration* get_joff() const { return j_off_; }
  SgVariableDeclaration* get_koff() const { return k_off_; }

  void set_index2D(SgVariableDeclaration* value) { index2D_ = value; }
  void set_index3D(SgVariableDeclaration* value) { index3D_ = value; }

  void set_joff(SgVariableDeclaration* value) { j_off_ = value; }
  void set_koff(SgVariableDeclaration* value) { k_off_ = value; }

  // Functions
  SgVariableDeclaration* CreateIndex2DVariable();
  SgVariableDeclaration* CreateParallelIndex2DVariable(RootDirection* direction);
  SgVariableDeclaration* CreateIndex3DVariable(const std::string& variable_name, SgVariableDeclaration* joff_variable, SgVariableDeclaration* koff_variable);

  SgVariableDeclaration* GenerateJoffVariables(domain::Decomposer* decomposer);
  SgVariableDeclaration* GenerateKoffVariable(domain::Decomposer* decomposer);

  ~Index() {}

private:

  SgVariableDeclaration* CreateJoffVariable(SgVariableDeclaration* nsdx_variable);
  SgVariableDeclaration* CreateKoffVariable(SgVariableDeclaration* nsdy_variable);

  SgVariableDeclaration* j_off_;
  SgVariableDeclaration* k_off_;
  SgVariableDeclaration* index3D_;
  SgVariableDeclaration* index2D_;
};

} // namespace variables

#endif // PANDA_VARIABLES_INDEX_H_

