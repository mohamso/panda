#ifndef PANDA_VARIABLES_RANK_H
#define PANDA_VARIABLES_RANK_H

#include "../panda.h"
#include "root_rank.h"

namespace variables
{
  class Rank : public RankX, RankY, RankZ {
  public:

  Rank() {
    this->rank_list_.push_back(new RankX());
    this->rank_list_.push_back(new RankY());
    this->rank_list_.push_back(new RankZ());
  }
  
  // Getter 
  std::vector<RootRank*> const ranks() const { return rank_list_; }
  std::vector<SgVariableDeclaration*> const rank_mapping_variables() const { return rank_mapping_list_; }
  SgVariableDeclaration* variable() const { return variable_; }
  std::string name() const;

  // Setter
  void set_rank_mapping_variables(std::vector<SgVariableDeclaration*> const rank_mapping_list) { rank_mapping_list_ = rank_mapping_list; }
  void set_rank(std::vector<RootRank*> const rank_list) { rank_list_ = rank_list; }
  void set_variable(SgVariableDeclaration* variable) { variable_ = variable; }

  // Functions
  SgVariableDeclaration* CreateRankVariable();

  std::vector<SgVariableDeclaration*> CollectRankMappingVariables(std::vector<SgVariableDeclaration*> p_variable_list);

  ~Rank() {}

  private:
  std::vector<RootRank*> rank_list_;
  std::vector<SgVariableDeclaration*> rank_mapping_list_;
  SgVariableDeclaration* variable_;
  std::string name_;
};

} // namespace variables

#endif // PANDA_VARIABLES_RANK_H
