#ifndef PANDA_VARIABLES_P_H_
#define PANDA_VARIABLES_P_H_

#include "../panda.h"

namespace variables
{
  class P {
  public:

    P(int value) {
    	this->value_ = 0;
    	this->set_value(value);
    }

    std::string name() const { return "p"; }
    int value() const { return value_; }

    void set_value(int value) { value_ = value; }

    ~P();

  private:
    int value_;
};

} // namespace variables

#endif // PANDA_VARIABLES_TAG_H_

