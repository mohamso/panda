#include "start.h"

namespace variables {

std::vector<SgVariableDeclaration*> Start::CollectStartMappingVariables(std::map<SgVariableDeclaration*, std::string> rank_domain_size_map, variables::Geometry* geometry_object) {

	std::vector<SgVariableDeclaration*> start_variable_list;
	std::vector<SgVariableDeclaration*> geometry_variable_list = geometry_object->variables();
	std::vector<std::string> name_list = this->names();

	for (auto i = name_list.begin(), j = rank_domain_size_map.begin(), k = geometry_variable_list.begin(); i != name_list.end(), j != rank_domain_size_map.end(), k != geometry_variable_list.end(); i++, j++, k++) {
		std::string name = (*i);
		start_variable_list.push_back(this->CreateStartVariable(name, isSgVariableDeclaration(j->first), j->second, isSgVariableDeclaration((*k))));
	}

	return start_variable_list;
}

SgVariableDeclaration* Start::CreateStartVariable(std::string name, SgVariableDeclaration* rank_variable, std::string domain_size, SgVariableDeclaration* p_variable) {

	SgExpression* init = SageBuilder::buildDivideOp(SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(rank_variable), SageBuilder::buildVarRefExp(domain_size)), SageBuilder::buildVarRefExp(p_variable));

	return SageBuilder::buildVariableDeclaration(name, SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(init));
}

void Start::Initialize() {

	this->name_list_.push_back("start_x");
	this->name_list_.push_back("start_y");
	this->name_list_.push_back("start_z");
}

} //namespace
