#ifndef PANDA_VARIABLES_DIRECTIONS_H
#define PANDA_VARIABLES_DIRECTIONS_H

#include "../panda.h"
#include "../domain/domain_manager.h"
#include "root_direction.h"
#include "rank.h"

namespace variables
{
class Direction: public EastDirection, public WestDirection, public NorthDirection, public SouthDirection, public FrontDirection, public BackDirection, public TopDirection, public BottomDirection {
public:
  Direction() { this->Initialize(); }

  // Getters
  int number_of_directions() const { return number_of_directions_; }
  std::string name() const { return "direction"; }
  SgVariableDeclaration* variable() const { return variable_; }
  std::vector<RootDirection*> const &directions() const { return direction_list_; }
  std::vector<SgExprStatement*> const &direction_statements() const { return direction_statement_list_; }

  // Setters
  void set_number_of_directions(int number_of_directions) { number_of_directions_ = number_of_directions; }
  void set_variable(SgVariableDeclaration* variable) { variable_ = variable; }
  void set_directions(const std::vector<RootDirection*> &value) { direction_list_ = value; }
  void set_direction_statements(const std::vector<SgExprStatement*> &value) { direction_statement_list_ = value; }

  // Functions
  SgVariableDeclaration* CreateDirectionVariable();

  std::vector<SgExpression*> CollectDirectionExpressions();
  std::vector<SgExprStatement*> CollectDirectionStatements(Rank* rank_object, std::vector<SgVariableDeclaration*> p_variable_list);

  std::vector<SgInitializedName*> CollectEnumValues();
  SgInitializedName* CreateEnumValue(std::string name);

  void ConnectPlanesToDirections(domain::DomainManager* domain_manager);
  void BindBoundaryDirectionVariable(domain::DomainManager* domain_manager);

  ~Direction() {}

private:
  std::string name_;
  int number_of_directions_;
  SgVariableDeclaration* variable_;
  std::vector<RootDirection*> direction_list_;
  std::vector<SgExprStatement*> direction_statement_list_;

  void Initialize();
  std::vector<RootDirection*> CreateDirectionObjects();
};

} // namespace

#endif // PANDA_VARIABLES_DIRECTIONS_H

