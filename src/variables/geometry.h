#ifndef PANDA_VARIABLES_GEOMETRY_H
#define PANDA_VARIABLES_GEOMETRY_H

#include "../panda.h"

namespace variables
{
class Geometry {
public:
	Geometry() { this->Initialize(); }

	// Getters
	std::string px() const { return "px"; }
	std::string py() const { return "py"; }
	std::string pz() const { return "pz"; }
	std::vector<std::string> name_list() const { return name_list_; }
	std::vector<SgVariableDeclaration*> variables() const { return variables_list_; }

	// Setter
    void set_variables(std::vector<SgVariableDeclaration*> value) { variables_list_ = value; }

	// Functions
	std::vector<SgVariableDeclaration*> CollectGeometryVariables();

	~Geometry() {}

private:
	void Initialize();
	std::vector<std::string> name_list_;
	std::vector<SgVariableDeclaration*> variables_list_;
};

}

#endif // PANDA_VARIABLES_GEOMETRY_H
