#include "rank.h"

namespace variables
{
  std::string Rank::name() const {
    return "rank";
  }

  SgVariableDeclaration* Rank::CreateRankVariable() {
	  return SageBuilder::buildVariableDeclaration(this->name(), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0)));
  }

  std::vector<SgVariableDeclaration*> Rank::CollectRankMappingVariables(std::vector<SgVariableDeclaration*> p_variable_list) {

    std::vector<SgVariableDeclaration*> variable_list;
    std::vector<RootRank*> rank_list = this->ranks();

    for(auto i = rank_list.begin(); i != rank_list.end(); i++) {
      RootRank* rank_variable = (*i);
      variable_list.push_back(rank_variable->CreateRankMappingVariable(this->variable(), p_variable_list));
    }

    this->set_rank_mapping_variables(variable_list);

    return variable_list;
  }

} // namespace
