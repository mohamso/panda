#include "tag.h"

namespace variables
{
  SgVariableDeclaration* Tag::CreateTagVariable(std::string tag_name, int value) {
    return SageBuilder::buildVariableDeclaration(tag_name, SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(value)));
  }
} // namespace
