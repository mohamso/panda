#ifndef PANDA_VARIABLES_SIZE_H
#define PANDA_VARIABLES_SIZE_H

#include "../panda.h"

namespace variables {

class Size {
public:
	Size() { this->set_name("size"); }

	// Getter
	SgVariableDeclaration* variable() const { return variable_; }
	std::string name() const { return name_; }

	// Setter
	void set_variable(SgVariableDeclaration* variable) { variable_ = variable; }
	void set_name(const std::string& name) { name_ = name; }

	// Functions
	SgVariableDeclaration* CreateSizeVariable();

	~Size() {}

private:
	SgVariableDeclaration* variable_;
	std::string name_;
};

} // namespace

#endif // PANDA_VARIABLES_SIZE_H
