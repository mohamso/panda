#ifndef PANDA_VARIABLES_TAG_H_
#define PANDA_VARIABLES_TAG_H_

#include "../panda.h"

namespace variables
{
  class Tag {
  public:

    // Getter
    std::vector<SgVariableDeclaration*> const &tag_variables() const { return tag_variable_list_; }
    SgVariableDeclaration* cpu_tag() const { return cpu_tag_; }
    SgVariableDeclaration* gpu_tag() const { return gpu_tag_; }

    // Setter
    void set_tag_variables(const std::vector<SgVariableDeclaration*> &value) { tag_variable_list_ = value; }
    void set_cpu_tag(SgVariableDeclaration* value) { this->cpu_tag_ = value; }
    void set_gpu_tag(SgVariableDeclaration* value) { this->gpu_tag_ = value; }

    // Functions
    SgVariableDeclaration* CreateTagVariable(std::string tag_name, int value);

    ~Tag() {}

  private:
    SgVariableDeclaration* cpu_tag_;
    SgVariableDeclaration* gpu_tag_;
    std::vector<SgVariableDeclaration*> tag_variable_list_;
};

} // namespace variables

#endif // PANDA_VARIABLES_TAG_H_

