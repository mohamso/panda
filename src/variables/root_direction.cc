#include "root_direction.h"

namespace variables
{
RootDirection::~RootDirection() {}

SgExprStatement* EastDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* px_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(p_variables.at(0)), SageBuilder::buildIntVal(1));
  SgExpression* test = SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(0)), px_minus_one);
  SgExpression* true_test = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildIntVal(1));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL");

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

SgExprStatement* WestDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* test = SageBuilder::buildGreaterThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(0)), SageBuilder::buildIntVal(0));
  SgExpression* true_test = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildIntVal(1));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL");

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

SgExprStatement* NorthDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* py_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(p_variables.at(1)), SageBuilder::buildIntVal(1));
  SgExpression* test = SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(1)), py_minus_one);

  SgExpression* true_test = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildVarRefExp(p_variables.at(0)));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL");

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

SgExprStatement* SouthDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* test = SageBuilder::buildGreaterThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(1)), SageBuilder::buildIntVal(0));
  SgExpression* true_test = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildVarRefExp(p_variables.at(0)));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL");

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

SgExprStatement* FrontDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* pz_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(p_variables.at(2)), SageBuilder::buildIntVal(1));
  SgExpression* test = SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(2)), pz_minus_one);

  SgExpression* true_test = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(p_variables.at(0)), SageBuilder::buildVarRefExp(p_variables.at(1))));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL");

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

SgExprStatement* BackDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* test = SageBuilder::buildGreaterThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(2)), SageBuilder::buildIntVal(0));
  SgExpression* true_test = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(p_variables.at(0)), SageBuilder::buildVarRefExp(p_variables.at(1))));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL"); //SageBuilder::buildMinusOp(SageBuilder::buildIntVal(1));

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

SgExprStatement* TopDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* pz_minus_one = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(p_variables.at(2)), SageBuilder::buildIntVal(1));
  SgExpression* test = SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(2)), pz_minus_one);

  SgExpression* true_test = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(p_variables.at(0)), SageBuilder::buildVarRefExp(p_variables.at(1))));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL"); //SageBuilder::buildMinusOp(SageBuilder::buildIntVal(1));

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

SgExprStatement* BottomDirection::BuildDirectionStatement(SgExpression* direction, Rank* rank_object, std::vector<SgVariableDeclaration*> p_variables) {

  SgExpression* test = SageBuilder::buildGreaterThanOp(SageBuilder::buildVarRefExp(rank_object->rank_mapping_variables().at(2)), SageBuilder::buildIntVal(0));
  SgExpression* true_test = SageBuilder::buildSubtractOp(SageBuilder::buildVarRefExp(rank_object->variable()), SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(p_variables.at(0)), SageBuilder::buildVarRefExp(p_variables.at(1))));
  SgExpression* false_test = SageBuilder::buildVarRefExp("MPI_PROC_NULL"); //SageBuilder::buildMinusOp(SageBuilder::buildIntVal(1));

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  SgExprStatement* statement = SageBuilder::buildAssignStatement(direction, conditional_expression);

  return statement;
}

} // namespace
