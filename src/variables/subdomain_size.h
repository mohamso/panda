#ifndef PANDA_VARIABLES_SUBDOMAINSIZE_H
#define PANDA_VARIABLES_SUBDOMAINSIZE_H

#include "../panda.h"
#include <map>

namespace variables
{
class SubdomainSize {
public:
  SubdomainSize() {
    this->set_nsdx("nsdx");
    this->set_nsdy("nsdy");
    this->set_nsdz("nsdz");
  }

  // Getters
  std::string nsdx() const { return nsdx_; }
  std::string nsdy() const { return nsdy_; }
  std::string nsdz() const { return nsdz_; }
  std::vector<SgVariableDeclaration*> const &variables() const { return variable_list_; } // duplicate, implemented on decomposer - mark for deletion
  std::map<std::string, std::string> const &global_to_local_size_map() const { return global_to_local_size_map_; }

  // Setters
  void set_nsdx(const std::string& value) { nsdx_ = value; }
  void set_nsdy(const std::string& value) { nsdy_ = value; }
  void set_nsdz(const std::string& value) { nsdz_ = value; }
  void set_variables(const std::vector<SgVariableDeclaration*> &value) { variable_list_ = value; } // duplicate, also implemented on decomposer - mark for deletion
  void set_global_to_local_size_map(const std::map<std::string, std::string> &value) { global_to_local_size_map_ = value; }

  std::vector<SgVariableDeclaration*> CollectSubdomainSizeVariables(std::vector<SgExpression*> expression_list, SgScopeStatement* scope);
  std::vector<SgExpression*> CollectAssignInitializerExpression(std::map<SgVarRefExp*, SgVariableDeclaration*> subdomain_geomeotry_size_map, std::vector<SgVariableDeclaration*> rank_variable_list);

  SgVariableDeclaration* CreateSubdomainSizeVariable(std::string variable_name, SgExpression* expression, SgScopeStatement* scope);
  std::vector<std::string> CollectSubdomainSizeVariableNames();

private:
  std::string nsdx_;
  std::string nsdy_;
  std::string nsdz_;
  std::vector<SgVariableDeclaration*> variable_list_; // duplicate - mark for deletion
  std::map<std::string, std::string> global_to_local_size_map_;

};

}

#endif // PANDA_VARIABLES_SUBDOMAINSIZE_H
