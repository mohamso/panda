#include "size.h"

namespace variables
{

  SgVariableDeclaration* Size::CreateSizeVariable() {
    SgVariableDeclaration* size_variable = 
    SageBuilder::buildVariableDeclaration(this->name(), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0)));

    return size_variable;
  }

} // namespace
