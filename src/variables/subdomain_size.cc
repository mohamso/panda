#include "subdomain_size.h"

namespace variables
{
std::vector<SgVariableDeclaration*> SubdomainSize::CollectSubdomainSizeVariables(std::vector<SgExpression*> expression_list, SgScopeStatement* scope) {

	std::vector<std::string> name_list = this->CollectSubdomainSizeVariableNames();

	std::vector<SgVariableDeclaration*> variable_list;

	for(auto i = name_list.begin(), j = expression_list.begin(); i != name_list.end(), j != expression_list.end(); i++, j++) {
		std::string variable_name = (*i);
		SgExpression* expression = (*j);
		variable_list.push_back(this->CreateSubdomainSizeVariable(variable_name, expression, scope));
	}

	name_list.clear();

	return variable_list;
}

std::vector<SgExpression*> SubdomainSize::CollectAssignInitializerExpression(std::map<SgVarRefExp*, SgVariableDeclaration*> subdomain_geometery_size_map, std::vector<SgVariableDeclaration*> rank_variable_list) {

	std::vector<std::string> local_size_names = this->CollectSubdomainSizeVariableNames();
	std::vector<SgExpression*> expression_list;
	std::map<std::string, std::string> global_to_local_size_map;

	for (auto i = subdomain_geometery_size_map.begin(), j = rank_variable_list.begin(), k = local_size_names.begin(); i != subdomain_geometery_size_map.end(), j != rank_variable_list.end(), k != local_size_names.end(); i++, j++, k++) {

		global_to_local_size_map[isSgVarRefExp(i->first)->unparseToString()] = (*k);

		SgExpression* first_part = SageBuilder::buildMultiplyOp(SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(isSgVariableDeclaration(*j)), SageBuilder::buildIntVal(1)), i->first);
		SgExpression* lhs = SageBuilder::buildDivideOp(first_part, SageBuilder::buildVarRefExp(i->second));
		SgExpression* rhs = SageBuilder::buildDivideOp(SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(isSgVariableDeclaration(*j)), i->first), SageBuilder::buildVarRefExp(i->second));

		expression_list.push_back(SageBuilder::buildSubtractOp(lhs, rhs));
	}

	this->set_global_to_local_size_map(global_to_local_size_map);

	local_size_names.clear();

	return expression_list;
}

SgVariableDeclaration* SubdomainSize::CreateSubdomainSizeVariable(std::string variable_name, SgExpression* expression, SgScopeStatement* scope) {
	return SageBuilder::buildVariableDeclaration(variable_name, SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(expression), scope);
}

std::vector<std::string> SubdomainSize::CollectSubdomainSizeVariableNames() {

	std::vector<std::string> name_list;

	name_list.push_back(this->nsdx());
	name_list.push_back(this->nsdy());
	name_list.push_back(this->nsdz());

	return name_list;
}

} // namespace
