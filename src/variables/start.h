#ifndef PANDA_VARIABLES_START_H
#define PANDA_VARIABLES_START_H

#include "../panda.h"
#include "rank.h"
#include "geometry.h"

namespace variables
{
class Start {
public:

	Start() { this->Initialize(); }

	// Getter
	std::vector<SgVariableDeclaration*> variables() const { return variable_list_; }
	std::vector<std::string> names() const { return name_list_; }

	// Setter
	void set_variables(std::vector<SgVariableDeclaration*> value) { variable_list_ = value; }
	void set_names(std::vector<std::string> value) { name_list_ = value; }

	// Functions
	std::vector<SgVariableDeclaration*> CollectStartMappingVariables(std::map<SgVariableDeclaration*, std::string> rank_domain_size_map, variables::Geometry* geometry_object);
	SgVariableDeclaration* CreateStartVariable(std::string name, SgVariableDeclaration* rank_variable, std::string domain_size, SgVariableDeclaration* p_variable);

	~Start() {}

private:
	void Initialize();
	std::vector<SgVariableDeclaration*> variable_list_;
	std::vector<std::string> name_list_;
};

} // namespace variables

#endif // PANDA_VARIABLES_START_H
