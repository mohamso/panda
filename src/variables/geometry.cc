#include "geometry.h"

namespace variables
{

std::vector<SgVariableDeclaration*> Geometry::CollectGeometryVariables() {

	std::vector<std::string> name_list = this->name_list();
	std::vector<SgVariableDeclaration*> variable_list;

	for (auto i = name_list.begin(); i != name_list.end(); i++) {
		variable_list.push_back(SageBuilder::buildVariableDeclaration((*i), SageBuilder::buildIntType()));
	}

	return variable_list;
}

void Geometry::Initialize() {
	this->name_list_.push_back("px");
	this->name_list_.push_back("py");
	this->name_list_.push_back("pz");
}

} // namespace
