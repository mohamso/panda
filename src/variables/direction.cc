#include "direction.h"

namespace variables
{

void Direction::Initialize() {
  this->number_of_directions_ = 6;
  this->direction_list_ = this->CreateDirectionObjects();
}
std::vector<RootDirection*> Direction::CreateDirectionObjects() {

  std::vector<RootDirection*> direction_list;

  direction_list.push_back(new EastDirection());
  direction_list.push_back(new WestDirection());
  direction_list.push_back(new NorthDirection());
  direction_list.push_back(new SouthDirection());
  direction_list.push_back(new FrontDirection());
  direction_list.push_back(new BackDirection());

  return direction_list;
}

SgVariableDeclaration* Direction::CreateDirectionVariable() {
  SgVariableDeclaration* direction_variable = SageBuilder::buildVariableDeclaration(this->name(),
      SageBuilder::buildArrayType(SageBuilder::buildIntType(), SageBuilder::buildIntVal((int)this->number_of_directions())));

  return direction_variable;
}

std::vector<SgExpression*> Direction::CollectDirectionExpressions() {

  std::vector<SgExpression*> expression_list;

  for (auto i = this->directions().begin(); i != this->directions().end(); i++) {
    RootDirection* direction_object = (*i);
    expression_list.push_back(SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(this->name()), SageBuilder::buildVarRefExp(direction_object->name())));
  }

  return expression_list;
}

std::vector<SgExprStatement*> Direction::CollectDirectionStatements(Rank* rank_object, std::vector<SgVariableDeclaration*> p_variable_list) {

  std::vector<SgExprStatement*> statement_list;
  std::vector<SgExpression*> direction_exprs = this->CollectDirectionExpressions();

  for (auto i = direction_exprs.begin(), j = this->directions().begin(); i != direction_exprs.end(), j != this->directions().end(); i++, j++) {
    SgExpression* expression = (*i);
    RootDirection* direction_object = (*j);
    statement_list.push_back(direction_object->BuildDirectionStatement(expression, rank_object, p_variable_list));
  }

  return statement_list;
}

std::vector<SgInitializedName*> Direction::CollectEnumValues() {

  std::vector<SgInitializedName*> enum_list;

  for (auto i = this->directions().begin(); i != this->directions().end(); i++) {
    RootDirection* direction_object = (*i);
    enum_list.push_back(this->CreateEnumValue(direction_object->name()));
  }

  return enum_list;
}


SgInitializedName* Direction::CreateEnumValue(std::string name) {

  SgInitializedName* enum_value = new SgInitializedName(name, NULL, NULL, NULL);
  enum_value->set_file_info(Sg_File_Info::generateDefaultFileInfoForTransformationNode());

  return enum_value;
}

void Direction::ConnectPlanesToDirections(domain::DomainManager* domain_manager) {

  for (auto i = this->directions().begin(); i != this->directions().end(); i++) {
    RootDirection* direction = (*i);

    for (auto j = domain_manager->decomposer()->subdomain_variables().begin(); j != domain_manager->decomposer()->subdomain_variables().end(); j++) {
      if (isSgVariableDeclaration(*j)->get_variables().front()->unparseToString() == domain_manager->decomposer()->subdomain_size()->nsdx()) {
        if (direction->IsFront() || direction->IsBack() || direction->IsNorth() || direction->IsSouth()) {
          direction->set_plane_one(isSgVariableDeclaration(*j));
        }
      }

      if (isSgVariableDeclaration(*j)->get_variables().front()->unparseToString() == domain_manager->decomposer()->subdomain_size()->nsdy()) {
        if (direction->IsFront() || direction->IsBack()) {
          direction->set_plane_two(isSgVariableDeclaration(*j));
        }

        if (direction->IsEast() || direction->IsWest()) {
          direction->set_plane_one(isSgVariableDeclaration(*j));
        }
      }

      if (isSgVariableDeclaration(*j)->get_variables().front()->unparseToString() == domain_manager->decomposer()->subdomain_size()->nsdz()) {
        if (direction->IsEast() || direction->IsWest() || direction->IsNorth() || direction->IsSouth() ) {
          direction->set_plane_two(isSgVariableDeclaration(*j));
        }
      }
    }
  }
}

void Direction::BindBoundaryDirectionVariable(domain::DomainManager* domain_manager) {

  for (auto i = directions().begin(); i != this->directions().end(); i++) {
    RootDirection* direction = (*i);

    for (auto j = domain_manager->decomposer()->subdomain_variables().begin(); j != domain_manager->decomposer()->subdomain_variables().end(); j++) {
    	 if (isSgVariableDeclaration(*j)->get_variables().front()->unparseToString() == domain_manager->decomposer()->subdomain_size()->nsdx()) {
    		 if (direction->IsEast() || direction->IsWest()) {
    			 direction->set_boundary_direction(isSgVariableDeclaration(*j));
    		 }
    	 }

    	 if (isSgVariableDeclaration(*j)->get_variables().front()->unparseToString() == domain_manager->decomposer()->subdomain_size()->nsdy()) {
    		 if (direction->IsNorth() || direction->IsSouth()) {
    			 direction->set_boundary_direction(isSgVariableDeclaration(*j));
    		 }
    	 }

    	 if (isSgVariableDeclaration(*j)->get_variables().front()->unparseToString() == domain_manager->decomposer()->subdomain_size()->nsdz()) {
    		 if (direction->IsFront() || direction->IsBack()) {
    			 direction->set_boundary_direction(isSgVariableDeclaration(*j));
    		 }
    	 }
    }
  }
}

} // namespace
