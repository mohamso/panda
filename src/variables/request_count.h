#ifndef PANDA_VARIABLES_REQUESTCOUNT_H
#define PANDA_VARIABLES_REQUESTCOUNT_H

#include "../panda.h"

namespace variables {

class RequestCount {
  public:
    // Getter
    SgVariableDeclaration* variable() const { return variable_; }
    SgExprStatement* statement() const { return statement_; }
    std::string name() const { return name_; }

    // Setter
    void set_variable(SgVariableDeclaration* variable) { variable_ = variable; }
    void set_statement(SgExprStatement* value) { statement_ = value; }
    void set_name(const std::string& name) { name_ = name; }

    // Functions
    SgVariableDeclaration* CreateRequestCountVariable();
    SgExprStatement* CreateRequestCountStatement();
    SgExprStatement* CreateRequestCountPlusPlusStatement();

    RequestCount()
    {
      this->set_name("request_count");
    }

  private:
    SgVariableDeclaration* variable_;
    SgExprStatement* statement_;
    std::string name_;
};

} // namespace

#endif // PANDA_VARIABLES_REQUESTCOUNT_H
