#include "request_count.h"

namespace variables
{
  SgVariableDeclaration* RequestCount::CreateRequestCountVariable() {
    SgVariableDeclaration* request_count_variable = 
    SageBuilder::buildVariableDeclaration(this->name(), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0)));

    return request_count_variable;
  }

  SgExprStatement* RequestCount::CreateRequestCountStatement() {
    SgExprStatement* assignment_statement = SageBuilder::buildAssignStatement(SageBuilder::buildVarRefExp(this->variable()), SageBuilder::buildIntVal(0));

    return assignment_statement;
  }

  SgExprStatement* RequestCount::CreateRequestCountPlusPlusStatement() {
    return SageBuilder::buildExprStatement(SageBuilder::buildPlusPlusOp(SageBuilder::buildVarRefExp(this->variable()), SgUnaryOp::postfix));
  }

} // namespace
