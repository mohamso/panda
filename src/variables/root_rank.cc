#include "root_rank.h"

namespace variables
{
  RootRank::~RootRank() {}

  std::string RankX::name() const {
    return "rank_x";
  }

  std::string RankY::name() const {
    return "rank_y";
  }

  std::string RankZ::name() const {
    return "rank_z";
  }

  SgVariableDeclaration* RankX::CreateRankMappingVariable(SgVariableDeclaration* rank_variable, std::vector<SgVariableDeclaration*> p_variable_list) {

    SgExpression* expression = SageBuilder::buildModOp(SageBuilder::buildVarRefExp(rank_variable), SageBuilder::buildVarRefExp(p_variable_list.at(0)));

    SgVariableDeclaration* rankx_variable = 
    SageBuilder::buildVariableDeclaration(this->name(), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(expression));

    return rankx_variable;    
  }

  SgVariableDeclaration* RankY::CreateRankMappingVariable(SgVariableDeclaration* rank_variable, std::vector<SgVariableDeclaration*> p_variable_list) {

    SgExpression* expression = SageBuilder::buildDivideOp(SageBuilder::buildModOp(SageBuilder::buildVarRefExp(rank_variable),
      SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(p_variable_list.at(0)), SageBuilder::buildVarRefExp(p_variable_list.at(1)))), SageBuilder::buildVarRefExp(p_variable_list.at(0)));

    SgVariableDeclaration* ranky_variable = 
    SageBuilder::buildVariableDeclaration(this->name(), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(expression));

    return ranky_variable;    
  }

  SgVariableDeclaration* RankZ::CreateRankMappingVariable(SgVariableDeclaration* rank_variable, std::vector<SgVariableDeclaration*> p_variable_list) {

    SgExpression* expression = SageBuilder::buildDivideOp();
    SageInterface::setLhsOperand(expression, SageBuilder::buildVarRefExp(rank_variable));
    SageInterface::setRhsOperand(expression, SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(p_variable_list.at(0)), SageBuilder::buildVarRefExp(p_variable_list.at(1))));

    SgVariableDeclaration* rankz_variable = 
    SageBuilder::buildVariableDeclaration(this->name(), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(expression));

    return rankz_variable;      
  }

} // namespace
