#ifndef PANDA_LOOP_LOOPMANAGER_H
#define PANDA_LOOP_LOOPMANAGER_H

#include "../panda.h"
#include "../translator/pragma_manager.h"
#include "../domain/decomposer.h"
#include "loop_transformer.h"
#include "compute_loop_transformer.h"
#include "initialize_loop_transformer.h"
#include <map>

namespace loop
{
class LoopManager {
public:
  LoopManager() { this->Initialize(); }

  // Getters
  LoopTransformer* loop_transformer() const { return loop_transformer_; }
  ComputeLoopTransformer* compute_transformer() const { return compute_transformer_; }
  InitializeLoopTransformer* initialize_transformer() const { return initialize_transformer_; }

  // Setters
  void set_compute_transformer(ComputeLoopTransformer* value) { compute_transformer_ = value; }
  void set_initialize_transformer(InitializeLoopTransformer* value) { initialize_transformer_ = value; }

  // Functions
  std::vector<SgForStatement*> CollectLoopStatements(std::vector<SgPragmaDeclaration*> pragma_list, std::string distributed);
  bool IsInnerMostLoop(SgForStatement* for_statement);

private:
  std::vector<SgForStatement*> FindEnclosingForLoops(SgPragmaDeclaration* pragma_declaration);

  LoopTransformer* loop_transformer_;
  ComputeLoopTransformer* compute_transformer_;
  InitializeLoopTransformer* initialize_transformer_;

  void Initialize();
};

} // namespace loop

#endif // PANDA_LOOP_LOOPMANAGER_H
