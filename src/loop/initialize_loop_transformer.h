#ifndef PANDA_LOOP_INITIALIZELOOPTRANSFORMER_H
#define PANDA_LOOP_INITIALIZELOOPTRANSFORMER_H

#include "iloop_transformer.h"

namespace loop
{
class InitializeLoopTransformer : public ILoopTransformer {
public:
	InitializeLoopTransformer() {}

	// Functions
	void TransformLoopBody(domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager);

	~InitializeLoopTransformer() {}

private:
	std::map<std::string, std::string> MapInitVariablesToStartVariables(std::vector<SgVariableDeclaration*> init_variable_list);
	void UpdateInitReferences(std::vector<SgVariableDeclaration*> init_variable_list, std::map<std::string, std::string> init_start_map, std::vector<std::string> index_name_list, SgForStatement* for_loop);
	std::map<int,std::vector<SgVariableDeclaration*>> InsertStartVariables(std::vector<SgVariableDeclaration*> start_variable_list, std::vector<SgForStatement*> loop_list);
	std::vector<SgVariableDeclaration*> CollectIndexVariables(std::vector<SgExpression*> init_expression_list, std::vector<SgVariableDeclaration*> start_variable_list);
	SgVariableDeclaration* CreateIndexVariable(SgExpression* init_variable, SgVariableDeclaration* start_variable);
	void ReplaceLoopTestVariablesWithPVariables(SgExpression* rhs_expression, std::map<std::string, std::string> init_start_map);
	void CollectLoopTestVariables(SgForStatement* for_loop, std::map<std::string, std::string> init_start_map);
};

} // namespace loop

#endif // PANDA_LOOP_INITIALIZELOOPTRANSFORMER_H
