#ifndef PANDA_LOOP_LOOP_GENERATOR_H
#define PANDA_LOOP_LOOP_GENERATOR_H

#include "../panda.h"
#include "../variables/root_direction.h"

namespace loop
{
class LoopGenerator {
public:

	SgForStatement* CreateBoundaryForLoops(variables::RootDirection* direction, SgExprStatement* unpack_stmt, SgVariableDeclaration* index3D_variable);
	SgForStatement* CreateForLoop(const std::string& increment_name, SgVariableDeclaration* subdomain_size, SgBasicBlock* loop_body);

	SgForStatement* GetInnerMostLoop(SgForStatement* for_statement);

private:
	std::string GetLoopIncrementValue(SgVariableDeclaration* plane);
	bool IsInnerMostLoop(SgForStatement* for_statement);

	~LoopGenerator() {};
};

} // namespace loop

#endif // PANDA_LOOP_LOOP_GENERATOR_H
