#include "loop_transformer.h"

namespace loop
{

/**
* @brief  A generic function that replaces all iterator references to
* global domain variables with references to that corresponds to
* local subdomain variables.
*
* The translation will abort if the function fails.
*
* @param[in]  for_loops  A vector of for loop statements
*/
void LoopTransformer::TransformLoopIteratorsOrDie(std::vector<SgForStatement*> for_loops, std::map<std::string, std::string> size_map) {

  for (auto i = for_loops.begin(); i != for_loops.end(); i++) {
    auto test_exp = isSgForStatement(*i)->get_test();

    if (test_exp != NULL) {
      Rose_STL_Container<SgNode*> node_list = NodeQuery::querySubTree(test_exp, V_SgVarRefExp);

      for (auto j = node_list.begin(); j != node_list.end(); j++) {

        auto p = size_map.find(isSgVarRefExp(*j)->unparseToString());

        if (p != size_map.end()) {
          SageInterface::replaceExpression(isSgVarRefExp(*j), SageBuilder::buildVarRefExp(p->second));
        }
      }

    } else {
      std::cerr << "Panda: Could not get reduction loop iterator statement." << std::endl;
      std::cerr << "Panda error: Translation will abort." << std::endl;
      exit(1);
    }
  }
}

/**
* @brief  A generic function that replaces all references to
* global domain variables with references to that corresponds to
* local subdomain variables in the body of the inner most loop.
*
* The translation will abort if the function fails.
*
* @param[in]  for_loop_body  For loop body
*/
void LoopTransformer::TransformLoopBody(SgStatement* for_loop_body, std::map<std::string, std::string> size_map) {

  Rose_STL_Container<SgNode*> node_list = NodeQuery::querySubTree(for_loop_body, V_SgVarRefExp);

  for (auto j = node_list.begin(); j != node_list.end(); j++) {

    auto p = size_map.find(isSgVarRefExp(*j)->unparseToString());

    if (p != size_map.end()) {
      SageInterface::replaceExpression(isSgVarRefExp(*j), SageBuilder::buildVarRefExp(p->second));
    }
  }
}

} //namespace
