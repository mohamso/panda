#ifndef PANDA_LOOP_REDUCTIONLOOPTRANSFORMER_H
#define PANDA_LOOP_REDUCTIONLOOPTRANSFORMER_H

#include "iloop_transformer.h"

namespace loop
{
class ReductionLoopTransformer : public ILoopTransformer {
public:
	ReductionLoopTransformer() {}

	// Getter
	std::map<SgVariableDeclaration*, SgVariableDeclaration*> reduction_variables() const { return reduction_variable_list_; }

	// Setter
	void set_reduction_variable_map(std::map<SgVariableDeclaration*, SgVariableDeclaration*> value) { reduction_variable_list_ = value; }

	// Functions
	void TransformLoopTest(domain::Decomposer* decomposer);
	void TransformLoopBody(domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager);
	void TransformReductionVariables(SgProject* project, domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager);
	std::map<SgVariableDeclaration*, SgVariableDeclaration*> CreateReductionVariable(SgProject* project, translator::PragmaManager* pragma_manager);

	~ReductionLoopTransformer() {}

private:
	SgVariableDeclaration* CreateReductionVariable(std::string variable_name, SgType* type);
	std::map<SgVariableDeclaration*, SgVariableDeclaration*> reduction_variable_list_;
};

} // namespace loop

#endif // PANDA_LOOP_REDUCTIONLOOPTRANSFORMER_H
