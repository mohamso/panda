#ifndef PANDA_LOOP_ILOOPTRANSFORMER_H
#define PANDA_LOOP_ILOOPTRANSFORMER_H

#include "../panda.h"
#include "../domain/domain_manager.h"
#include <map>

namespace loop
{
class ILoopTransformer {
public:

	// Getter
	virtual std::vector<SgForStatement*> const &loop_statements() const { return loop_statements_; }
	virtual std::map<std::string, std::string> domain_size_map() const { return size_map_; }
	virtual std::map<std::string, std::string> domain_name_map() const { return name_map_; }

	// Setter
	void set_loop_statements(const std::vector<SgForStatement*> &value) { loop_statements_ = value; }
	void set_domain_size_map(std::map<std::string, std::string> value) { size_map_ = value; }
	void set_domain_name_map(std::map<std::string, std::string> value) { name_map_ = value; }

	// Functions
	virtual SgForStatement* OuterMostLoop();

	virtual std::vector<std::string> FindIndexVariable(SgForStatement* statement);
	virtual std::map<std::string, std::string> CollectIndexDeclaration(SgForStatement* statement, std::vector<std::string> index_name_list);

	virtual bool IsInnerMostLoop(SgForStatement* for_statement);

	virtual void TransformIndex(SgForStatement* statement, std::map<std::string, std::string> index_map, std::vector<SgVariableDeclaration*> subdomain_variables, std::vector<std::string> domain_size_list);
	virtual void TransformLoopTest(domain::Decomposer* decomposer);
	virtual void TransformLoopBody(domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager) = 0;

	virtual ~ILoopTransformer();

private:
	virtual void UpdateIndexAssignment(Rose_STL_Container<SgNode*> assign_list, std::map<std::string, std::string> index_map, std::vector<SgVariableDeclaration*> subdomain_variables, std::vector<std::string> domain_size_list);

protected:

	std::vector<SgForStatement*> loop_statements_;
	std::map<std::string, std::string> size_map_;
	std::map<std::string, std::string> name_map_;

};

} // namespace loop

#endif // PANDA_LOOP_ILOOPTRANSFORMER_H
