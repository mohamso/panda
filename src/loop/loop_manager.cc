#include "loop_manager.h"

namespace loop
{

std::vector<SgForStatement*> LoopManager::CollectLoopStatements(std::vector<SgPragmaDeclaration*> pragma_list, std::string pragma_string_to_match) {

	std::vector<SgForStatement*> for_loop_list;

	for(auto i = pragma_list.begin(); i != pragma_list.end(); i++) {
		std::string pragma_string = isSgPragmaDeclaration(*i)->get_pragma()->get_pragma();

		if (pragma_string.find(pragma_string_to_match) == 0) {
			for_loop_list = this->FindEnclosingForLoops(isSgPragmaDeclaration(*i));
		}
	}

	return for_loop_list;
}

std::vector<SgForStatement*> LoopManager::FindEnclosingForLoops(SgPragmaDeclaration* pragma_declaration) {

	std::vector<SgForStatement*> for_loop_list;
	SgScopeStatement* scope = pragma_declaration->get_scope();

	if (isSgScopeStatement(scope)) {

		Rose_STL_Container<SgNode*> for_loop_statements = NodeQuery::querySubTree(SageInterface::getNextStatement(pragma_declaration), V_SgForStatement);

		for (auto i = for_loop_statements.begin(); i != for_loop_statements.end(); i++) {
			for_loop_list.push_back(isSgForStatement(*i));
		}
	}

	return for_loop_list;
}

/**
* @brief  A function that checks whether the loop level is the
* the last one or not.
*
* @param[in]  for_statement  A for loop statement object
*/
bool LoopManager::IsInnerMostLoop(SgForStatement* for_statement) {

	Rose_STL_Container<SgNode*> ref_list = NodeQuery::querySubTree(for_statement->get_loop_body(), V_SgForStatement);

	if (ref_list.size() == 0) {
		return true;
	}

	return false;
}

void LoopManager::Initialize() {
	this->loop_transformer_ = new LoopTransformer();
	this->set_compute_transformer(new ComputeLoopTransformer());
	this->set_initialize_transformer(new InitializeLoopTransformer());
}

} //namespace
