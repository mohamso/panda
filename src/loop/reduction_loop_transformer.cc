#include "reduction_loop_transformer.h"

namespace loop
{

void ReductionLoopTransformer::TransformLoopBody(domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager) {

  std::vector<SgForStatement*> inner_most_loop_list;

  for (auto i = this->loop_statements().begin(); i != this->loop_statements().end(); i++) {
    if (this->IsInnerMostLoop(isSgForStatement(*i))) {
      inner_most_loop_list.push_back(*i);
    }
  }

  // Find and transform the index variable
  std::map<std::string, std::string> index_map;
  std::vector<std::string> index_name_list;

  for (auto i = inner_most_loop_list.begin(); i != inner_most_loop_list.end(); i++) {
    index_name_list = this->FindIndexVariable(isSgForStatement(*i));
    index_map = this->CollectIndexDeclaration(isSgForStatement(*i), index_name_list);

    this->TransformIndex(isSgForStatement(*i), index_map, decomposer->subdomain_variables(), pragma_manager->domain_size_list());
  }
}

void ReductionLoopTransformer::TransformReductionVariables(SgProject* project, domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager) {
  // Find user-specified variable and perform transformations
  Rose_STL_Container<SgNode*> var_ref_exp_list;
  Rose_STL_Container<SgNode*> declaration_list = NodeQuery::querySubTree(project, V_SgAssignOp);

  for (auto i = declaration_list.begin(); i != declaration_list.end(); i++) {
    if (isSgAssignOp(*i)->get_lhs_operand() != NULL) {
      if (isSgAssignOp(*i)->get_lhs_operand()->variantT() == V_SgVarRefExp) {
        bool IsPresent = (std::find(pragma_manager->reduction_clause_list().begin(), pragma_manager->reduction_clause_list().end(), isSgVarRefExp(isSgAssignOp(*i)->get_lhs_operand())->unparseToString()) != pragma_manager->reduction_clause_list().end());

        if (IsPresent) {
          if (isSgAssignOp(*i)->get_rhs_operand() != NULL) {
            var_ref_exp_list = NodeQuery::querySubTree(isSgAssignOp(*i)->get_rhs_operand(), V_SgVarRefExp);
          }
        }
      }
    }
  }

  std::vector<SgVarRefExp*> detected_domain_size_list;

  for (auto i = var_ref_exp_list.begin(); i != var_ref_exp_list.end(); i++) {
    bool IsPresent = (std::find(pragma_manager->domain_size_list().begin(), pragma_manager->domain_size_list().end(), isSgVarRefExp(*i)->unparseToString()) != pragma_manager->domain_size_list().end());

    if (IsPresent) {
      detected_domain_size_list.push_back(isSgVarRefExp(*i));
    }
  }

  for (auto i = detected_domain_size_list.begin(), j = decomposer->subdomain_variables().begin(); i != detected_domain_size_list.end(), j != decomposer->subdomain_variables().end(); i++, j++) {
    SageInterface::replaceExpression(isSgVarRefExp(*i), SageBuilder::buildVarRefExp(isSgVariableDeclaration(*j)));
  }
}

std::map<SgVariableDeclaration*, SgVariableDeclaration*> ReductionLoopTransformer::CreateReductionVariable(SgProject* project, translator::PragmaManager* pragma_manager) {

  // Create variable based on the user specified variables
  // that are defined in the clause list
  std::vector<std::string> clause_list = pragma_manager->reduction_clause_list();
  Rose_STL_Container<SgNode*> declaration_list = NodeQuery::querySubTree(project, V_SgVariableDeclaration);
  std::vector<SgVariableDeclaration*> clause_declaration_list;

  for (auto i = declaration_list.begin(); i != declaration_list.end(); i++) {

    bool IsPresent = (std::find(clause_list.begin(), clause_list.end(), isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()) != clause_list.end());

    if (IsPresent) {
      clause_declaration_list.push_back(isSgVariableDeclaration(*i));
    }
  }

  // Create global reduction variable
  std::map<SgVariableDeclaration*, SgVariableDeclaration*> reduction_variable_list;

  for (auto i = clause_declaration_list.begin(); i != clause_declaration_list.end(); i++) {
    SgVariableDeclaration* reduction_variable = this->CreateReductionVariable(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString(), SageInterface::getFirstVarSym(isSgVariableDeclaration(*i))->get_type());
    SageInterface::insertStatementAfter(isSgVariableDeclaration(*i), reduction_variable);
    reduction_variable_list[isSgVariableDeclaration(*i)] = reduction_variable;
  }

  return reduction_variable_list;
}

SgVariableDeclaration* ReductionLoopTransformer::CreateReductionVariable(std::string variable_name, SgType* type) {

  return SageBuilder::buildVariableDeclaration("p_global_" + variable_name, type, SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0)));
}

} // namespace
