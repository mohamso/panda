#include "loop_generator.h"

namespace loop
{

SgForStatement* LoopGenerator::CreateBoundaryForLoops(variables::RootDirection* direction, SgExprStatement* unpack_stmt, SgVariableDeclaration* index3D_variable) {

  SgBasicBlock* inner_loop_body = SageBuilder::buildBasicBlock();

  SageInterface::prependStatement(unpack_stmt, inner_loop_body);
  SageInterface::prependStatement(index3D_variable, inner_loop_body);

  SgForStatement* inner_loop = this->CreateForLoop(this->GetLoopIncrementValue(direction->plane_one()), direction->plane_one(), inner_loop_body);

  SgBasicBlock* outer_loop_body = SageBuilder::buildBasicBlock(inner_loop);

  return this->CreateForLoop(this->GetLoopIncrementValue(direction->plane_two()), direction->plane_two(), outer_loop_body);
}

SgForStatement* LoopGenerator::CreateForLoop(const std::string& increment_name, SgVariableDeclaration* subdomain_size, SgBasicBlock* loop_body) {

	SgStatement* init_statement = SageBuilder::buildVariableDeclaration(increment_name, SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(1)), NULL);
	SgStatement* condition_statement = SageBuilder::buildExprStatement(SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(increment_name, NULL), SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(subdomain_size), SageBuilder::buildIntVal(1))));
	SgExpression* increment_expression = SageBuilder::buildPlusPlusOp(SageBuilder::buildVarRefExp(increment_name, NULL), SgUnaryOp::postfix);

	return SageBuilder::buildForStatement(init_statement, condition_statement, increment_expression, loop_body);
}

std::string LoopGenerator::GetLoopIncrementValue(SgVariableDeclaration* plane) {
	std::string name = "ERROR";

	if (plane->get_variables().front()->unparseToString() == "nsdz") {
		name = "k";
	}

	if (plane->get_variables().front()->unparseToString() == "nsdy") {
		name = "j";
	}

	if (plane->get_variables().front()->unparseToString() == "nsdx") {
		name = "i";
	}

	return name;
}

SgForStatement* LoopGenerator::GetInnerMostLoop(SgForStatement* for_statement) {
	Rose_STL_Container<SgNode*> loop_list = NodeQuery::querySubTree(for_statement->get_loop_body(), V_SgForStatement);

	SgForStatement* inner_most_loop;

	for (auto i = loop_list.begin(); i != loop_list.end(); i++) {
		 if (this->IsInnerMostLoop(isSgForStatement(*i))) {
			 inner_most_loop = isSgForStatement(*i);
		 }
	}

	if (inner_most_loop == NULL) {
		std::cout << "Panda: Could not locate the inner most loop body" << std::endl;
	}

	return inner_most_loop;
}

// Duplicate code.
// Marked for deletion
bool LoopGenerator::IsInnerMostLoop(SgForStatement* for_statement) {

	Rose_STL_Container<SgNode*> ref_list = NodeQuery::querySubTree(for_statement->get_loop_body(), V_SgForStatement);

	if (ref_list.size() == 0) {
		return true;
	}

	return false;
}

} //namespace
