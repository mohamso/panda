#include "iloop_transformer.h"

namespace loop
{

ILoopTransformer::~ILoopTransformer() {}

// This function is possibly outdated and replaced by TransformLoopIteratorsOrDie
void ILoopTransformer::TransformLoopTest(domain::Decomposer* decomposer) {

	std::vector<SgBinaryOp*> loop_test_list;
	std::map<std::string, SgVariableDeclaration*> test_map;

	for (auto i = this->loop_statements().begin(); i != this->loop_statements().end(); i++) {
		loop_test_list.push_back(isSgBinaryOp(isSgExprStatement((*i)->get_test())->get_expression()));
	}

	for (auto i = this->loop_statements().begin(), j = decomposer->subdomain_variables().rbegin(); i != this->loop_statements().end(), j != decomposer->subdomain_variables().rend(); i++, j++) {
		test_map[isSgBinaryOp(isSgExprStatement((*i)->get_test())->get_expression())->get_lhs_operand()->unparseToString()] = (*j);
	}

	for (auto i = loop_test_list.begin(); i != loop_test_list.end(); i++) {

		auto p = test_map.find(isSgBinaryOp(*i)->get_lhs_operand()->unparseToString());

		if (p != test_map.end()) {

			Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(isSgBinaryOp(*i)->get_rhs_operand(), V_SgVarRefExp);

			for (auto j = var_ref_list.begin(); j != var_ref_list.end(); j++) {
				SageInterface::replaceExpression(isSgVarRefExp(*j), SageBuilder::buildVarRefExp(test_map[isSgBinaryOp(*i)->get_lhs_operand()->unparseToString()]));
			}
		}
	}
}

void ILoopTransformer::TransformIndex(SgForStatement* statement, std::map<std::string, std::string> index_map, std::vector<SgVariableDeclaration*> subdomain_variables, std::vector<std::string> domain_size_list) {

	// Scenario one: if the index variable is an assignment
	Rose_STL_Container<SgNode*> assign_list = NodeQuery::querySubTree(statement->get_loop_body(), V_SgAssignOp);

	for (auto i = assign_list.begin(); i != assign_list.end(); i++) {
		if (isSgAssignOp(*i)->get_lhs_operand() != NULL) {
			auto p = index_map.find(isSgAssignOp(*i)->get_lhs_operand()->unparseToString());

			if (p != index_map.end()) {
				Rose_STL_Container<SgNode*> multiply_list = NodeQuery::querySubTree(isSgAssignOp(*i), V_SgMultiplyOp);

				this->UpdateIndexAssignment(multiply_list, index_map, subdomain_variables, domain_size_list);
			}
		}
	}

	// Scenario two: if the index variable is a declaration
	Rose_STL_Container<SgNode*> declaration_list = NodeQuery::querySubTree(statement->get_loop_body(), V_SgVariableDeclaration);

	for (auto i = declaration_list.begin(); i != declaration_list.end(); i++) {
		auto p = index_map.find(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString());

		if (p != index_map.end()) {
			Rose_STL_Container<SgNode*> multiply_list = NodeQuery::querySubTree(isSgVariableDeclaration(*i), V_SgMultiplyOp);

			this->UpdateIndexAssignment(multiply_list, index_map, subdomain_variables, domain_size_list);
		}
	}

	// Scenario three: if the index variable is a function call
	Rose_STL_Container<SgNode*> function_call_list = NodeQuery::querySubTree(statement->get_loop_body(), V_SgFunctionCallExp);

	for (auto i = function_call_list.begin(); i != function_call_list.end(); i++) {

		if (isSgExpression(isSgFunctionCallExp(*i)->get_function()) != NULL) {
			auto p = index_map.find(isSgExpression(isSgFunctionCallExp(*i)->get_function())->unparseToString());

			if (p != index_map.end()) {
				if (isSgFunctionCallExp(*i)->get_args() != NULL) {
					SgExpressionPtrList expression_list = isSgExprListExp(isSgFunctionCallExp(*i)->get_args())->get_expressions();

					for (auto j = expression_list.begin(); j != expression_list.end(); j++) {

						if ((*j)->variantT() == V_SgVarRefExp) {
							auto it = std::find(domain_size_list.begin(), domain_size_list.end(), isSgVarRefExp(*j)->unparseToString());

							if (it != domain_size_list.end()) {
								auto index = std::distance(domain_size_list.begin(), it);
								SageInterface::replaceExpression(isSgVarRefExp(*j), SageBuilder::buildVarRefExp(subdomain_variables.at(index)));
							}
						}
					}
				}
			}
		}
	}
}

void ILoopTransformer::UpdateIndexAssignment(Rose_STL_Container<SgNode*> multiply_list, std::map<std::string, std::string> index_map, std::vector<SgVariableDeclaration*> subdomain_variables, std::vector<std::string> domain_size_list) {

	for (auto i = multiply_list.begin(); i != multiply_list.end(); i++) {

		if (isSgMultiplyOp(*i)->get_lhs_operand() != NULL && isSgMultiplyOp(*i)->get_lhs_operand()->variantT() == V_SgVarRefExp) {

			if (isSgMultiplyOp(*i)->get_rhs_operand() != NULL) {
				Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(isSgExpression(isSgMultiplyOp(*i)->get_rhs_operand()), V_SgVarRefExp);

				for (auto j = var_ref_list.begin(); j != var_ref_list.end(); j++) {

					bool IsPresent = (std::find(domain_size_list.begin(), domain_size_list.end(), isSgVarRefExp(*j)->unparseToString()) != domain_size_list.end());

					if (IsPresent) {
						SageInterface::replaceExpression(isSgVarRefExp(*j), SageBuilder::buildVarRefExp(subdomain_variables.front()));
					}
				}
			}
		}

		Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(isSgExpression(isSgMultiplyOp(*i)->get_rhs_operand()), V_SgVarRefExp);

		for (auto j = var_ref_list.begin(); j != var_ref_list.end(); j++) {
			bool IsPresent = (std::find(domain_size_list.begin(), domain_size_list.end(), isSgVarRefExp(*j)->unparseToString()) != domain_size_list.end());

			if (IsPresent) {
				SageInterface::replaceExpression(isSgVarRefExp(*j), SageBuilder::buildVarRefExp(subdomain_variables.at(1)));
			}
		}
	}
}


std::map<std::string, std::string> ILoopTransformer::CollectIndexDeclaration(SgForStatement* statement, std::vector<std::string> index_name_list) {

	Rose_STL_Container<SgNode*> declaration_list = NodeQuery::querySubTree(statement->get_loop_body(), V_SgLocatedNode);
	std::vector<std::string> variable_name_list;
	std::map<std::string, std::string> index_map;

	for (auto i = declaration_list.begin(); i != declaration_list.end(); i++) {
		if (isSgLocatedNode(*i)->variantT() == V_SgExprStatement) {
			if (isSgExprStatement(*i)->get_expression() != NULL) {
				if (isSgExprStatement(*i)->get_expression()->variantT() == V_SgAssignOp) {
					if (isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_lhs_operand() != NULL) {
						variable_name_list.push_back(isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_lhs_operand()->unparseToString());
					}
				}
			}
		}

		if (isSgLocatedNode(*i)->variantT() == V_SgVariableDeclaration) {
			variable_name_list.push_back(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString());
		}

		if (isSgLocatedNode(*i)->variantT() == V_SgFunctionCallExp) {
			if (isSgExpression(isSgFunctionCallExp(*i)->get_function()) != NULL) {
				variable_name_list.push_back(isSgExpression(isSgFunctionCallExp(*i)->get_function())->unparseToString());
			}
		}
	}

	for (auto i = variable_name_list.begin(); i != variable_name_list.end(); i++) {

		if (std::find(index_name_list.begin(), index_name_list.end(), (*i)) != index_name_list.end()) {
			index_map[(*i)] = (*i);
		}
	}

	return index_map;
}

std::vector<std::string> ILoopTransformer::FindIndexVariable(SgForStatement* statement) {

	Rose_STL_Container<SgNode*> array_list = NodeQuery::querySubTree(statement, V_SgPntrArrRefExp);
	std::vector<std::string> index_variable_list;

	for (auto i = array_list.begin(); i != array_list.end(); i++) {
		if (isSgPntrArrRefExp(*i)->get_rhs_operand() != NULL) {
			if (isSgPntrArrRefExp(*i)->get_rhs_operand()->variantT() == V_SgVarRefExp) {
				index_variable_list.push_back(isSgVarRefExp(isSgPntrArrRefExp(*i)->get_rhs_operand())->unparseToString());
			}

			if (isSgPntrArrRefExp(*i)->get_rhs_operand()->variantT() == V_SgFunctionCallExp) {
				index_variable_list.push_back(isSgExpression(isSgFunctionCallExp(isSgPntrArrRefExp(*i)->get_rhs_operand())->get_function())->unparseToString());
			}
		}
	}

	// Remove duplicates
	index_variable_list.erase(std::unique(index_variable_list.begin(), index_variable_list.end()), index_variable_list.end());

	return index_variable_list;
}

bool ILoopTransformer::IsInnerMostLoop(SgForStatement* for_statement) {

	Rose_STL_Container<SgNode*> ref_list = NodeQuery::querySubTree(for_statement->get_loop_body(), V_SgForStatement);

	if (ref_list.size() == 0) {
		return true;
	}

	return false;
}

SgForStatement* ILoopTransformer::OuterMostLoop() {
	return this->loop_statements().front();
}

} //namespace
