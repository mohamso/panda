#include "initialize_loop_transformer.h"

namespace loop
{

void InitializeLoopTransformer::TransformLoopBody(domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager) {

	std::vector<SgVariableDeclaration*> subdomain_variables = decomposer->subdomain_variables();
	std::vector<SgVariableDeclaration*> start_variable_list = decomposer->start_variable()->variables();
	std::vector<std::string> domain_size_list = pragma_manager->domain_size_list();
	std::vector<SgForStatement*> loop_list = this->loop_statements();
	std::vector<SgForStatement*> inner_most_loop_list;

	for (auto i = loop_list.begin(); i != loop_list.end(); i++) {

		if (this->IsInnerMostLoop(isSgForStatement(*i))) {
			inner_most_loop_list.push_back(*i);
		}
	}

	std::map<std::string, std::string> index_map;
	std::vector<std::string> index_name_list;

	// Find and transform the index variable
	for (auto i = inner_most_loop_list.begin(); i != inner_most_loop_list.end(); i++) {
		 index_name_list = this->FindIndexVariable(isSgForStatement(*i));
		 index_map = this->CollectIndexDeclaration(isSgForStatement(*i), index_name_list);
		 this->TransformIndex(isSgForStatement(*i), index_map, subdomain_variables, domain_size_list);
	}

	// Update references to the init variables
	std::map<int,std::vector<SgVariableDeclaration*>> init_variable_map = this->InsertStartVariables(start_variable_list, loop_list);
	std::map<std::string, std::string> init_start_map;

	for (auto i = inner_most_loop_list.begin(), j = init_variable_map.begin(); i != inner_most_loop_list.end(), j != init_variable_map.end(); i++, j++) {
		init_start_map = this->MapInitVariablesToStartVariables(j->second);
		this->UpdateInitReferences(j->second, init_start_map, index_name_list, isSgForStatement(*i));
	}

	// Replace loop test variables with p_i, p_j, etc variables
	for (auto i = inner_most_loop_list.begin(); i != inner_most_loop_list.end(); i++) {
		this->CollectLoopTestVariables(isSgForStatement(*i), init_start_map);
	}

	// Replace references to the original domain with subdomain
	for (auto i = inner_most_loop_list.begin(); i != inner_most_loop_list.end(); i++) {
		Rose_STL_Container<SgNode*> pntr_array = NodeQuery::querySubTree(isSgForStatement(*i)->get_loop_body(), V_SgPntrArrRefExp);

		for (auto j = pntr_array.begin(); j != pntr_array.end(); j++) {
			if (isSgPntrArrRefExp(*j)->get_rhs_operand() != NULL) {

				Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(isSgPntrArrRefExp(*j)->get_rhs_operand(), V_SgVarRefExp);

				for (auto k = var_ref_list.begin(); k != var_ref_list.end(); k++) {
					auto it = std::find(domain_size_list.begin(), domain_size_list.end(), isSgVarRefExp(*k)->unparseToString());

					if (it != domain_size_list.end()) {
						auto index = std::distance(domain_size_list.begin(), it);
						SageInterface::replaceExpression(isSgVarRefExp(*k), SageBuilder::buildVarRefExp(subdomain_variables.at(index)));
					}
				}
			}
		}
	}
}

void InitializeLoopTransformer::CollectLoopTestVariables(SgForStatement* for_loop, std::map<std::string, std::string> init_start_map) {

	Rose_STL_Container<SgNode*> located_node_list = NodeQuery::querySubTree(for_loop->get_loop_body(), V_SgExprStatement);

	std::map<std::string, std::string>::iterator p;

	for (auto i = located_node_list.begin(); i != located_node_list.end(); i++) {

		if (isSgExprStatement(*i)->get_expression() != NULL) {

			if (isSgExprStatement(*i)->get_expression()->variantT() != V_SgAssignOp) {
				this->ReplaceLoopTestVariablesWithPVariables(isSgExprStatement(*i)->get_expression(), init_start_map);
			}
		}
	}
}

std::map<std::string, std::string> InitializeLoopTransformer::MapInitVariablesToStartVariables(std::vector<SgVariableDeclaration*> init_variable_list) {

	// Create a map between init variables and start variables
	std::map<std::string, std::string> init_start_map;
	std::map<std::string, std::string>::iterator p;

	for (auto i = init_variable_list.begin(); i != init_variable_list.end(); i++) {

		Rose_STL_Container<SgNode*> assign_list = NodeQuery::querySubTree(isSgVariableDeclaration(*i), V_SgAddOp);

		for (auto j = assign_list.begin(); j != assign_list.end(); j++) {
			init_start_map[isSgVarRefExp(isSgAddOp(*j)->get_lhs_operand())->unparseToString()] = isSgVariableDeclaration(*i)->get_variables().front()->unparseToString();
		}
	}

	return init_start_map;
}

void InitializeLoopTransformer::UpdateInitReferences(std::vector<SgVariableDeclaration*> init_variable_list, std::map<std::string, std::string> init_start_map, std::vector<std::string> index_name_list, SgForStatement* for_loop) {

	Rose_STL_Container<SgNode*> statement_list = NodeQuery::querySubTree(for_loop->get_loop_body(), V_SgExprStatement);

	for (auto i = statement_list.begin(); i != statement_list.end(); i++) {
		if (isSgExprStatement(*i)->get_expression() != NULL) {
			if (isSgExprStatement(*i)->get_expression()->variantT() == V_SgAssignOp) {
				if (isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_lhs_operand() != NULL) {

					bool IsPresent = (std::find(index_name_list.begin(), index_name_list.end(), isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_lhs_operand()->unparseToString()) != index_name_list.end());

					if (IsPresent) {
						statement_list.erase(i);
					}
				}
			}
		}
	}

	for (auto i = statement_list.begin(); i != statement_list.end(); i++) {
		if (isSgExprStatement(*i)->get_expression() != NULL) {
			if (isSgExprStatement(*i)->get_expression()->variantT() == V_SgAssignOp) {
				if (isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_lhs_operand() != NULL) {
					if (isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_lhs_operand()->variantT() == V_SgPntrArrRefExp) {

						bool IsPresent = (std::find(index_name_list.begin(), index_name_list.end(), isSgPntrArrRefExp(isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_lhs_operand())->get_rhs_operand()->unparseToString()) != index_name_list.end());

						if (IsPresent) {
							this->ReplaceLoopTestVariablesWithPVariables(isSgAssignOp(isSgExprStatement(*i)->get_expression())->get_rhs_operand(), init_start_map);
						}
					}
				}
			}
		}
	}

	// If we have variable declarations that reference i, j, k,
	// they must be replaced with p_i, etc.
	Rose_STL_Container<SgNode*> declaration_list = NodeQuery::querySubTree(for_loop->get_loop_body(), V_SgVariableDeclaration);

	for (auto i = declaration_list.begin(); i != declaration_list.end();) {

		bool IsPresent = (std::find(index_name_list.begin(), index_name_list.end(), isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()) != index_name_list.end());

		if (IsPresent) {
			declaration_list.erase(i);
		}
		else {
			++i;
		}
	}

	for (auto i = declaration_list.begin(); i != declaration_list.end();) {

		for (auto j = init_variable_list.begin(); j != init_variable_list.end(); ++j) {
			if (isSgVariableDeclaration(*i) == isSgVariableDeclaration(*j)) {
				i = declaration_list.erase(i);
			}
			else {
				++i;
			}
		}
	}

	// Note: make sure that the index variable is not replaced
	for (auto i = declaration_list.begin(); i != declaration_list.end(); i++) {

		Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(isSgVariableDeclaration(*i), V_SgVarRefExp);

		for (auto j = var_ref_list.begin(); j != var_ref_list.end(); j++) {
			this->ReplaceLoopTestVariablesWithPVariables(isSgVarRefExp(*j), init_start_map);
		}
	}
}

void InitializeLoopTransformer::ReplaceLoopTestVariablesWithPVariables(SgExpression* rhs_expression, std::map<std::string, std::string> init_start_map) {

	Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(rhs_expression, V_SgVarRefExp);

	std::map<std::string, std::string>::iterator p;

	for (auto i = var_ref_list.begin(); i != var_ref_list.end(); i++) {
		p = init_start_map.find(isSgVarRefExp(*i)->unparseToString());

		if (p != init_start_map.end()) {
			SageInterface::replaceExpression(isSgVarRefExp(*i), SageBuilder::buildVarRefExp(init_start_map[isSgVarRefExp(*i)->unparseToString()]));
		}
	}
}

std::map<int,std::vector<SgVariableDeclaration*>> InitializeLoopTransformer::InsertStartVariables(std::vector<SgVariableDeclaration*> start_variable_list, std::vector<SgForStatement*> loop_list) {

	std::vector<SgExpression*> init_list;
	std::map<int,std::vector<SgExpression*>> init_map;
	std::map<int,std::vector<SgVariableDeclaration*>> init_variable_map;

	// Collect for loop init variables
	int counter = 0;

	for (auto i = loop_list.begin(); i != loop_list.end(); i++) {

		if (isSgForStatement(*i)->get_init_stmt().front()->variantT() == V_SgExprStatement) {
			init_list.push_back(isSgAssignOp(isSgExprStatement(isSgForStatement(*i)->get_init_stmt().front())->get_expression())->get_lhs_operand());
		}

		if (isSgForStatement(*i)->get_init_stmt().front()->variantT() == V_SgVariableDeclaration) {

			init_list.push_back(SageBuilder::buildVarRefExp(isSgVariableDeclaration(isSgForStatement(*i)->get_init_stmt().front())->get_variables().front()->unparseToString()));
		}

		if (this->IsInnerMostLoop(isSgForStatement(*i))) {
			init_map[counter] = init_list;
			init_list.clear();
		}

		counter++;
	}

	counter = 0;

	for (auto i = init_map.begin(); i != init_map.end(); i++) {
		init_variable_map[counter] = this->CollectIndexVariables(i->second, start_variable_list);
		counter++;
	}

	counter = 0;

	for (auto i = loop_list.begin(); i != loop_list.end(); i++) {

		if (this->IsInnerMostLoop(isSgForStatement(*i))) {

			SageBuilder::pushScopeStack(isSgScopeStatement(isSgForStatement(*i)->get_loop_body()));

			SgStatement* first_statement = SageInterface::getFirstStatement(isSgScopeStatement(isSgForStatement(*i)->get_loop_body()));

			for (auto j = init_variable_map[counter].begin(); j != init_variable_map[counter].end(); j++) {
				SageInterface::insertStatementBefore(first_statement, isSgVariableDeclaration(*j));
			}

			SageBuilder::popScopeStack();

			counter++;
		}
	}

	return init_variable_map;
}

std::vector<SgVariableDeclaration*> InitializeLoopTransformer::CollectIndexVariables(std::vector<SgExpression*> init_expression_list, std::vector<SgVariableDeclaration*> start_variable_list) {

	std::vector<SgVariableDeclaration*> variable_list;

	for(auto i = init_expression_list.rbegin(), j = start_variable_list.begin(); i != init_expression_list.rend(), j != start_variable_list.end(); i++, j++) {
		variable_list.push_back(this->CreateIndexVariable(isSgExpression(*i), isSgVariableDeclaration(*j)));
	}

	return variable_list;
}

SgVariableDeclaration* InitializeLoopTransformer::CreateIndexVariable(SgExpression* init_variable, SgVariableDeclaration* start_variable) {

	SgExpression* initial_expression = SageBuilder::buildAddOp(init_variable, SageBuilder::buildVarRefExp(start_variable));

	SgVariableDeclaration* variable_declaration = SageBuilder::buildVariableDeclaration("p_"+init_variable->unparseToString(), SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(initial_expression));

	return variable_declaration;
}


} // namespace
