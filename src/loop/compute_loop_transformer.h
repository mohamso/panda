#ifndef PANDA_LOOP_COMPUTELOOPTRANSFORMER_H
#define PANDA_LOOP_COMPUTELOOPTRANSFORMER_H

#include "iloop_transformer.h"

namespace loop
{
class ComputeLoopTransformer : public ILoopTransformer {
public:

	// Getter
	std::vector<SgForStatement*> const &stencil_loops() const { return stencil_loop_list_; }
	std::vector<std::string> index_names() const { return index_name_list_; }

	// Setter
	void set_stencil_loops(const std::vector<SgForStatement*> &value) { stencil_loop_list_ = value; }
	void set_index_name_list(std::vector<std::string> value) { index_name_list_ = value; }

	ComputeLoopTransformer() {}

	// Functions
	void ComputeInteriorPoints(domain::Decomposer* decomposer);

	std::vector<SgForStatement*> CollectComputeLoops(std::vector<SgForStatement*> loop_list, std::vector<std::string> domain_size_list);
	void TransformLoopTest(domain::Decomposer* decomposer);
	void TransformLoopBody(domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager);

	~ComputeLoopTransformer() {}

private:

	bool IsStencilLoop(SgForStatement* for_loop, std::vector<std::string> domain_size_list);

	std::vector<SgForStatement*> stencil_loop_list_;
	std::vector<std::string> index_name_list_;
};

} // namespace loop

#endif // PANDA_LOOP_COMPUTELOOPTRANSFORMER_H
