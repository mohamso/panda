#include "compute_loop_transformer.h"

namespace loop
{

/**
 * @brief This functions updates the loop iterators (tests)
 * so that only the interior points of the subdomain is computed.
 *
 * @param[in]	decomposer	Global domain partitioner
 */
void ComputeLoopTransformer::ComputeInteriorPoints(domain::Decomposer* decomposer) {

  std::cout << "Panda: Updating compute loops...";

  // Change start value to 2 i.e. int k,j,i = 1 -> int k,j,i = 2
  for (auto i = this->stencil_loops().begin(); i != this->stencil_loops().end(); i++) {

    auto init_stmt = isSgForStatement(*i)->get_for_init_stmt();

    if (init_stmt != NULL) {
      Rose_STL_Container<SgNode*> var_decl = NodeQuery::querySubTree(init_stmt,  V_SgAssignInitializer);
      for (auto j = var_decl.begin(); j != var_decl.end(); j++) {
        isSgAssignInitializer(*j)->set_operand(SageBuilder::buildIntVal(2));
      }
    } else {
      std::cerr << "Panda: Could not get compute loop init statement" << std::endl;
      exit(1);
    }

    // Change the test value so that we stop at < nsdz,nsdy,nsdx
    auto test_exp = isSgForStatement(*i)->get_test();

    if (test_exp != NULL) {
      std::vector<SgVarRefExp*> exp_list;
      Rose_STL_Container<SgNode*> node_list = NodeQuery::querySubTree(test_exp, V_SgLessThanOp);

      for (auto j = node_list.begin(); j != node_list.end(); j++) {
        Rose_STL_Container<SgNode*> var_refs = NodeQuery::querySubTree(isSgExpression(isSgLessThanOp(*j)->get_rhs_operand()), V_SgVarRefExp);
        for (auto k = var_refs.begin(); k != var_refs.end(); k++) {
          exp_list.push_back(isSgVarRefExp(*k));
        }
      }

      for (auto j = node_list.begin(), k = exp_list.begin(); j != node_list.end(), k != exp_list.end(); j++, k++) {
        isSgLessThanOp(*j)->set_rhs_operand(isSgVarRefExp(*k));
      }
    } else {
      std::cerr << "Panda: Could not get compute loop test statement" << std::endl;
      exit(1);
    }

  }

  std::cout << "done" << std::endl;
}

void ComputeLoopTransformer::TransformLoopTest(domain::Decomposer* decomposer) {

  std::cout << "Panda: Transforming compute loop...";

  std::vector<SgForStatement*> loop_list = this->loop_statements();
  std::vector<std::string> domain_size_list;
  std::map<SgVarRefExp*, SgVariableDeclaration*> domain_size_map = decomposer->domain_size_geometry_map();

  for (auto i = domain_size_map.begin(); i != domain_size_map.end(); i++) {
    domain_size_list.push_back(i->first->unparseToString());
  }

  std::vector<SgForStatement*> compute_loop_list = this->CollectComputeLoops(loop_list, domain_size_list);
  this->set_stencil_loops(compute_loop_list);

  std::vector<SgBinaryOp*> loop_test_list;

  for (auto i = compute_loop_list.begin(); i != compute_loop_list.end(); i++) {
    loop_test_list.push_back(isSgBinaryOp(isSgExprStatement((*i)->get_test())->get_expression()));
  }

  std::map<std::string, SgVariableDeclaration*> test_map;

  for (auto i = compute_loop_list.begin(), j = decomposer->subdomain_variables().rbegin(); i != loop_list.end(), j != decomposer->subdomain_variables().rend(); i++, j++) {
    test_map[isSgBinaryOp(isSgExprStatement((*i)->get_test())->get_expression())->get_lhs_operand()->unparseToString()] = isSgVariableDeclaration(*j);
  }

  for (auto i = loop_test_list.begin(); i != loop_test_list.end(); i++) {

    auto p = test_map.find(isSgBinaryOp(*i)->get_lhs_operand()->unparseToString());

    if (p != test_map.end()) {

      Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(isSgBinaryOp(*i)->get_rhs_operand(), V_SgVarRefExp);

      for (auto j = var_ref_list.begin(); j != var_ref_list.end(); j++) {
        SageInterface::replaceExpression(isSgVarRefExp(*j), SageBuilder::buildVarRefExp(test_map[isSgBinaryOp(*i)->get_lhs_operand()->unparseToString()]));
      }
    }
  }

  std::cout << "done" << std::endl;
}

std::vector<SgForStatement*> ComputeLoopTransformer::CollectComputeLoops(std::vector<SgForStatement*> loop_list, std::vector<std::string> domain_size_list) {

  // Eliminate the loop that is not a stencil loop from the loop list
  for (auto i = loop_list.begin(); i != loop_list.end();) {

    if (this->IsStencilLoop(isSgForStatement(*i), domain_size_list) == false) {
      loop_list.erase(i);
    } else {
      i++;
    }
  }

  return loop_list;
}

bool ComputeLoopTransformer::IsStencilLoop(SgForStatement* for_loop, std::vector<std::string> domain_size_list) {

  // Get test statement
  SgExprStatement* test = isSgExprStatement(for_loop->get_test());

  // Get the binary op expression
  SgBinaryOp* binary_op = isSgBinaryOp(test->get_expression());

  // Check if it contains domain size
  Rose_STL_Container<SgNode*> variable_list = NodeQuery::querySubTree(binary_op->get_rhs_operand_i(), V_SgVarRefExp);

  for (auto j = variable_list.begin(); j != variable_list.end(); j++) {
    bool IsPresent = (std::find(domain_size_list.begin(), domain_size_list.end(), (*j)->unparseToString()) != domain_size_list.end());

    if (IsPresent) {
      return true;
    }
  }

  return false;
}

void ComputeLoopTransformer::TransformLoopBody(domain::Decomposer* decomposer, translator::PragmaManager* pragma_manager) {

  std::vector<SgForStatement*> loop_list = this->loop_statements();
  std::vector<SgForStatement*> inner_most_loop_list;

  for (auto i = loop_list.begin(); i != loop_list.end(); i++) {

    if (this->IsInnerMostLoop(isSgForStatement(*i))) {
      inner_most_loop_list.push_back(*i);
    }
  }

  std::map<std::string, std::string> index_map;
  std::vector<std::string> index_name_list;
  std::vector<SgVariableDeclaration*> subdomain_variables = decomposer->subdomain_variables();
  std::vector<std::string> domain_size_list = pragma_manager->domain_size_list();

  // Find and transform the index variable
  for (auto i = inner_most_loop_list.begin(); i != inner_most_loop_list.end(); i++) {
    index_name_list = this->FindIndexVariable(isSgForStatement(*i));
    index_map = this->CollectIndexDeclaration(isSgForStatement(*i), index_name_list);
    this->TransformIndex(isSgForStatement(*i), index_map, subdomain_variables, domain_size_list);
  }

  // Set Index name list
  this->set_index_name_list(index_name_list);

  // Update references to original domain size to the new domain size
  for (auto i = inner_most_loop_list.begin(); i != inner_most_loop_list.end(); i++) {

    if (isSgForStatement(*i)->get_loop_body() != NULL) {

      Rose_STL_Container<SgNode*> array_list = NodeQuery::querySubTree(isSgForStatement(*i)->get_loop_body(), V_SgPntrArrRefExp);

      for (auto j = array_list.begin(); j != array_list.end(); j++) {
        if (isSgPntrArrRefExp(*j)->get_rhs_operand() != NULL) {
          Rose_STL_Container<SgNode*> var_ref_list = NodeQuery::querySubTree(isSgForStatement(*i)->get_loop_body(), V_SgVarRefExp);

          for (auto k = var_ref_list.begin(); k != var_ref_list.end(); k++) {

            auto it = std::find(domain_size_list.begin(), domain_size_list.end(), isSgVarRefExp(*k)->unparseToString());

            if (it != domain_size_list.end()) {
              auto index = std::distance(domain_size_list.begin(), it);
              SageInterface::replaceExpression(isSgVarRefExp(*k), SageBuilder::buildVarRefExp(subdomain_variables.at(index)));
            }
          }
        }
      }
    }
  }
}

} // namespace
