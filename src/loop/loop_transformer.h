#ifndef PANDA_LOOP_LOOPTRANSFORMER_H
#define PANDA_LOOP_LOOPTRANSFORMER_H

#include "../panda.h"

namespace loop
{
class LoopTransformer {
public:
  LoopTransformer() {}

  void TransformLoopIteratorsOrDie(std::vector<SgForStatement*> for_loops, std::map<std::string, std::string> size_map);
  void TransformLoopBody(SgStatement* for_loop_body, std::map<std::string, std::string> size_map);

  ~LoopTransformer() {}
};

} // namespace loop

#endif // PANDA_LOOP_LOOPTRANSFORMER_H
