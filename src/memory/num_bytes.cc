#include "num_bytes.h"

namespace buffer
{

SgVariableDeclaration* NumBytes::CreateNumBytesVariableDeclaration(SgGlobal* global_scope) {
  return SageBuilder::buildVariableDeclaration(this->direction()->name()+"_num_bytes", SageBuilder::buildOpaqueType("size_t", global_scope), this->CreateAssignInitializer());
}

SgAssignInitializer* NumBytes::CreateAssignInitializer() {
	if (this->direction()->IsFront() || this->direction()->IsBack()) {

		auto x_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(this->direction()->plane_one()), SageBuilder::buildIntVal(2));
		auto y_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(this->direction()->plane_two()), SageBuilder::buildIntVal(2));
		auto xy_plane = SageBuilder::buildMultiplyOp(x_plane, y_plane);

		return SageBuilder::buildAssignInitializer(SageBuilder::buildMultiplyOp(SageBuilder::buildSizeOfOp(this->type()), xy_plane));

	} else {
		auto plane = SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(this->direction()->plane_one()), SageBuilder::buildVarRefExp(this->direction()->plane_two()));
		return SageBuilder::buildAssignInitializer(SageBuilder::buildMultiplyOp(SageBuilder::buildSizeOfOp(this->type()), plane));
	}
}


}
