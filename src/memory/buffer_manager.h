#ifndef PANDA_BUFFER_BUFFERMANAGER_H
#define PANDA_BUFFER_BUFFERMANAGER_H

#include "buffer.h"
#include "num_bytes.h"

#include "../mpi/mpi_finalize.h"
#include "../domain/domain_manager.h"
#include "../variables/direction.h"

namespace buffer
{
  class BufferManager {
  public:

    // Getter
    std::vector<NumBytes*> const &num_bytes() const { return num_bytes_list_; }
    std::vector<RootBuffer*> const &send_buffers() const { return send_buffers_list_; }
    std::vector<RootBuffer*> const &recv_buffers() const { return recv_buffers_list_; }

    // Setter
    void set_num_bytes(const std::vector<NumBytes*> &value) { num_bytes_list_ = value; }
    void set_send_buffers(const std::vector<RootBuffer*> &value) { send_buffers_list_ = value; }
    void set_recv_buffers(const std::vector<RootBuffer*> &value) { recv_buffers_list_ = value; }

    // Functions
    void InsertSendRecvBuffers(domain::DomainManager* domain_manager);

    void CollectNumBytesDeclarations(SgProject* project, domain::DomainManager* domain_manager, variables::Direction* direction_variable);

    void FreeHostBuffer(mpi::MPIFinalize* mpi_finalize, SgScopeStatement* scope);

    ~BufferManager() {}

  private:

    std::vector<NumBytes*> num_bytes_list_;
    std::vector<RootBuffer*> send_buffers_list_;
    std::vector<RootBuffer*> recv_buffers_list_;
    std::vector<SgVariableDeclaration*> num_bytes_declarations_;
};

} // namespace buffer

#endif // PANDA_BUFFER_BUFFERMANAGER_H

