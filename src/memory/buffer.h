#ifndef PANDA_BUFFER_BUFFER_H
#define PANDA_BUFFER_BUFFER_H

#include "../variables/direction.h"
#include "num_bytes.h"

namespace buffer
{
  class RootBuffer {
  public:
	  virtual std::string prefix() const = 0;
	  virtual std::string name() const = 0;

	  virtual bool IsSendBuffer() const { return false; }
	  virtual bool IsRecvBuffer() const { return false; }

	  // Getter
	  SgVariableDeclaration* variable() const { return variable_; }

	  // Setter
	  void set_variable(SgVariableDeclaration* value) { this->variable_ = value; }

	  SgVariableDeclaration* CreateBufferVariableDeclaration(NumBytes* num_bytes);

	  virtual ~RootBuffer();

  private:
	  virtual SgExpression* CreateMallocExpression(SgType* type, SgVariableDeclaration* num_bytes);
	  SgVariableDeclaration* variable_;
  };

  class SendBuffer: public RootBuffer {

	  std::string prefix() const { return "_send_buffer"; }
	  std::string name() const { return "send_buffer"; }

	  bool IsSendBuffer() const { return true; }
	  bool IsRecvBuffer() const { return false; }

  };

  class RecvBuffer: public RootBuffer {

	  std::string prefix() const { return "_recv_buffer"; }
	  std::string name() const { return "recv_buffer"; }

	  bool IsSendBuffer() const { return false; }
	  bool IsRecvBuffer() const { return true; }

  };

} // namespace buffer

#endif //PANDA_BUFFER_BUFFER_H
