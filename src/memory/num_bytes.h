#ifndef PANDA_BUFFER_NUM_BYTES_H
#define PANDA_BUFFER_NUM_BYTES_H

#include "../variables/direction.h"

namespace buffer
{
  class NumBytes {
  public:

	NumBytes(SgType* type, variables::RootDirection* direction) {
		this->type_ = type;
		this->direction_ = direction;
	}

	// Getter
	SgType* type() { return type_; }
	variables::RootDirection* direction() const { return direction_; }
	SgVariableDeclaration* variable() const { return variable_; }

    void set_variable(SgVariableDeclaration* value) { this->variable_ = value; }

    // Functions
    SgVariableDeclaration* CreateNumBytesVariableDeclaration(SgGlobal* global_scope);


    ~NumBytes() {}

  private:
    SgAssignInitializer* CreateAssignInitializer();

    SgType* type_;
    SgVariableDeclaration* variable_;

    variables::RootDirection* direction_;
};

} // namespace buffer

#endif // PANDA_BUFFER_NUM_BYTES_H

