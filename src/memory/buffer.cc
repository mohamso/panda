#include "buffer.h"

namespace buffer {

RootBuffer::~RootBuffer() {}


SgVariableDeclaration* RootBuffer::CreateBufferVariableDeclaration(NumBytes* num_bytes) {

	return SageBuilder::buildVariableDeclaration(num_bytes->direction()->name()+this->prefix(), num_bytes->type(),
			SageBuilder::buildAssignInitializer(this->CreateMallocExpression(num_bytes->type(), num_bytes->variable())));
}

SgExpression* RootBuffer::CreateMallocExpression(SgType* type, SgVariableDeclaration* num_bytes) {

	SgExpression* malloc_expression = SageBuilder::buildFunctionCallExp("malloc", SageBuilder::buildVoidType(), SageBuilder::buildExprListExp(SageBuilder::buildVarRefExp(num_bytes)), NULL);
	SgExpression* cast_expression = SageBuilder::buildCastExp(malloc_expression, type);

	return cast_expression;
}

} // namespace
