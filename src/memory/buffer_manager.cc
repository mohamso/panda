#include "buffer_manager.h"

namespace buffer
{

void BufferManager::InsertSendRecvBuffers(domain::DomainManager* domain_manager) {

  std::cout << "Panda: Generating send/recv buffers..";

  // 1. Create Send/RecvBuffer objects
  std::vector<RootBuffer*> send_buffer_list;
  std::vector<RootBuffer*> recv_buffer_list;
  std::vector<SgVariableDeclaration*> num_bytes_var_decls;

  for (auto i = this->num_bytes().begin(); i != this->num_bytes().end(); i++) {
    send_buffer_list.push_back(new SendBuffer());
    recv_buffer_list.push_back(new RecvBuffer());
    NumBytes* num_bytes = (*i);

    if (!num_bytes->direction()->IsFront() && !num_bytes->direction()->IsBack()) {
      num_bytes_var_decls.push_back(num_bytes->variable());
    }
  }

  // 2. Generate & insert Send/RecvBuffer variable declarations
  for (auto i = send_buffer_list.begin(), j = recv_buffer_list.begin(), k = this->num_bytes().begin();
       i != send_buffer_list.end(), j != recv_buffer_list.end(), k != this->num_bytes().end(); i++, j++, k++) {
    RootBuffer* send_buffer = (*i);
    RootBuffer* recv_buffer = (*j);
    NumBytes* num_bytes = (*k);

    if (!num_bytes->direction()->IsFront() && !num_bytes->direction()->IsBack()) {
      SgVariableDeclaration* send_variable = send_buffer->CreateBufferVariableDeclaration(num_bytes);
      SgVariableDeclaration* recv_variable = recv_buffer->CreateBufferVariableDeclaration(num_bytes);
      send_buffer->set_variable(send_variable);
      recv_buffer->set_variable(recv_variable);
      SageInterface::insertStatementAfter(domain_manager->domain_variables().back(), send_variable);
      SageInterface::insertStatementAfter(send_variable, recv_variable);
    }
  }

  // 3. Insert num_bytes variable declarations
  for (auto i = num_bytes_var_decls.begin(); i != num_bytes_var_decls.end(); i++) {
    SageInterface::insertStatementAfter(domain_manager->domain_variables().back(), isSgVariableDeclaration(*i));
  }


  this->set_recv_buffers(recv_buffer_list);
  this->set_send_buffers(send_buffer_list);

  std::cout << "done" << std::endl;
}

void BufferManager::CollectNumBytesDeclarations(SgProject* project, domain::DomainManager* domain_manager, variables::Direction* direction_variable) {

  std::cout << "Panda: Computing buffer bytes..";

  // Connect nsdx && nsdy [a plane] to front and back
  // nsdy && nsdz to east and west etc.
  direction_variable->ConnectPlanesToDirections(domain_manager);

  /* The subdomain variable not connected belongs to a plane
   * belongs to the boundary functions (unpack/pack)
   * It essentially connects e.g.
   * nsdz in "int k = p < 1 ? nsdz : 1;"
   */
  direction_variable->BindBoundaryDirectionVariable(domain_manager);

  // Fetch type from unew_sd
  SgVariableDeclaration* domain_variable = domain_manager->domain_variables().front();
  SgType* type = domain_variable->get_definition()->get_type();

  // Create NumBytesObjects
  std::vector<NumBytes*> num_bytes_list;

  for (auto i = direction_variable->directions().begin(); i != direction_variable->directions().end(); i++) {
    variables::RootDirection* direction = (*i);
    num_bytes_list.push_back(new NumBytes(type, direction));
  }

  this->set_num_bytes(num_bytes_list);

  SgGlobal* global_scope = SageInterface::getFirstGlobalScope(project);

  std::vector<SgVariableDeclaration*> num_bytes_declaration_list;

  for (auto i = num_bytes_list.begin(); i != num_bytes_list.end(); i++) {
    NumBytes* num_bytes = (*i);
    auto variable = num_bytes->CreateNumBytesVariableDeclaration(global_scope);
    num_bytes->set_variable(variable);
  }

  std::cout << "done" << std::endl;
}

/**
 * @brief A function that collects all send/recv buffers registered by the
 * buffer manager. Moreover, an individual free function call is generated
 * for each collected buffer.
 *
 * Each free function call is then inserted before MPI_Finalize();
 *
 * @param[in] mpi_finalize The insertion point of the free function calls
 * @param[in] scope  Scope object that defines the scope of the insertion point
 */
void BufferManager::FreeHostBuffer(mpi::MPIFinalize* mpi_finalize, SgScopeStatement* scope) {

  std::cout << "Panda: Freeing Allocated Memory...";

  std::vector<SgVariableDeclaration*> variable_list;

  for (auto i = this->recv_buffers().begin(); i != this->recv_buffers().end(); i++) {
    buffer::RootBuffer* recv_buffer = (*i);
    SgVariableDeclaration* recv_buffer_var = isSgVariableDeclaration(recv_buffer->variable());

    if (recv_buffer_var != NULL) {
      variable_list.push_back(recv_buffer_var);
    }
  }

  for (auto i = this->send_buffers().begin(); i != this->send_buffers().end(); i++) {
    buffer::RootBuffer* send_buffer = (*i);
    SgVariableDeclaration* send_buffer_var = isSgVariableDeclaration(send_buffer->variable());

    if (send_buffer_var != NULL) {
      variable_list.push_back(send_buffer_var);
    }
  }

  for (auto i = variable_list.begin(); i != variable_list.end(); i++) {
    SgStatement* free_statement = SageBuilder::buildFunctionCallStmt("free", SageBuilder::buildVoidType(), SageBuilder::buildExprListExp(SageBuilder::buildVarRefExp(isSgVariableDeclaration(*i))));
    SageInterface::insertStatementBefore(mpi_finalize->statement(), free_statement, scope);
  }

  variable_list.clear();

  std::cout << "done" << std::endl;
}

} // namespace
