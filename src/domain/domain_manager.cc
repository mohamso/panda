#include "domain_manager.h"

namespace domain
{

void DomainManager::TransformGlobalDomain(translator::PragmaManager* pragma_manager) {

	std::vector<std::string> domain_name_list = pragma_manager->domain_name_list();
	std::map<std::string, SgVariableDeclaration*>::iterator p;

	for (auto i = domain_name_list.begin(); i != domain_name_list.end(); i++) {

		p = this->domain_name_map().find((*i));

		if (p != this->domain_name_map().end()) {

			// Transform names
			if (this->domain_name_map()[(*i)]->get_variables().front()->variantT() == V_SgInitializedName) {

				isSgInitializedName(this->domain_name_map()[(*i)]->get_variables().front())->set_name((*i)+"_sd");
			}
		}
	}
}

void DomainManager::TransformGlobalDomainSize(SgProject* project, translator::PragmaManager* pragma_manager) {

	std::map<std::string, std::vector<SgVarRefExp*>> size_map = this->domain_to_domain_size_map();
	std::vector<SgVariableDeclaration*> subdomain_variable_list = this->decomposer()->subdomain_variables();

	for (auto i = size_map.begin(); i != size_map.end(); i++) {
		for (auto j = i->second.begin(), k = subdomain_variable_list.begin(); j != i->second.end(), k != subdomain_variable_list.end(); j++, k++) {
			SageInterface::replaceExpression(isSgVarRefExp((*j)), SageBuilder::buildVarRefExp(*k));
		}
	}
}

std::vector<std::string> DomainManager::CreateSubdomainNameList(translator::PragmaManager* pragma_manager) {

	std::vector<std::string> domain_name_list = pragma_manager->domain_name_list();
	std::vector<std::string> subdomain_name_list;

	for (auto i = domain_name_list.begin(); i != domain_name_list.end(); i++) {
		subdomain_name_list.push_back((*i)+"_sd");
	}

	return subdomain_name_list;
}

std::vector<SgVariableDeclaration*> DomainManager::CollectSubdomainVariables(SgProject* project) {

	Rose_STL_Container<SgNode*> variable_declaration_list = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	std::vector<std::string> subdomain_names = this->subdomain_name_list();
	std::vector<SgVariableDeclaration*> subdomain_variable_list;

	for (auto i = variable_declaration_list.begin(); i != variable_declaration_list.end(); i++) {

		bool IsPresent = std::find(subdomain_names.begin(), subdomain_names.end(), isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()) != subdomain_names.end();

		if (IsPresent) {
			subdomain_variable_list.push_back(isSgVariableDeclaration(*i));
		}
	}

	return subdomain_variable_list;
}

std::map<std::string, SgVariableDeclaration*> DomainManager::MapDomainVariableDeclarations(SgProject* project, translator::PragmaManager* pragma_manager) {

	std::map<std::string, SgVariableDeclaration*> domain_map;
	std::vector<std::string> domain_name_list = pragma_manager->domain_name_list();

	Rose_STL_Container<SgNode*> domain_declaration_list = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	for (auto i = domain_declaration_list.begin(); i != domain_declaration_list.end(); i++) {

		bool IsPresent = std::find(domain_name_list.begin(), domain_name_list.end(), isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()) != domain_name_list.end();

		if (IsPresent) {
			domain_map[isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()] = isSgVariableDeclaration(*i);
		}
	}

	return domain_map;
}

std::map<std::string, std::vector<SgVarRefExp*>> DomainManager::MapDomainToDomainSize(translator::PragmaManager* pragma_manager) {

	std::vector<std::string> name_list = pragma_manager->domain_name_list();
	std::vector<std::string> size_list = pragma_manager->domain_size_list();

	std::map<std::string, SgExpression*> domain_malloc_map = this->domain_malloc_map();
	std::map<std::string, SgExpression*>::iterator p;

	std::map<std::string, std::vector<SgVarRefExp*>> domain_size_map;

	for (auto i = name_list.begin(); i != name_list.end(); i++) {

		p = domain_malloc_map.find(*i);

		if (p != domain_malloc_map.end()) {

			Rose_STL_Container<SgNode*> ref_list = NodeQuery::querySubTree(isSgExpression(domain_malloc_map[*i]), V_SgVarRefExp);

			for (auto j = ref_list.begin(); j != ref_list.end(); j++) {
				domain_size_map[(*i)].push_back(isSgVarRefExp((*j)));
			}
		}
	}

	return domain_size_map;
}

std::map<std::string, SgExpression*> DomainManager::MapDomainMallocExpressions(SgProject* project) {

	std::map<std::string, SgVariableDeclaration*> domain_name_map = this->domain_name_map();
	std::map<std::string, SgVariableDeclaration*>::iterator p;

	std::map<std::string, SgExpression*> domain_malloc_map;

	Rose_STL_Container<SgNode*> query_list = NodeQuery::querySubTree(project, V_SgStatement);

	for(auto i = query_list.begin(); i != query_list.end(); i++) {

		if ((*i)->variantT() == V_SgExprStatement) {
			if (isSgExpression(isSgExprStatement(*i)->get_expression())->variantT() == V_SgAssignOp) {
				SgAssignOp* assign_op = isSgAssignOp(isSgExpression(isSgExprStatement(*i)->get_expression()));

				p = domain_name_map.find(assign_op->get_lhs_operand()->unparseToString());

				if (p != domain_name_map.end()) {
					if (assign_op->get_rhs_operand()->variantT() == V_SgCastExp) {
						domain_malloc_map[assign_op->get_lhs_operand()->unparseToString()] = assign_op->get_rhs_operand();
					}
				}
			}
		}

		if ((*i)->variantT() == V_SgVariableDeclaration) {
			p = domain_name_map.find(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString());

			if (p != domain_name_map.end()) {
				Rose_STL_Container<SgNode*> assign_list = NodeQuery::querySubTree(isSgVariableDeclaration(*i), V_SgAssignInitializer);

				for(auto j = assign_list.begin(); j != assign_list.end(); j++) {
					domain_malloc_map[isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()] = isSgExpression(*j);
				}
			}
		}
	}

	return domain_malloc_map;
}

std::vector<SgVariableDeclaration*> DomainManager::DomainVariableDeclaration(SgProject* project, translator::PragmaManager* pragma_manager) {

	std::vector<std::string> domain_name_list = pragma_manager->domain_name_list();
	std::vector<SgVariableDeclaration*> domain_list;

	Rose_STL_Container<SgNode*> domain_declaration_list = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	for (auto i = domain_declaration_list.begin(); i != domain_declaration_list.end(); i++) {

		bool IsPresent = std::find(domain_name_list.begin(), domain_name_list.end(), isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()) != domain_name_list.end();

		if (IsPresent) {
			domain_list.push_back(isSgVariableDeclaration(*i));
		}
	}

	return domain_list;
}

void DomainManager::Initialize() {
	this->decomposer_ = new Decomposer();
}

} // namespace
