#include "decomposer.h"

namespace domain
{
void Decomposer::InsertGeometryVariables(SgProject* project) {

	std::vector<SgVariableDeclaration*> variable_list = this->geometry()->CollectGeometryVariables();
	this->geometry()->set_variables(variable_list);

	SgFunctionDeclaration* main_function = SageInterface::findMain(project);
	SgBasicBlock* main_function_body = main_function->get_definition()->get_body();

	if (isSgScopeStatement(main_function_body)) {
		SageBuilder::pushScopeStack(main_function_body);

		SgStatement* first_statement = SageInterface::getFirstStatement(SageBuilder::topScopeStack());

		for(auto i = variable_list.begin(); i != variable_list.end(); i++) {
			SageInterface::insertStatementBefore(first_statement, (*i));
		}

		SageBuilder::popScopeStack();
	}
}

void Decomposer::InsertRankMappingVariables(SgProject* project, translator::PragmaManager* pragma_manager, variables::Rank* rank_object) {

	std::vector<SgVariableDeclaration*> variable_list = rank_object->CollectRankMappingVariables(this->geometry()->variables());

	// Insert after the declaration point of the last domain size declaration
	std::vector<std::string> domain_size_list = pragma_manager->domain_size_list();
	std::vector<SgVariableDeclaration*> last_variable_list;
	Rose_STL_Container<SgNode*> declaration_list = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	for (auto i = declaration_list.begin(); i != declaration_list.end(); i++) {

		bool IsPresent = (std::find(domain_size_list.begin(), domain_size_list.end(), isSgVariableDeclaration(*i)->get_variables().front()->unparseToString()) != domain_size_list.end());

		if (IsPresent) {
			last_variable_list.push_back(isSgVariableDeclaration(*i));
		}
	}

	for (auto i = variable_list.rbegin(); i != variable_list.rend(); i++) {
		SageInterface::insertStatementAfter(isSgVariableDeclaration(last_variable_list.back()), isSgVariableDeclaration(*i));
	}
}

std::map<SgVariableDeclaration*, std::string> Decomposer::BindRankMappingVariablesToDomainSize(variables::Rank* rank_object, translator::PragmaManager* pragma_manager) {

	std::vector<SgVariableDeclaration*> rank_variable_list = rank_object->rank_mapping_variables();

	std::vector<std::string> domain_size_list = pragma_manager->domain_size_list();

	std::map<SgVariableDeclaration*, std::string> rank_domain_size_map;

	for (auto i = domain_size_list.begin(), j = rank_variable_list.begin(); i != domain_size_list.end(), j != rank_variable_list.end(); i++, j++) {

		rank_domain_size_map[isSgVariableDeclaration(*j)] = (*i);
	}

	return rank_domain_size_map;
}

std::vector<SgVariableDeclaration*> Decomposer::InsertStartMappingVariables(variables::Rank* rank_object, std::map<SgVariableDeclaration*, std::string> rank_domain_size_map) {

	std::vector<SgVariableDeclaration*> start_variable_list = this->start_variable()->CollectStartMappingVariables(rank_domain_size_map, this->geometry());
	this->start_variable()->set_variables(start_variable_list);

    for(auto i = start_variable_list.rbegin(); i != start_variable_list.rend(); i++) {
      SageInterface::insertStatementAfter(rank_object->rank_mapping_variables().back(), isSgVariableDeclaration(*i));
    }

	return start_variable_list;
}

void Decomposer::InsertGeometryVariableCommandLineInterface(SgProject* project) {

	std::vector<std::string> argument_list = this->GetMainFunctionArguments(project);

	SgFunctionDeclaration* main_function = SageInterface::findMain(project);

	SgScopeStatement* scope = main_function->get_scope();

	SgForStatement* for_loop = this->CreateForloopStatement(argument_list.front());

	std::vector<SgExprStatement*> strmp_expression_list = this->CollectStrcmpExpressions(this->geometry()->variables(), argument_list.back(), scope);
	std::vector<SgIfStmt*> if_statement_list = this->CollectIfStatements(strmp_expression_list);
	std::vector<SgExprStatement*> atoi_statement_list = this->CollectAtoiStatements(this->geometry()->variables(), argument_list.back(), scope);

	if_statement_list = this->InsertIntoIfStatement(if_statement_list, atoi_statement_list);
	for_loop = this->InsertIfStatementsIntoForloop(for_loop, if_statement_list);

	SageInterface::insertStatementAfter(this->geometry()->variables().back(), for_loop);

	// Check if #include <stdlib.h> (includes atoi) is included or not
	// If not included, include it
	if (!this->IsStdlibHeaderIncluded(project)) {
		SageInterface::insertHeader("stdlib.h", PreprocessingInfo::after, true, SageInterface::getFirstGlobalScope(project));
	}
}

bool Decomposer::IsStdlibHeaderIncluded(SgProject* project) {

	  Rose_STL_Container<SgNode*> test_list = NodeQuery::querySubTree(project, V_SgLocatedNode);
	  std::vector<std::string> header_include_list;

	  for (auto i = test_list.begin(); i != test_list.end(); i++) {
		  if (isSgLocatedNode(*i) != NULL) {

			  AttachedPreprocessingInfoType* comments = isSgLocatedNode(*i)->getAttachedPreprocessingInfo();

			  if (comments != NULL) {

				  for (auto i = comments->rbegin(); i != comments->rend(); i++) {

					  if ((*i)->getTypeOfDirective() == PreprocessingInfo::CpreprocessorIncludeDeclaration) {
						  header_include_list.push_back((*i)->getString());
					  }
				  }
			  }
		  }
	  }

	  for (auto i = header_include_list.begin(); i != header_include_list.end(); i++) {
		  if (std::string(*i).find("stdlib.h") != std::string::npos) {
			  return true;
		  }
	  }

	  return false;
}

std::map<SgVarRefExp*, SgVariableDeclaration*> Decomposer::MapDomainSizeToProcessorGeometry(translator::PragmaManager* pragma_manager) {

	std::vector<std::string> domain_size_list = pragma_manager->domain_size_list();

	std::vector<SgVariableDeclaration*> p_variable_list = this->geometry()->variables();
	std::map<SgVarRefExp*, SgVariableDeclaration*> domain_size_geometry_map;

	for (auto i = domain_size_list.begin(), j = p_variable_list.begin(); i != domain_size_list.end(), j != p_variable_list.end(); i++, j++) {
		domain_size_geometry_map[SageBuilder::buildVarRefExp(*i)] = isSgVariableDeclaration(*j);
	}

	return domain_size_geometry_map;
}

std::vector<SgVariableDeclaration*> Decomposer::InsertSubdomainSizeVariables(SgScopeStatement* scope, translator::PragmaManager* pragma_manager, variables::Rank* rank_object) {

	std::vector<SgVariableDeclaration*> rank_variable_list = rank_object->rank_mapping_variables();

	std::vector<SgExpression*> expression_list = this->subdomain_size()->CollectAssignInitializerExpression(this->domain_size_geometry_map(), rank_variable_list);
	std::vector<SgVariableDeclaration*> variable_list = this->subdomain_size()->CollectSubdomainSizeVariables(expression_list, scope);
	this->subdomain_size()->set_variables(variable_list);

	std::vector<SgVariableDeclaration*> start_variables_list = this->start_variable()->variables();

	// Insert after start_x, start_y, start_z
	for (auto i = variable_list.rbegin(); i != variable_list.rend(); i++) {
		SageInterface::insertStatementAfter(isSgVariableDeclaration(start_variables_list.back()), isSgVariableDeclaration(*i));
	}

	return variable_list;
}

std::vector<std::string> Decomposer::GetMainFunctionArguments(SgProject* project) {

	std::vector<std::string> argument_list;

	SgFunctionDeclaration* main_function = SageInterface::findMain(project);
	SgInitializedNamePtrList main_argument_list = main_function->get_args();

	for (SgInitializedNamePtrList::iterator i = main_argument_list.begin(); i != main_argument_list.end(); i++) {
		SgSymbol* symbol = (*i)->get_symbol_from_symbol_table();
		argument_list.push_back(symbol->get_name().getString());
	}

	return argument_list;
}

SgForStatement* Decomposer::InsertIfStatementsIntoForloop(SgForStatement* for_loop, std::vector<SgIfStmt*> if_statement_list) {

	SgScopeStatement* loop_scope = SageInterface::getScope(for_loop);
	SgStatement* loop_body = SageInterface::getLoopBody(loop_scope);
	SageBuilder::pushScopeStack(isSgScopeStatement(loop_body));

	for (std::vector<SgIfStmt*>::iterator i = if_statement_list.begin(); i != if_statement_list.end(); i++) {
		SgIfStmt* if_statement = (*i);

		ROSE_ASSERT(if_statement != NULL);

		SageInterface::appendStatement(if_statement);
	}

	SageBuilder::popScopeStack();

	return for_loop;
}

SgForStatement* Decomposer::CreateForloopStatement(std::string argc_name) {

	SgStatement* init_statement = SageBuilder::buildVariableDeclaration("i", SageBuilder::buildIntType(),
			SageBuilder::buildAssignInitializer(SageBuilder::buildIntVal(0)));

	SgStatement* condition_statement = SageBuilder::buildExprStatement(
			SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp("i"), SageBuilder::buildVarRefExp(argc_name)));

	SgExpression* increment_expression = SageBuilder::buildPlusPlusOp(SageBuilder::buildVarRefExp("i"), SgUnaryOp::postfix);

	SgBasicBlock* loop_body = SageBuilder::buildBasicBlock();

	SgForStatement* for_statement = SageBuilder::buildForStatement(init_statement, condition_statement, increment_expression, loop_body);

	return for_statement;
}

std::vector<SgIfStmt*> Decomposer::InsertIntoIfStatement(std::vector<SgIfStmt*> if_statement_list, std::vector<SgExprStatement*> atoi_statement_list) {

	std::vector<SgIfStmt*>::iterator i;
	std::vector<SgExprStatement*>::iterator j;

	for(i = if_statement_list.begin(), j = atoi_statement_list.begin(); i != if_statement_list.end(), j != atoi_statement_list.end(); i++, j++) {
		SgIfStmt* if_statement = (*i);
		SgExprStatement* atoi_statement = (*j);
		SageInterface::appendStatement(atoi_statement, isSgScopeStatement(if_statement->get_true_body()));
	}

	return if_statement_list;
}

std::vector<SgIfStmt*> Decomposer::CollectIfStatements(std::vector<SgExprStatement*> strmp_expression_list) {

	std::vector<SgIfStmt*> if_statement_list;

	for (std::vector<SgExprStatement*>::iterator i = strmp_expression_list.begin(); i != strmp_expression_list.end(); i++) {
		SgExprStatement* expression = (*i);
		if_statement_list.push_back(this->CreateIfStatement(expression));
	}

	return if_statement_list;
}

SgIfStmt* Decomposer::CreateIfStatement(SgExprStatement* expression) {

	SgBasicBlock* true_body = SageBuilder::buildBasicBlock();

	SgIfStmt* if_statement = SageBuilder::buildIfStmt(expression, true_body, NULL);

	return if_statement;
}

std::vector<SgExprStatement*> Decomposer::CollectStrcmpExpressions(std::vector<SgVariableDeclaration*> variable_list, std::string argv, SgScopeStatement* project) {

	std::vector<SgExprStatement*> expression_statement_list;

	for (std::vector<SgVariableDeclaration*>::iterator i = variable_list.begin(); i != variable_list.end(); i++) {
		SgVariableDeclaration* variable_declaration = (*i);
		expression_statement_list.push_back(this->CreateStrcmpExpression(variable_declaration, argv, project));
	}

	return expression_statement_list;
}

SgExprStatement* Decomposer::CreateStrcmpExpression(SgVariableDeclaration* variable_declaration, std::string argv, SgScopeStatement* scope) {

	SgExpression* argv_expression = SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(argv), SageBuilder::buildVarRefExp("i"));
	SgExpression* p_expression = SageBuilder::buildVarRefExp("\"-"+variable_declaration->get_variables().front()->unparseToString()+"\"");

	SgExprListExp* argument_list = SageBuilder::buildExprListExp();
	SageInterface::appendExpression(argument_list, argv_expression);
	SageInterface::appendExpression(argument_list, p_expression);

	SgExprStatement* expression_statement = SageBuilder::buildFunctionCallStmt("!strcmp", SageBuilder::buildVoidType(), argument_list, scope);

	return expression_statement;
}

std::vector<SgExprStatement*> Decomposer::CollectAtoiStatements(std::vector<SgVariableDeclaration*> variable_list, std::string argv, SgScopeStatement* scope) {

	std::vector<SgExprStatement*> atoi_statements;

	for(std::vector<SgVariableDeclaration*>::iterator i = variable_list.begin(); i != variable_list.end(); i++) {
		SgVariableDeclaration* variable_declaration = (*i);
		atoi_statements.push_back(this->CreateAtoiStatement(variable_declaration, argv, scope));
	}

	return atoi_statements;
}

SgExprStatement* Decomposer::CreateAtoiStatement(SgVariableDeclaration* variable_declaration, std::string argv, SgScopeStatement* scope) {

	SgExpression* lhs_expression = SageBuilder::buildVarRefExp(variable_declaration);

	SgExpression* argv_expression = SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(argv),
			SageBuilder::buildAddOp(SageBuilder::buildVarRefExp("i"), SageBuilder::buildIntVal(1)));

	SgExprListExp* argument_list = SageBuilder::buildExprListExp();
	SageInterface::appendExpression(argument_list, argv_expression);

	SgExpression* rhs_expression = SageBuilder::buildFunctionCallExp("atoi", SageBuilder::buildVoidType(), argument_list, scope);

	SgExprStatement* statement = SageBuilder::buildAssignStatement(lhs_expression, rhs_expression);

	return statement;
}

void Decomposer::Initialize() {
	this->geometry_ = new variables::Geometry();
	this->subdomain_size_ = new variables::SubdomainSize();
	this->start_ = new variables::Start();
}

} // namespace
