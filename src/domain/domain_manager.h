#ifndef PANDA_DOMAIN_DOMAINMANAGER_H
#define PANDA_DOMAIN_DOMAINMANAGER_H

#include "../panda.h"
#include "decomposer.h"
#include "../translator/pragma_manager.h"

#include <map>

namespace domain
{
  class DomainManager {
  public:

    DomainManager() { this->Initialize(); }

    // Getter
    Decomposer* decomposer() const { return decomposer_; }

    std::map<std::string, SgVariableDeclaration*> domain_name_map() const { return domain_name_map_; }

    std::map<std::string, SgExpression*> domain_malloc_map() const { return domain_malloc_map_; }
    std::map<std::string, std::vector<SgVarRefExp*>> domain_to_domain_size_map() const { return domain_to_domain_size_map_; }
    std::vector<SgVariableDeclaration*> domain_variables() const { return domain_variable_list_; }
    std::vector<SgVariableDeclaration*> subdomain_variables() const { return subdomain_variable_list_; }
	std::vector<std::string> subdomain_name_list() const { return subdomain_name_list_; }

    // Setter
    void set_domain_name_map(std::map<std::string, SgVariableDeclaration*> value) { domain_name_map_ = value; }
    void set_domain_malloc_map(std::map<std::string, SgExpression*> value) { domain_malloc_map_ = value; }
    void set_domain_to_domain_size_map(std::map<std::string, std::vector<SgVarRefExp*>> value) { domain_to_domain_size_map_ = value; }
    void set_domain_variables(std::vector<SgVariableDeclaration*> value) { domain_variable_list_ = value; }

    void set_subdomain_variables(std::vector<SgVariableDeclaration*> value) { subdomain_variable_list_ = value; }
    void set_subdomain_name_list(std::vector<std::string> value) { subdomain_name_list_ = value; }

    // Functions
	std::map<std::string, SgVariableDeclaration*> MapDomainVariableDeclarations(SgProject* project, translator::PragmaManager* pragma_manager);
	std::map<std::string, SgExpression*> MapDomainMallocExpressions(SgProject* project);
	std::map<std::string, std::vector<SgVarRefExp*>> MapDomainToDomainSize(translator::PragmaManager* pragma_manager);

	std::vector<SgVariableDeclaration*> DomainVariableDeclaration(SgProject* project, translator::PragmaManager* pragma_manager);
	std::vector<std::string> CreateSubdomainNameList(translator::PragmaManager* pragma_manager);
	std::vector<SgVariableDeclaration*> CollectSubdomainVariables(SgProject* project);

	void TransformGlobalDomain(translator::PragmaManager* pragma_manager);
	void TransformGlobalDomainSize(SgProject* project, translator::PragmaManager* pragma_manager);

    ~DomainManager() {}

  private:
    void Initialize();

    std::map<std::string, SgVariableDeclaration*> domain_name_map_;
    std::map<std::string, SgExpression*> domain_malloc_map_;
    std::map<std::string, std::vector<SgVarRefExp*>> domain_to_domain_size_map_;
    std::vector<std::string> subdomain_name_list_;

    std::vector<SgVariableDeclaration*> domain_variable_list_;
    std::vector<SgVariableDeclaration*> subdomain_variable_list_;

    Decomposer* decomposer_;
};

} // namespace domain

#endif // PANDA_DOMAIN_DOMAINMANAGER_H

