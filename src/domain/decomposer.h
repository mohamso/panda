#ifndef PANDA_DOMAIN_DECOMPOSER_H
#define PANDA_DOMAIN_DECOMPOSER_H

#include "../panda.h"
#include "../translator/pragma_manager.h"
#include "../variables/geometry.h"
#include "../variables/subdomain_size.h"
#include "../variables/rank.h"
#include "../variables/start.h"
#include <map>

namespace domain
{
  class Decomposer {
  public:
    
    Decomposer() { this->Initialize(); }

    // Getter
    variables::Geometry* geometry() const { return geometry_; }
    variables::SubdomainSize* subdomain_size() const { return subdomain_size_; }
    variables::Start* start_variable() const { return start_; }

    std::map<SgVarRefExp*, SgVariableDeclaration*> domain_size_geometry_map() const { return domain_size_geometry_map_; }
    std::vector<SgVariableDeclaration*> const &subdomain_variables() const { return subdomain_variable_list_; }

    // Setter
    void set_geometry(variables::Geometry* geometry) { geometry_ = geometry; }
    void set_subdomain_size(variables::SubdomainSize* subdomain_size) { subdomain_size_ = subdomain_size; }
    void set_start(variables::Start* value) { start_ = value; }

    void set_domain_size_geometry_map(std::map<SgVarRefExp*, SgVariableDeclaration*> value) { domain_size_geometry_map_ = value; }
    void set_subdomain_variables(const std::vector<SgVariableDeclaration*> &value) { subdomain_variable_list_ = value; }

    // Functions
    void InsertGeometryVariables(SgProject* project);
    std::vector<SgVariableDeclaration*> InsertSubdomainSizeVariables(SgScopeStatement* scope, translator::PragmaManager* pragma_manager, variables::Rank* rank_object);
    std::map<SgVarRefExp*, SgVariableDeclaration*> MapDomainSizeToProcessorGeometry(translator::PragmaManager* pragma_manager);
    void InsertRankMappingVariables(SgProject* project, translator::PragmaManager* pragma_manager, variables::Rank* rank_object);
    std::map<SgVariableDeclaration*, std::string> BindRankMappingVariablesToDomainSize(variables::Rank* rank_object, translator::PragmaManager* pragma_manager);
    std::vector<SgVariableDeclaration*> InsertStartMappingVariables(variables::Rank* rank_object, std::map<SgVariableDeclaration*, std::string> rank_domain_size_map);
    void InsertGeometryVariableCommandLineInterface(SgProject* project);
    std::vector<std::string> GetMainFunctionArguments(SgProject* project);
    SgForStatement* InsertIfStatementsIntoForloop(SgForStatement* for_loop, std::vector<SgIfStmt*> if_statement_list);
    SgForStatement* CreateForloopStatement(std::string argc_name);
    std::vector<SgIfStmt*> InsertIntoIfStatement(std::vector<SgIfStmt*> if_statement_list, std::vector<SgExprStatement*> atoi_statement_list);
    std::vector<SgIfStmt*> CollectIfStatements(std::vector<SgExprStatement*> strmp_expression_list);
    SgIfStmt* CreateIfStatement(SgExprStatement* expression);
    std::vector<SgExprStatement*> CollectStrcmpExpressions(std::vector<SgVariableDeclaration*> variable_list, std::string argv, SgScopeStatement* scope);
    SgExprStatement* CreateStrcmpExpression(SgVariableDeclaration* variable_declaration, std::string argv, SgScopeStatement* scope);
    std::vector<SgExprStatement*> CollectAtoiStatements(std::vector<SgVariableDeclaration*> variable_list, std::string argv, SgScopeStatement* scope);
    SgExprStatement* CreateAtoiStatement(SgVariableDeclaration* variable_declaration, std::string argv, SgScopeStatement* scope);

    ~Decomposer() {}

  private:
    // Functions
    bool IsStdlibHeaderIncluded(SgProject* project);

    variables::Geometry* geometry_;
    variables::SubdomainSize* subdomain_size_;
    variables::Start* start_;

    std::map<SgVarRefExp*, SgVariableDeclaration*> domain_size_geometry_map_;
    std::vector<SgVariableDeclaration*> subdomain_variable_list_;

    void Initialize();
};

} // namespace domain

#endif // PANDA_DOMAIN_DECOMPOSER_H

