#include "panda.h"

#include "./io/command_line_interface.h"
#include "./io/filemanager.h"
#include "./translator/modes.h"
#include "./translator/translator_manager.h"

int main(int argc, char* argv[])
{
  // Start the Command Line Interface (CLI)
  IO::CommandLineInterface* cli_object = new IO::CommandLineInterface();
  cli_object->StartOrDie(argc, argv);

  // Use CLI to interpret user mode
  // Die upon invalid mode
  translator::TranslatorMode* mode = cli_object->GetTranslatorMode(argc, argv);
  mode->VerifyModeOrDie();

  // Read the first command line argument to
  // check whether the input source file is a valid 
  // .c source file or not
  std::string input_source_file = argv[1];
  IO::FileManager* file_manager_object = new IO::FileManager();
  file_manager_object->ValidateInputSourceFileOrDie(input_source_file);

  // Now that we know that we are dealing with a valid 
  // input source code file, call the Rose front-end
  SgProject* project = frontend(argc, argv);

  // Read a single input source file
  SgSourceFile* sg_source_file_object = isSgSourceFile(project->get_fileList()[0]);

  // Validate the input source file
  SgFile* input_source_sgfile = isSgFile(sg_source_file_object);

  // Get the input source filename and its full path
  std::string filename_with_full_path = input_source_sgfile->get_file_info()->get_filenameString();

  // Use the filemanager to get the canonical filename
  // Set the concatenated filename of the output source code file
  // Set the filename for the project
  std:: string output_filename = file_manager_object->CanonizeInputFilename(filename_with_full_path);
  file_manager_object->set_output_filename(output_filename);
  file_manager_object->SetTranslatedFilename(input_source_sgfile);

  // Call the Translator Manager and start translating the project
  SgFunctionDeclaration* main_function = SageInterface::findMain(project);
  SgScopeStatement* scope = isSgScopeStatement(main_function->get_scope());

  translator::TranslatorManager* translator_manager = new translator::TranslatorManager(project, scope, mode);
  translator_manager->Translate();

  // Cleanup
  file_manager_object->DeleteFiles();

  std::cout << "Panda: Code sucessfully translated." << std::endl;

  project->unparse();

  std::cout << "Panda: Writing translated code to disk." << std::endl;

  return 0;
}
