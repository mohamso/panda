#include "mpi_request.h"

namespace mpi
{
  SgType* MPIRequest::CreateType(SgGlobal* global_scope) {
    SgType* type = SageBuilder::buildOpaqueType(this->type_name(), global_scope);

    return type;
  }

  SgVariableDeclaration* MPIRequest::CreateMPIRequestVariable(SgGlobal* global_scope) {

	SgType* type = this->CreateType(global_scope);
	this->set_type(type);
    
    SgVariableDeclaration* request_variable = 
    SageBuilder::buildVariableDeclaration(this->variable_name(), SageBuilder::buildArrayType(this->type(), SageBuilder::buildIntVal((int)this->directions()*2)));

    return request_variable;
  }

  SgExpression* MPIRequest::CreateMPIRequestAddressOfRequestCountExpression(SgVariableDeclaration* request_count_variable) {

    SgExpression* expression = SageBuilder::buildAddressOfOp(SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(this->variable()), 
      SageBuilder::buildVarRefExp(request_count_variable)));

    return expression;
  }

} // namespace
