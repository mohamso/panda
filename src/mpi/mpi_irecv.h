#ifndef PANDA_MPI_MPIIRECV_H_
#define PANDA_MPI_MPIIRECV_H_

#include "mpi_root_recv.h"

namespace mpi
{
class MPIIrecv : public RootMPIRecv {
  public:
	MPIIrecv() {
        this->set_communicator(new MPICommunicator());
        this->set_data_type(new MPIDouble());
        this->set_status(new MPIStatus("MPI_STATUS_IGNORE"));
    }

    // Getter
    std::string function_name() const { return "MPI_Irecv"; }

    ~MPIIrecv() {}
};

} // namespace mpi

#endif //PANDA_MPI_MPIIRECV_H_
