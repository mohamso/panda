#include "mpi_allreduce.h"

namespace mpi
{

/**
* @brief  Default constructur for MPIAllreduce.
* Automatically installs the default MPI Communicator
* which is MPI_COMM_WORLD.
*
*/
MPIAllreduce::MPIAllreduce() {
	this->communicator_ = new MPICommunicator();
}

/**
* @brief  A function that generates an MPIAllreduce call.
*
* MPI_Allreduce(&l2_uold, &global_l2, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
*
* @param[in]  local_var_name  local variable name
* @param[in]  global_var_name  global variable name
* @param[in]  op    MPIOp (e.g. MPI_SUM)
* @param[in]  data_type    MPIDatatype (e.g. MPI_DOUBLE)
* @param[out] statement Statement object that holds the created function call
*/
SgStatement* MPIAllreduce::CreateReduceStatement(const std::string& local_var_name, const std::string& global_var_name, MPIOp* op, MPIDatatype* data_type) {

    SgExprListExp* argument_list = SageBuilder::buildExprListExp();

    SageInterface::appendExpression(argument_list, SageBuilder::buildAddressOfOp(SageBuilder::buildVarRefExp(local_var_name)));
    SageInterface::appendExpression(argument_list, SageBuilder::buildAddressOfOp(SageBuilder::buildVarRefExp(global_var_name)));
    SageInterface::appendExpression(argument_list, SageBuilder::buildIntVal(1));
    SageInterface::appendExpression(argument_list, SageBuilder::buildVarRefExp(data_type->name()));
    SageInterface::appendExpression(argument_list, SageBuilder::buildVarRefExp(op->name()));
    SageInterface::appendExpression(argument_list, SageBuilder::buildVarRefExp(this->communicator()->name()));

	return SageBuilder::buildFunctionCallStmt("MPI_Allreduce", SageBuilder::buildVoidType(), argument_list);
}

} // namespace
