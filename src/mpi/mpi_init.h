#ifndef PANDA_MPI_MPIINIT_H_
#define PANDA_MPI_MPIINIT_H_

#include "../panda.h"

namespace mpi
{
class MPIInit {
public:
	// Getter
	SgExprStatement* statement() const { return statement_; }
	std::string function_name() const { return "MPI_Init"; }

	// Setter
	void set_statement(SgExprStatement* value) { statement_ = value; }

	// Functions
	SgExprStatement* CreateMPIInitStatement(SgProject* project);

	~MPIInit() {}

private:
	std::vector<SgName> GetMainFunctionArguments(SgFunctionDeclaration* main_function);
	SgExprStatement* statement_;

};

} // namespace mpi

#endif //PANDA_MPI_MPIINIT_H_
