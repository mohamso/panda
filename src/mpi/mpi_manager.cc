#include "mpi_manager.h"

namespace mpi
{
void MPIManager::CollectMPIReceiveStatements(buffer::BufferManager* buffer_manager, domain::Decomposer* decomposer, analyzer::Stencil* stencil) {

	std::cout << "Panda: Generating MPI_Irecvs...";

	// Create Create MPI_Irecv argument list, and
	// add recv_buffer_variable (dir<T>_recv_buffer) to the argument list
	std::vector<SgExprListExp*> expr_list;

	for (auto i = buffer_manager->recv_buffers().begin(), j = this->direction_variable()->directions().begin();
			i != buffer_manager->recv_buffers().end(), j != this->direction_variable()->directions().end(); i++, j++) {
		SgExprListExp* expr_list_exp = SageBuilder::buildExprListExp();
		buffer::RootBuffer* buffer = (*i);
		variables::RootDirection* direction = (*j);

		if(direction->IsFront()) {
			//unew_sd+(nsdz+1)*(nsdx+2)*(nsdy+2)
			auto x_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(direction->plane_one()), SageBuilder::buildIntVal(2));
			auto y_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(direction->plane_two()), SageBuilder::buildIntVal(2));
			auto xy_plane = SageBuilder::buildMultiplyOp(x_plane, y_plane);
			auto nsdz_plus_one = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(decomposer->subdomain_size()->nsdz()), SageBuilder::buildIntVal(1));
			SageInterface::appendExpression(expr_list_exp, SageBuilder::buildAddOp(stencil->write_array()->get_lhs_operand(), SageBuilder::buildMultiplyOp(nsdz_plus_one, xy_plane)));
		}
		else if (direction->IsBack()) {
			SageInterface::appendExpression(expr_list_exp, stencil->write_array()->get_lhs_operand());

		} else {
			SageInterface::appendExpression(expr_list_exp, SageBuilder::buildVarRefExp(buffer->variable()));
		}

		expr_list.push_back(expr_list_exp);
	}

	// Append (nsdx+2)*(nsdy+2) (count) and data_type (MPI_DOUBLE) to the argument list
	for (auto i = buffer_manager->num_bytes().begin(), j = expr_list.begin(); i != buffer_manager->num_bytes().end(), j != expr_list.end(); i++, j++) {
		buffer::NumBytes* num_bytes = (*i);
		SgExprListExp* expr_list_exp = (*j);
		SageInterface::appendExpression(expr_list_exp, this->mpi_recv_variable()->CreateCountExpression(num_bytes));
		SageInterface::appendExpression(expr_list_exp, SageBuilder::buildVarRefExp(this->mpi_recv_variable()->data_type()->name()));
	}

	// Append direction to the argument list
	std::vector<SgExpression*> direction_expression_list = this->direction_variable()->CollectDirectionExpressions();
	for (auto i = direction_expression_list.begin(), j = expr_list.begin(); i != direction_expression_list.end(), j != expr_list.end(); i++, j++) {
		SgExprListExp* expr_list_exp = (*j);
		SageInterface::appendExpression(expr_list_exp, isSgExpression(*i));
	}

	// Append tag, MPI_COMM_WORLD, &mpi_request[request_count] to the argument list
	// Create MPI_Irecv function calls
	SgExpression* mpirequest_expression = this->mpi_request_variable()->CreateMPIRequestAddressOfRequestCountExpression(this->request_count_variable()->variable());
	std::vector<SgExprStatement*> mpi_recv_statement_list;

	for (auto i = expr_list.begin(); i != expr_list.end(); i++) {
		SgExprListExp* expr_list_exp = (*i);
		SageInterface::appendExpression(expr_list_exp, SageBuilder::buildVarRefExp(this->tag_variable()->cpu_tag()));
		SageInterface::appendExpression(expr_list_exp, SageBuilder::buildVarRefExp(this->mpi_recv_variable()->communicator()->name()));
		SageInterface::appendExpression(expr_list_exp, mpirequest_expression);

		//std::cout << expr_list_exp->unparseToString() << std::endl;
		mpi_recv_statement_list.push_back(SageBuilder::buildFunctionCallStmt(this->mpi_recv_variable()->function_name(), SageBuilder::buildVoidType(), expr_list_exp));
	}

	this->mpi_recv_variable()->set_mpi_recv_statements(mpi_recv_statement_list);

	std::cout << "done" << std::endl;
}

void MPIManager::CollectMPIISendStatements(buffer::BufferManager* buffer_manager, domain::Decomposer* decomposer, analyzer::Stencil* stencil) {

	std::cout << "Panda: Generating MPI_Isends...";

    //MPI_Isend(unew_sd+(nsdz)*(nsdx+2)*(nsdy+2), (nsdx+2)*(nsdy+2), MPI_DOUBLE, direction[front], tag, MPI_COMM_WORLD, &mpi_request[request_count]); //front
    //MPI_Isend(unew_sd+1*(nsdx+2)*(nsdy+2), (nsdx+2)*(nsdy+2), MPI_DOUBLE, direction[back], tag, MPI_COMM_WORLD, &mpi_request[request_count]); // back
    //request_count++;

	std::vector<SgExprListExp*> expr_list;
	for (auto i = buffer_manager->send_buffers().begin(), j = this->direction_variable()->directions().begin(); i != buffer_manager->send_buffers().end(), j != this->direction_variable()->directions().end(); i++, j++) {
		buffer::RootBuffer* buffer = (*i);
		variables::RootDirection* direction = (*j);
		SgExprListExp* expr_list_exp = SageBuilder::buildExprListExp();

		auto x_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(direction->plane_one()), SageBuilder::buildIntVal(2));
		auto y_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(direction->plane_two()), SageBuilder::buildIntVal(2));
		auto xy_plane = SageBuilder::buildMultiplyOp(x_plane, y_plane);

		if (direction->IsBack()) {
			auto one_plus_xy_plane = SageBuilder::buildMultiplyOp(xy_plane, SageBuilder::buildIntVal(1));
			SageInterface::appendExpression(expr_list_exp, SageBuilder::buildAddOp(stencil->write_array()->get_lhs_operand(), one_plus_xy_plane));
			SageInterface::appendExpression(expr_list_exp, xy_plane);

		}
		else if (direction->IsFront()) {
			auto target_plus_nsdz = SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(decomposer->subdomain_size()->nsdz()), xy_plane);
			SageInterface::appendExpression(expr_list_exp, SageBuilder::buildAddOp(stencil->write_array()->get_lhs_operand(), target_plus_nsdz));
			SageInterface::appendExpression(expr_list_exp, xy_plane);
		} else {
			SageInterface::appendExpression(expr_list_exp, SageBuilder::buildVarRefExp(buffer->variable()));
			auto count = SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(direction->plane_one()), SageBuilder::buildVarRefExp(direction->plane_two()));
			SageInterface::appendExpression(expr_list_exp, count);
		}

		SageInterface::appendExpression(expr_list_exp, SageBuilder::buildVarRefExp(this->mpi_send_variable()->data_type()->name()));
		expr_list.push_back(expr_list_exp);
	}

	SgExpression* mpirequest_expression = this->mpi_request_variable()->CreateMPIRequestAddressOfRequestCountExpression(this->request_count_variable()->variable());

	// Append direction to the argument list
	std::vector<SgExpression*> direction_expression_list = this->direction_variable()->CollectDirectionExpressions();
	std::vector<SgExprStatement*> mpi_send_stmts;

	for (auto i = direction_expression_list.begin(), j = expr_list.begin(); i != direction_expression_list.end(), j != expr_list.end(); i++, j++) {
		SageInterface::appendExpression(isSgExprListExp(*j), isSgExpression(*i));
		SageInterface::appendExpression(isSgExprListExp(*j), SageBuilder::buildVarRefExp(this->tag_variable()->cpu_tag()));
		SageInterface::appendExpression(isSgExprListExp(*j), SageBuilder::buildVarRefExp(this->mpi_recv_variable()->communicator()->name()));
		SageInterface::appendExpression(isSgExprListExp(*j), mpirequest_expression);
		mpi_send_stmts.push_back(SageBuilder::buildFunctionCallStmt(this->mpi_send_variable()->function_name(), SageBuilder::buildVoidType(), isSgExprListExp(*j)));
	}

	this->mpi_send_variable()->set_mpi_send_statements(mpi_send_stmts);

	std::cout << "done" << std::endl;
}

void MPIManager::InsertMPIRequestVariable(SgProject* project) {

	std::cout << "Panda: Creating MPIRequest variable...";

	SgGlobal* global_scope = SageInterface::getFirstGlobalScope(project);

	this->mpi_request_variable()->set_number_of_directions(this->direction_variable()->number_of_directions());

	SgVariableDeclaration* mpi_request_variable = this->mpi_request_variable()->CreateMPIRequestVariable(global_scope);
	SgVariableDeclaration* request_count_variable = this->request_count_variable()->variable();

	SageInterface::insertStatementAfter(request_count_variable, mpi_request_variable);

	this->mpi_request_variable()->set_variable(mpi_request_variable);

	std::cout << "done" << std::endl;
}

void MPIManager::InsertHeader(SgProject* project) {
	std::cout << "Panda: Inserting <mpi.h>...";
	this->mpi_header_variable()->InsertMPIHeader(project);
	std::cout << "done" << std::endl;
}

void MPIManager::InsertFinalize(SgProject* project) {
	std::cout << "Panda: Inserting MPI_Finalize()...";
	SgExprStatement* finalize_statement = this->mpi_finalize_variable()->InsertMPIFinalize(project);
	this->mpi_finalize_variable()->set_statement(finalize_statement);
	std::cout << "done" << std::endl;
}

void MPIManager::InsertRequestCountVariable() {

	std::cout << "Panda: Creating RequestCount...";

	SgVariableDeclaration* request_count_variable = this->request_count_variable()->CreateRequestCountVariable();
	std::vector<SgVariableDeclaration*> tag_variable_list = this->tag_variable()->tag_variables();
	SageInterface::insertStatementAfter(tag_variable_list.back(), request_count_variable);

	this->request_count_variable()->set_variable(request_count_variable);

	std::cout << "done" << std::endl;
}

void MPIManager::InsertRequestCountStatement(SgForStatement* outer_loop) {

	SgExprStatement* request_count_statement = this->request_count_variable()->CreateRequestCountStatement();

	SgScopeStatement* scope_statement = SageInterface::getScope(outer_loop);

	if (isSgScopeStatement(scope_statement)) {
		SgStatement* loop_body = SageInterface::getLoopBody(scope_statement);
		SageBuilder::pushScopeStack(isSgScopeStatement(loop_body));
		SageInterface::prependStatement(request_count_statement);
		SageBuilder::popScopeStack();
	}

	this->request_count_variable()->set_statement(request_count_statement);
}

void MPIManager::InsertMPIInit(SgProject* project) {

	std::cout << "Panda: Inserting MPI_Init()...";
	SgExprStatement* mpi_init_statement = this->mpi_init_variable()->CreateMPIInitStatement(project);

	this->mpi_init_variable()->set_statement(mpi_init_statement);

	SageInterface::insertStatementAfter(this->size_variable()->variable(), mpi_init_statement);
	std::cout << "done" << std::endl;
}

void MPIManager::InsertMPICommRank(SgProject* project) {

	std::cout << "Panda: Inserting MPI_Comm_rank()...";

	SgExprStatement* comm_rank_statement = this->mpi_comm_rank_variable()->CreateMPICommRankStatement(project, this->rank_variable()->variable());
	this->mpi_comm_rank_variable()->set_statement(comm_rank_statement);

	SageInterface::insertStatementAfter(isSgExprStatement(this->mpi_init_variable()->statement()), isSgExprStatement(comm_rank_statement));

	std::cout << "done" << std::endl;
}

void MPIManager::InsertMPICommSize(SgProject* project) {

	std::cout << "Panda: Inserting MPI_Comm_size()...";

	SgExprStatement* comm_size_statement = this->mpi_comm_size_variable()->CreateMPICommSizeStatement(project, this->size_variable()->variable());
	this->mpi_comm_size_variable()->set_statement(comm_size_statement);

	SageInterface::insertStatementAfter(this->mpi_comm_rank_variable()->statement(), comm_size_statement);

	std::cout << "done" << std::endl;
}

void MPIManager::InsertSizeVariable(SgProject* project) {

	SgFunctionDeclaration* main_function = SageInterface::findMain(project);
	SgBasicBlock* main_function_body = main_function->get_definition()->get_body();
	SageBuilder::pushScopeStack(isSgScopeStatement(main_function_body));

	SgVariableDeclaration* size_variable = this->size_variable()->CreateSizeVariable();
	this->size_variable()->set_variable(size_variable);

	SgStatement* first_statement = SageInterface::getFirstStatement(SageBuilder::topScopeStack());
	SageInterface::insertStatementBefore(first_statement, size_variable);

	SageBuilder::popScopeStack();
}

void MPIManager::InsertRankVariable() {

	SgVariableDeclaration* rank_variable = this->rank_variable()->CreateRankVariable();
	this->rank_variable()->set_variable(rank_variable);
	SageInterface::insertStatementBefore(this->size_variable()->variable(), rank_variable);
}

void MPIManager::InsertTagVariables(bool IsHybrid) {

	std::cout << "Panda: Creating Tags...";

	std::vector<SgVariableDeclaration*> tags;

	if (IsHybrid) {
		auto gpu_tag = this->tag_variable()->CreateTagVariable("gpu_tag", 0);
		this->tag_variable()->set_gpu_tag(gpu_tag);
		tags.push_back(gpu_tag);
	}

	auto cpu_tag = this->tag_variable()->CreateTagVariable("cpu_tag", 9);
	this->tag_variable()->set_cpu_tag(cpu_tag);
	tags.push_back(cpu_tag);

	this->tag_variable()->set_tag_variables(tags);

	std::vector<SgExprStatement*> statement_list = this->direction_variable()->direction_statements();

	for(auto i = tags.rbegin(); i != tags.rend(); i++) {
		SgVariableDeclaration* tag_variable = (*i);
		SageInterface::insertStatementAfter(statement_list.back(), tag_variable);
	}

	std::cout << "done" << std::endl;
}

void MPIManager::InsertDirectionVariable(domain::DomainManager* domain_manager) {

	std::cout << "Panda: Creating directions...";

	SgVariableDeclaration* first_domain_declaration = domain_manager->domain_variables().front();
	std::vector<SgVariableDeclaration*> p_variable_list = domain_manager->decomposer()->geometry()->variables();

	SgVariableDeclaration* direction_variable = this->direction_variable()->CreateDirectionVariable();

	SageInterface::insertStatementBefore(first_domain_declaration, direction_variable);
	SageInterface::attachComment(direction_variable, "Direction Mappings", PreprocessingInfo::before, PreprocessingInfo::CplusplusStyleComment);

	this->direction_variable()->set_variable(direction_variable);

	std::vector<SgExprStatement*> statement_list = this->direction_variable()->CollectDirectionStatements(this->rank_variable(), p_variable_list);

	for(std::vector<SgExprStatement*>::reverse_iterator i = statement_list.rbegin(); i != statement_list.rend(); i++) {
		SgExprStatement* statement = (*i);
		SageInterface::insertStatementAfter(direction_variable, statement);
	}

	this->direction_variable()->set_direction_statements(statement_list);

	std::cout << "done" << std::endl;
}

void MPIManager::InsertDirectionEnumerations(SgProject* project) {

	SgFunctionDeclaration* main_function = SageInterface::findMain(project);

	SgEnumDeclaration* enum_declaration = SageBuilder::buildEnumDeclaration(this->direction_variable()->name(), SageBuilder::topScopeStack());

	std::vector<SgInitializedName*> enum_value_list = this->direction_variable()->CollectEnumValues();

	for(auto i = enum_value_list.begin(); i != enum_value_list.end(); i++) {
		SgInitializedName* enum_value = (*i);
		enum_declaration->append_enumerator(enum_value);
	}

	SageInterface::insertStatementBefore(main_function, enum_declaration);

	SageInterface::attachComment(enum_declaration, "Enumerate direction mappings", PreprocessingInfo::before, PreprocessingInfo::CplusplusStyleComment);
}

void MPIManager::Initialize() {
    this->set_direction_variable(new variables::Direction());
    this->set_rank_variable(new variables::Rank());
    this->set_size_variable(new variables::Size());
    this->set_tag_variable(new variables::Tag());
    this->set_request_count_variable(new variables::RequestCount());
    this->set_mpi_init_variable(new MPIInit());
    this->set_mpi_comm_rank_variable(new MPICommRank());
    this->set_mpi_comm_size_variable(new MPICommSize());
    this->set_mpi_request_variable(new MPIRequest());
    this->set_mpi_send_variable(new MPIIsend());
    this->set_mpi_recv_variable(new MPIIrecv());
    this->set_mpi_sync_variable(new MPIWaitall());
    this->set_mpi_header_variable(new MPIHeader());
    this->set_mpi_finalize_variable(new MPIFinalize());
	this->mpi_sync_variable()->set_mpi_status(this->mpi_recv_variable()->status());
}

} // namespace
