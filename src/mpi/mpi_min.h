#ifndef PANDA_MPI_MPIMIN_H_
#define PANDA_MPI_MPIMIN_H_

#include "mpi_op.h"

namespace mpi
{

class MPIMin : public MPIOp {
public:

	std::string name() const;

	~MPIMin() {}
};

} // namespace

#endif //PANDA_MPI_MPIMIN_H_
