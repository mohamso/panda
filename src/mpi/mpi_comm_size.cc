#include "mpi_comm_size.h"

namespace mpi
{
  // Creates the following statement: MPI_Comm_size(MPI_COMM_WORLD, &size);
SgExprStatement* MPICommSize::CreateMPICommSizeStatement(SgProject* project, SgVariableDeclaration* size_variable) {
    
    SgGlobal* global_scope = SageInterface::getFirstGlobalScope(project);
    SageBuilder::pushScopeStack(isSgScopeStatement(global_scope));

    SgExpression* comunicator_expression = SageBuilder::buildVarRefExp(this->communicator()->name());
    SgExpression* rank_expression = SageBuilder::buildAddressOfOp(SageBuilder::buildVarRefExp(size_variable));

    SgExprListExp* argument_list = SageBuilder::buildExprListExp();

    SageInterface::appendExpression(argument_list, comunicator_expression);
    SageInterface::appendExpression(argument_list, rank_expression);

    return SageBuilder::buildFunctionCallStmt(this->function_name(), SageBuilder::buildVoidType(), argument_list, global_scope);
  }

void MPICommSize::Initialize() {
	this->set_communicator(new MPICommunicator());
}

} //namespace
