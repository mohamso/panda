#ifndef PANDA_MPI_MPIDATATYPE_H_
#define PANDA_MPI_MPIDATATYPE_H_

#include "../panda.h"

namespace mpi
{
  class MPIDatatype {
  public:
    virtual ~MPIDatatype();

    virtual std::string name() const = 0;
  };

  class MPIDouble : public MPIDatatype {
  public:
    std::string name() const;
    
    ~MPIDouble();
  };

  class MPIFloat : public MPIDatatype {
  public:
    std::string name() const;

    ~MPIFloat();
  };

} // namespace mpi

#endif //PANDA_MPI_MPIDATATYPE_H_
