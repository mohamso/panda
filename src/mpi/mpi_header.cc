#include "mpi_header.h"

namespace mpi {

void MPIHeader::InsertMPIHeader(SgProject* project) {

  Rose_STL_Container<SgNode*> global_scope_list = NodeQuery::querySubTree(project, V_SgGlobal);

	for(auto i = global_scope_list.begin(); i != global_scope_list.end(); i++) {
		SageInterface::insertHeader("mpi.h", PreprocessingInfo::after, true, isSgGlobal(*i));
	}
}

} //namespace translator
