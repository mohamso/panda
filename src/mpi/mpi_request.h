#ifndef PANDA_MPI_REQUEST_H
#define PANDA_MPI_REQUEST_H

#include "../panda.h"
#include "../variables/request_count.h"

namespace mpi
{
  class MPIRequest {
  public:
	  MPIRequest() {
		  this->set_variable_name("mpi_request");
	 }

    // Getters
    int directions() const { return directions_; }
    std::string type_name() const { return "MPI_Request"; }
    std::string variable_name() const { return variable_name_; }
    SgType* type() const { return type_; }
    SgVariableDeclaration* variable() const { return variable_; }

    // Setters
    void set_number_of_directions(int directions) { directions_ = directions; }
    void set_variable_name(const std::string& variable_name) { variable_name_ = variable_name; }
    void set_type(SgType* type) { type_ = type; }
    void set_variable(SgVariableDeclaration* variable) { variable_ = variable; }

    SgType* CreateType(SgGlobal* global_scope);

    SgVariableDeclaration* CreateMPIRequestVariable(SgGlobal* global_scope);

    SgExpression* CreateMPIRequestAddressOfRequestCountExpression(SgVariableDeclaration* request_count_variable);

    ~MPIRequest() {}

  private:
    int directions_;
    std::string variable_name_;
    SgType* type_;
    SgVariableDeclaration* variable_;
};

} // namespace

#endif // PANDA_MPI_REQUEST_H
