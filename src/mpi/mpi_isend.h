#ifndef PANDA_MPI_MPIISEND_H_
#define PANDA_MPI_MPIISEND_H_

#include "mpi_root_send.h"

namespace mpi
{
class MPIIsend : public RootMPISend {
  public:
	MPIIsend() {
		this->set_communicator(new MPICommunicator());
        this->set_data_type(new MPIDouble());
	}

    // Getter
	std::string function_name() const { return "MPI_Isend"; }

	~MPIIsend() {}
};

} // namespace mpi

#endif //PANDA_MPI_MPIISEND_H_
