#ifndef PANDA_MPI_MPISYNCHRONIZE_H_
#define PANDA_MPI_MPISYNCHRONIZE_H_

#include "../panda.h"
#include "mpi_status.h"

namespace mpi
{
class MPISynchronize {
public:
	// Getters
	virtual std::string function_name() const = 0;
	MPIStatus* mpi_status() const { return status_; }
	SgStatement* statement() const { return statement_; }

	// Setters
    void set_mpi_status(MPIStatus* mpi_status) { status_ = mpi_status; }
    void set_statement(SgStatement* statement) { statement_ = statement; }

	// Functions
    virtual SgStatement* CreateMPISynchronizeStatement(SgVariableDeclaration* request_count_variable,  SgVariableDeclaration* mpi_request_variable) = 0;

    virtual ~MPISynchronize();

protected:
    MPIStatus* status_;
    SgStatement* statement_;
};

} // namespace mpi

#endif //PANDA_MPI_MPISYNCHRONIZE_H_
