#ifndef PANDA_MPI_MPIPROD_H_
#define PANDA_MPI_MPIPROD_H_

#include "mpi_op.h"

namespace mpi
{

class MPIProd : public MPIOp {
public:

	std::string name() const;

	~MPIProd() {}
};

} // namespace

#endif //PANDA_MPI_MPIPROD_H_
