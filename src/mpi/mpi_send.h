#ifndef PANDA_MPI_MPISEND_H_
#define PANDA_MPI_MPISEND_H_

#include "mpi_root_send.h"

namespace mpi
{
class MPISend : public RootMPISend {
  public:
    MPISend() {
        this->set_communicator(new MPICommunicator());
        this->set_data_type(new MPIDouble());
    }    

    // Getter
    std::string function_name() const { return "MPI_Send"; }

    ~MPISend() {}
};

} // namespace mpi

#endif //PANDA_MPI_MPISEND_H_
