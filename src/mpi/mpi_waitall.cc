#include "mpi_waitall.h"

namespace mpi
{

SgStatement* MPIWaitall::CreateMPISynchronizeStatement(SgVariableDeclaration* request_count_variable,  SgVariableDeclaration* mpi_request_variable) {

	std::cout << "Panda: Creating MPI_Waitall...";
    SgExprListExp* argument_list = SageBuilder::buildExprListExp();
    SageInterface::appendExpression(argument_list, SageBuilder::buildVarRefExp(request_count_variable));
    SageInterface::appendExpression(argument_list, SageBuilder::buildVarRefExp(mpi_request_variable));
    SageInterface::appendExpression(argument_list, SageBuilder::buildVarRefExp(this->mpi_status()->status()));

    std::cout << "done" << std::endl;

    return SageBuilder::buildFunctionCallStmt(this->function_name(), SageBuilder::buildVoidType(), argument_list);
}

}
