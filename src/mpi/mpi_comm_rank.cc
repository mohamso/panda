#include "mpi_comm_rank.h"

namespace mpi
{

// Creates the following statement: MPI_Comm_rank(MPI_COMM_WORLD, &rank);
SgExprStatement* MPICommRank::CreateMPICommRankStatement(SgProject* project, SgVariableDeclaration* rank_variable) {

	SgGlobal* global_scope = SageInterface::getFirstGlobalScope(project);
	SageBuilder::pushScopeStack(isSgScopeStatement(global_scope));

	SgExpression* comunicator_expression = SageBuilder::buildVarRefExp(this->communicator()->name());
	SgExpression* rank_expression = SageBuilder::buildAddressOfOp(SageBuilder::buildVarRefExp(rank_variable));

	SgExprListExp* argument_list = SageBuilder::buildExprListExp();

	SageInterface::appendExpression(argument_list, comunicator_expression);
	SageInterface::appendExpression(argument_list, rank_expression);

	return SageBuilder::buildFunctionCallStmt(this->function_name(), SageBuilder::buildVoidType(), argument_list, global_scope);
}

void MPICommRank::Initialize() {
	this->set_communicator(new MPICommunicator());
}

} // namespace
