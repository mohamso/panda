#include "mpi_init.h"

namespace mpi
{

SgExprStatement* MPIInit::CreateMPIInitStatement(SgProject* project) {

	SgFunctionDeclaration* main_function = SageInterface::findMain(project);

	SgGlobal* global_scope = SageInterface::getFirstGlobalScope(project);
	SageBuilder::pushScopeStack(isSgScopeStatement(global_scope));

	std::vector<SgName> main_function_argument_list = this->GetMainFunctionArguments(main_function);

	SgExpression* argc_expression = SageBuilder::buildAddressOfOp(SageBuilder::buildVarRefExp(main_function_argument_list.at(0)));
	SgExpression* argv_expression = SageBuilder::buildAddressOfOp(SageBuilder::buildVarRefExp(main_function_argument_list.at(1)));

	SgExprListExp* argument_list = SageBuilder::buildExprListExp();

	SageInterface::appendExpression(argument_list, argc_expression);
	SageInterface::appendExpression(argument_list, argv_expression);

	return SageBuilder::buildFunctionCallStmt(this->function_name(), SageBuilder::buildVoidType(), argument_list, global_scope);
}

std::vector<SgName> MPIInit::GetMainFunctionArguments(SgFunctionDeclaration* main_function) {

	SgInitializedNamePtrList main_argument_list = main_function->get_args();
	std::vector<SgName> argument_list;

	for (SgInitializedNamePtrList::iterator i = main_argument_list.begin(); i != main_argument_list.end(); i++) {
		SgSymbol* symbol = (*i)->get_symbol_from_symbol_table();
		argument_list.push_back(symbol->get_name());
	}

	return argument_list;
}

} // namespace
