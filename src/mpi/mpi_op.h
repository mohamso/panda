#ifndef PANDA_MPI_MPIOP_H_
#define PANDA_MPI_MPIOP_H_

#include "../panda.h"

namespace mpi
{
  class MPIOp {
  public:
    virtual ~MPIOp();

    virtual std::string name() const = 0;
  };

} // namespace mpi

#endif //PANDA_MPI_MPIOP_H_
