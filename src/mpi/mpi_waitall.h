#ifndef PANDA_MPI_MPIWAITALL_H_
#define PANDA_MPI_MPIWAITALL_H_

#include "mpi_synchronize.h"

namespace mpi
{
class MPIWaitall : public MPISynchronize {
  public:
	MPIWaitall() { }

    // Getter
    std::string function_name() const { return "MPI_Waitall"; }

    // Setter

    // Functions
    SgStatement* CreateMPISynchronizeStatement(SgVariableDeclaration* request_count_variable,  SgVariableDeclaration* mpi_request_variable);

    ~MPIWaitall() {}
};

} // namespace mpi

#endif //PANDA_MPI_MPIWAITALL_H_
