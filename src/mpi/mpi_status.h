#ifndef PANDA_MPI_MPISTATUS_H_
#define PANDA_MPI_MPISTATUS_H_

#include "../panda.h"

namespace mpi
{
class MPIStatus {
public:
	MPIStatus(std::string status) {
		this->set_status(status);
	}

	// Getter
	std::string status() const { return status_; }

	// Setter
	void set_status(std::string status) { status_ = status; }

    ~MPIStatus() {}

private:
	std::string status_;

};

} // namespace mpi

#endif //PANDA_MPI_MPISTATUS_H_
