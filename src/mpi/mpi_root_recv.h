#ifndef PANDA_MPI_MPIROOTRECV_H_
#define PANDA_MPI_MPIROOTRECV_H_

#include "../panda.h"
#include "../memory/buffer.h"
#include "../variables/subdomain_size.h"
#include "mpi_communicator.h"
#include "mpi_datatype.h"
#include "mpi_status.h"

namespace mpi
{
class RootMPIRecv {
public:
    // Getters
    virtual std::string function_name() const = 0;
    virtual std::vector<SgExprStatement*> const &mpi_recv_statements() const { return mpi_recv_list_; }
    virtual std::vector<SgIfStmt*> const &mpi_recv_if_stmts() const { return if_stmts_; }

	virtual MPICommunicator* communicator() const { return communicator_; }
    virtual MPIDatatype* data_type() const { return data_type_; }
    virtual MPIStatus* status() const { return status_; }

    // Setters
    void set_communicator(MPICommunicator* value) { communicator_ = value; }
    void set_data_type(MPIDatatype* value) { data_type_ = value; }
    void set_status(MPIStatus* value) { status_ = value; }
    void set_mpi_recv_statements(const std::vector<SgExprStatement*> &value) { mpi_recv_list_ = value; }
    void set_mpi_recv_if_stmts(const std::vector<SgIfStmt*> &value) { if_stmts_ = value; }

    // Functions
    virtual SgExpression* CreateCountExpression(buffer::NumBytes* num_bytes);

	virtual ~RootMPIRecv();

protected:
    MPICommunicator* communicator_;
    MPIDatatype* data_type_;
    MPIStatus* status_;
    std::vector<SgExprStatement*> mpi_recv_list_;
    std::vector<SgIfStmt*> if_stmts_;
};

} //namespace

#endif //PANDA_MPI_MPIROOTRECV_H_
