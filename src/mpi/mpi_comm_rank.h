#ifndef PANDA_MPI_MPICOMMRANK_H_
#define PANDA_MPI_MPICOMMRANK_H_

#include "../panda.h"
#include "mpi_communicator.h"

namespace mpi
{
  class MPICommRank {
  public:
    MPICommRank() { this->Initialize(); }

    // Getter
    MPICommunicator* communicator() const { return communicator_; }
	SgExprStatement* statement() const { return statement_; }
    std::string function_name() const { return "MPI_Comm_rank"; }
    
    // Setter
	void set_statement(SgExprStatement* value) { statement_ = value; }
    void set_communicator(MPICommunicator* communicator) { communicator_ = communicator; }

    // Functions
    SgExprStatement* CreateMPICommRankStatement(SgProject* sg_project_object, SgVariableDeclaration* rank_variable);

  private:
    void Initialize();
    MPICommunicator* communicator_;
	SgExprStatement* statement_;
  };

} // namespace mpi

#endif //PANDA_MPI_MPICOMMRANK_H_
