#ifndef PANDA_MPI_MPIALLREDUCE_H_
#define PANDA_MPI_MPIALLREDUCE_H_

#include "../panda.h"
#include "mpi_op.h"
#include "mpi_communicator.h"
#include "mpi_datatype.h"

namespace mpi
{
class MPIAllreduce {
public:

  MPIAllreduce();

  MPICommunicator* const &communicator() const { return communicator_; }
  SgStatement* CreateReduceStatement(const std::string& local_var_name, const std::string& global_var_name, MPIOp* op, MPIDatatype* data_type);

private:
  MPICommunicator* communicator_;

};

}// namespace

#endif //PANDA_MPI_MPIALLREDUCE_H_
