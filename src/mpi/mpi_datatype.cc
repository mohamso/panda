#include "mpi_datatype.h"

namespace mpi
{
  MPIDatatype::~MPIDatatype() {} 

  MPIDouble::~MPIDouble() {}

  std::string MPIDouble::name() const {
    return "MPI_DOUBLE";
  }

  MPIFloat::~MPIFloat() {}

  std::string MPIFloat::name() const {
    return "MPI_FLOAT";
  }

}
