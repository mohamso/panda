#ifndef PANDA_MPI_MPIRECV_H_
#define PANDA_MPI_MPIRECV_H_

#include "mpi_root_recv.h"

namespace mpi
{
class MPIRecv : public RootMPIRecv {
  public:
	MPIRecv() {
        this->set_communicator(new MPICommunicator());
        this->set_data_type(new MPIDouble());
        this->set_status(new MPIStatus("MPI_STATUS_IGNORE"));
    }

    // Getter
    std::string function_name() const { return "MPI_Recv"; }

    ~MPIRecv() {}
};

} // namespace

#endif //PANDA_MPI_MPIRECV_H_
