#ifndef PANDA_MPI_MPISUM_H_
#define PANDA_MPI_MPISUM_H_

#include "mpi_op.h"

namespace mpi
{

class MPISum : public MPIOp {
public:

	std::string name() const;

	~MPISum() {}
};

} // namespace mpi

#endif //PANDA_MPI_MPISUM_H_
