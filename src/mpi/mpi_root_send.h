#ifndef PANDA_MPI_MPIROOTSEND_H_
#define PANDA_MPI_MPIROOTSEND_H_

#include "../panda.h"
#include "../memory/buffer.h"
#include "../variables/subdomain_size.h"
#include "mpi_communicator.h"
#include "mpi_datatype.h"

namespace mpi
{
class RootMPISend {
public:
	virtual MPICommunicator* communicator() const { return communicator_; }
    virtual MPIDatatype* data_type() const { return data_type_; }

    // Getters
    virtual std::string function_name() const = 0;
    virtual std::vector<SgExprStatement*> const &mpi_send_statements() const { return mpi_send_list_; }
    virtual std::vector<SgIfStmt*> const &mpi_send_if_stmts() const { return if_stmts_; }

    // Setters
    void set_communicator(MPICommunicator* communicator) { communicator_ = communicator; }
    void set_data_type(MPIDatatype* data_type) { data_type_ = data_type; }
    void set_mpi_send_statements(const std::vector<SgExprStatement*> &value) { mpi_send_list_ = value; }
    void set_mpi_send_if_stmts(const std::vector<SgIfStmt*> &value) { if_stmts_ = value; }

    // Functions
    virtual SgExpression* CreateCountExpression(buffer::NumBytes* num_bytes);

    virtual ~RootMPISend();

protected:
    MPICommunicator* communicator_;
    MPIDatatype* data_type_;
    std::vector<SgExprStatement*> mpi_send_list_;
    std::vector<SgIfStmt*> if_stmts_;
};

} // namespace mpi

#endif //PANDA_MPI_MPIROOTSEND_H_
