#ifndef PANDA_MPI_MPIMANAGER_H_
#define PANDA_MPI_MPIMANAGER_H_

#include "../panda.h"
#include "../variables/request_count.h"
#include "../variables/rank.h"
#include "../variables/size.h"
#include "../variables/tag.h"
#include "../variables/direction.h"

#include "../analyzer/stencil.h"
#include "../memory/buffer_manager.h"

#include "mpi_header.h"
#include "mpi_finalize.h"
#include "mpi_init.h"
#include "mpi_comm_rank.h"
#include "mpi_comm_size.h"
#include "mpi_isend.h"
#include "mpi_irecv.h"
#include "mpi_request.h"
#include "mpi_waitall.h"

namespace mpi
{
  class MPIManager {
  public:
    MPIManager() { this->Initialize(); }

    // Getters
    variables::RequestCount* request_count_variable() const { return request_count_variable_; }
    variables::Rank* rank_variable() const { return rank_variable_; }
    variables::Size* size_variable() const { return size_variable_; }
    variables::Tag* tag_variable() const { return tag_variable_; }
    variables::Direction* direction_variable() const { return direction_variable_; }

    MPIRequest* mpi_request_variable() const { return request_variable_; }
    RootMPISend* mpi_send_variable() const { return send_variable_; }
    RootMPIRecv* mpi_recv_variable() const { return recv_variable_; }
    MPISynchronize* mpi_sync_variable() const { return sync_variable_; }
	MPIHeader* mpi_header_variable() const { return mpi_header_; }
    MPIFinalize* mpi_finalize_variable() const { return mpi_finalize_; }
    MPIInit* mpi_init_variable() const { return mpi_init_; }
    MPICommRank* mpi_comm_rank_variable() const { return mpi_comm_rank_; }
    MPICommSize* mpi_comm_size_variable() const { return mpi_comm_size_; }

    // Setters
    void set_request_count_variable(variables::RequestCount* request_count_variable) { request_count_variable_ = request_count_variable; }
    void set_rank_variable(variables::Rank* rank_variable) { rank_variable_ = rank_variable; }
    void set_size_variable(variables::Size* size_variable) { size_variable_ = size_variable; }
    void set_tag_variable(variables::Tag* tag_variable) { tag_variable_ = tag_variable; }
	void set_direction_variable(variables::Direction* direction_variable) { direction_variable_ = direction_variable; }

    void set_mpi_request_variable(MPIRequest* request_variable) { request_variable_ = request_variable; }
    void set_mpi_send_variable(RootMPISend* send_variable) { send_variable_ = send_variable; }
    void set_mpi_recv_variable(RootMPIRecv* recv_variable) { recv_variable_ = recv_variable; }
    void set_mpi_sync_variable(MPISynchronize* sync_variable) { sync_variable_ = sync_variable; }
    void set_mpi_header_variable(MPIHeader* value) { mpi_header_ = value; }
    void set_mpi_finalize_variable(MPIFinalize* value) { mpi_finalize_ = value; }
    void set_mpi_init_variable(MPIInit* value) { mpi_init_ = value; }
    void set_mpi_comm_rank_variable(MPICommRank* value) { mpi_comm_rank_ = value; }
    void set_mpi_comm_size_variable(MPICommSize* value) { mpi_comm_size_ = value; }

    // Functions
    void InsertRequestCountStatement(SgForStatement* time_loop);

    void InsertRequestCountVariable();

    void InsertSizeVariable(SgProject* project);
    void InsertRankVariable();
    void InsertMPIInit(SgProject* project);
    void InsertMPICommRank(SgProject* project);
    void InsertMPICommSize(SgProject* project);
    void InsertMPIRequestVariable(SgProject* project);

    void InsertHeader(SgProject* project);
    void InsertFinalize(SgProject* project);
    void InsertMPIWaitallStatement();
    void CollectMPIISendStatements(buffer::BufferManager* buffer_manager, domain::Decomposer* decomposer, analyzer::Stencil* stencil);
    void CollectMPIReceiveStatements(buffer::BufferManager* buffer_manager, domain::Decomposer* decomposer, analyzer::Stencil* stencil);
    void InsertDirectionVariable(domain::DomainManager* domain_manager);
    void InsertTagVariables(bool IsHybrid);
    void InsertDirectionEnumerations(SgProject* project);

    ~MPIManager() {}

    private:      
    variables::RequestCount* request_count_variable_;
    variables::Rank* rank_variable_;
    variables::Size* size_variable_;
    variables::Tag* tag_variable_;
    variables::Direction* direction_variable_;

    MPIRequest* request_variable_;
    RootMPISend* send_variable_;
    RootMPIRecv* recv_variable_;
    MPISynchronize* sync_variable_;
    MPIHeader* mpi_header_;
    MPIFinalize* mpi_finalize_;
    MPIInit* mpi_init_;
    MPICommRank* mpi_comm_rank_;
    MPICommSize* mpi_comm_size_;

    void Initialize();

  };
} // namespace mpi

#endif //PANDA_MPI_MPIMANAGER_H_
