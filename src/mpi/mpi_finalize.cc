#include "mpi_finalize.h"

namespace mpi
{
SgExprStatement* MPIFinalize::InsertMPIFinalize(SgProject* project) {

    SgFunctionDeclaration* main_function = SageInterface::findMain(project);
    SgBasicBlock* main_function_body = main_function->get_definition()->get_body();

    SgExprStatement* mpi_finalize_statement = 
    SageBuilder::buildFunctionCallStmt("MPI_Finalize", SageBuilder::buildVoidType(), SageBuilder::buildExprListExp(), main_function_body);

    SageInterface::instrumentEndOfFunction(main_function, mpi_finalize_statement);

    return mpi_finalize_statement;
}

} //namespace
