#ifndef PANDA_MPI_MPICOMMSIZE_H_
#define PANDA_MPI_MPICOMMSIZE_H_

#include "../panda.h"
#include "mpi_communicator.h"

namespace mpi
{
class MPICommSize {
public:
	MPICommSize() {
		this->Initialize();
	}

	// Getters
	MPICommunicator* communicator() const { return communicator_; }
	SgExprStatement* statement() const { return statement_; }
	std::string function_name() const { return "MPI_Comm_size"; }


	// Setters
	void set_statement(SgExprStatement* value) { statement_ = value; }
	void set_communicator(MPICommunicator* communicator) { communicator_ = communicator; }

	// Functions
	SgExprStatement* CreateMPICommSizeStatement(SgProject* sg_project_object, SgVariableDeclaration* size_variable);

	~MPICommSize() {}

private:
	void Initialize();
	SgExprStatement* statement_;
	MPICommunicator* communicator_;
};
} // namespace mpi

#endif //PANDA_MPI_MPICOMMSIZE_H_
