#ifndef PANDA_MPI_MPIHEADER_H_
#define PANDA_MPI_MPIHEADER_H_

#include "../panda.h"

namespace mpi
{
  class MPIHeader {
  public:
    void InsertMPIHeader(SgProject* project);
    
    ~MPIHeader() {}
  };
} // namespace mpi

#endif //PANDA_MPI_MPIHEADER_H_
