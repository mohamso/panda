#ifndef PANDA_MPI_COMMUNICATOR_H_
#define PANDA_MPI_COMMUNICATOR_H_

#include "../panda.h"

namespace mpi
{
  class MPICommunicator {
  public:
    std::string const& name() const { return name_; }
    void set_name(const std::string& value) { name_ = value; }

    MPICommunicator() : name_("MPI_COMM_WORLD") {}

    ~MPICommunicator();

  private:
    std::string name_;
    
  };

} // namespace mpi

#endif //PANDA_MPI_COMMUNICATOR_H_
