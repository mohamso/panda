#ifndef PANDA_MPI_MPIFINALIZE_H_
#define PANDA_MPI_MPIFINALIZE_H_

#include "../panda.h"

namespace mpi
{
  class MPIFinalize {
  public:
	  // Getter
	  SgExprStatement* statement() const { return statement_; }

	  // Setter
	  void set_statement(SgExprStatement* value) { statement_ = value; }

	  // Functions
	  SgExprStatement* InsertMPIFinalize(SgProject* project);
    
    ~MPIFinalize() {}

  private:
    SgExprStatement* statement_;

  };
} // namespace mpi

#endif //PANDA_MPI_MPIFINALIZE_H_
