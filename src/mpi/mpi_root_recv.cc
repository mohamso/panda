#include "mpi_root_recv.h"

namespace mpi
{

RootMPIRecv::~RootMPIRecv() {}

SgExpression* RootMPIRecv::CreateCountExpression(buffer::NumBytes* num_bytes) {

	SgExpression* count_expression;

	if (num_bytes->direction()->IsFront() || num_bytes->direction()->IsBack()) {

		auto x_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(num_bytes->direction()->plane_one()), SageBuilder::buildIntVal(2));
		auto y_plane = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(num_bytes->direction()->plane_two()), SageBuilder::buildIntVal(2));
		count_expression = SageBuilder::buildMultiplyOp(x_plane, y_plane);

	} else {
		count_expression = SageBuilder::buildMultiplyOp(SageBuilder::buildVarRefExp(num_bytes->direction()->plane_one()), SageBuilder::buildVarRefExp(num_bytes->direction()->plane_two()));
	}

	return count_expression;
}

} //namespace
