#include "analyzer.h"

namespace analyzer
{

void Analyzer::Initialize() {
	this->compute_analyzer_ = new ComputeAnalyzer();
	this->stencil_analyzer_ = new StencilAnalyzer();
}

} //namespace
