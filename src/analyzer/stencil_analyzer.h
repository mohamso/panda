#ifndef PANDA_ANALYZER_STENCILANALYZER_H
#define PANDA_ANALYZER_STENCILANALYZER_H

#include "../loop/loop_manager.h"
#include "../domain/decomposer.h"
#include "stencil.h"

#include <vector>
#include <algorithm>
#include <functional>
#include <set>
#include <map>

namespace analyzer
{

class StencilAnalyzer {
  public:

	StencilAnalyzer() {
		this->stencil_ = new Stencil();
	}

	// Getter
	Stencil* get_stencil() { return this->stencil_; }

	// Functions
	std::map<std::string, int> Analyze(loop::LoopManager* loop_manager, domain::Decomposer* decomposer);
	std::vector<std::pair<int, std::string>> SortStencilRankMap(std::map<std::string, int> stencil_rank_map);

	void CollectReadOnlyArrays(loop::LoopManager* loop_manager);
	void CollectWriteArray(SgProject* project, loop::LoopManager* loop_manager);
	void CollectScalars(SgProject* project, translator::PragmaManager* pragma_manager, loop::LoopManager* loop_manager);

	int DetermineStencilReach(std::vector<std::pair<int, std::string>> sorted_rank_map);
	SgVariableDeclaration* DetermineTargetArray(domain::DomainManager* domain_manager, std::vector<std::pair<int, std::string>> sorted_rank_pair);

    ~StencilAnalyzer() {}

  private:

    std::vector<SgVariableDeclaration*> FindScalarDeclarations(SgProject* project, std::vector<SgVarRefExp*> scalars);
    void GetWriteArrayType(SgProject* project);

    Stencil* stencil_;

};

} // namespace analyzer

#endif // PANDA_ANALYZER_STENCILANALYZER_H
