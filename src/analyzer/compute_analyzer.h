#ifndef PANDA_ANALYZER_COMPUTEANALYZER_H
#define PANDA_ANALYZER_COMPUTEANALYZER_H

#include "../loop/loop_manager.h"
#include "../domain/decomposer.h"

namespace analyzer
{

class ComputeAnalyzer {
public:

  std::vector<SgAssignOp*> Analyze(loop::LoopManager* loop_manager);

  // Getter
  std::vector<SgAssignOp*> const &compute_expression() const { return compute_expression_; }

  // Setter
  void set_compute_expression(const std::vector<SgAssignOp*> &value) { compute_expression_ = value; }

  ~ComputeAnalyzer() {}

private:
  std::vector<SgAssignOp*> compute_expression_;

};

} // namespace analyzer

#endif // PANDA_ANALYZER_COMPUTEANALYZER_H
