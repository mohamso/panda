#include "stencil_analyzer.h"

namespace analyzer
{

bool SortComparer(const std::pair<int, std::string> &left, const std::pair<int,std::string> &right) {
	return left.second > right.second;
}

SgVariableDeclaration* StencilAnalyzer::DetermineTargetArray(domain::DomainManager* domain_manager, std::vector<std::pair<int, std::string>> sorted_rank_pair) {

	std::cout << "Panda: Determining target array...";

	std::string target_arr_name = std::get<1>(sorted_rank_pair[0]);
	std::vector<SgVariableDeclaration*> domain_variables = domain_manager->domain_variables();
	Rose_STL_Container<SgNode*> initialized_name_list;
	SgVariableDeclaration* target_arr_decl;

	for (auto i = domain_variables.begin(); i != domain_variables.end(); i++) {
		if (!target_arr_name.compare(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString())) {
			target_arr_decl = isSgVariableDeclaration(*i);
		}
	}

	std::cout << "done" << std::endl;

	return target_arr_decl;
}

int StencilAnalyzer::DetermineStencilReach(std::vector<std::pair<int, std::string>> sorted_rank_pair) {

	return std::get<0>(sorted_rank_pair[0]);
}

std::vector<std::pair<int, std::string>> StencilAnalyzer::SortStencilRankMap(std::map<std::string, int> stencil_rank_map) {

	std::vector<std::pair<int, std::string>> sorted_rank_pair;

	for (auto i = stencil_rank_map.begin(); i != stencil_rank_map.end(); i++) {
		sorted_rank_pair.push_back({i->second,i->first});
	}

	std::sort(sorted_rank_pair.begin(), sorted_rank_pair.end(), SortComparer);

	return sorted_rank_pair;
}

std::map<std::string, int> StencilAnalyzer::Analyze(loop::LoopManager* loop_manager, domain::Decomposer* decomposer) {

	  std::vector<SgForStatement*> compute_for_loop_list = loop_manager->compute_transformer()->loop_statements();
	  std::vector<std::string> subdomain_size_list;
	  std::map<SgVarRefExp*, SgVariableDeclaration*> subdomain_size_map = decomposer->domain_size_geometry_map();

	  for (auto i = subdomain_size_map.begin(); i != subdomain_size_map.end(); i++) {
		  subdomain_size_list.push_back(i->first->unparseToString());
	  }

	  std::vector<SgForStatement*> stencil_loop_list = loop_manager->compute_transformer()->CollectComputeLoops(compute_for_loop_list, subdomain_size_list);
	  std::vector<SgForStatement*> inner_most_stencil_loop_list;

	  for (auto i = stencil_loop_list.begin(); i != stencil_loop_list.end(); i++) {
		  if (loop_manager->compute_transformer()->IsInnerMostLoop(isSgForStatement(*i))) {
			  inner_most_stencil_loop_list.push_back(*i);
		  }
	  }

	  std::set<std::string> pntr_arr_ref_exp_set;
	  std::vector<std::string> var_ref_exp_string_list;
	  Rose_STL_Container<SgNode*> var_ref_exp_list;

	  for (auto i = inner_most_stencil_loop_list.begin(); i != inner_most_stencil_loop_list.end(); i++) {
		  if (isSgForStatement(*i)->get_loop_body() != NULL) {
			  Rose_STL_Container<SgNode*> assign_list = NodeQuery::querySubTree(isSgForStatement(*i)->get_loop_body(), V_SgAssignOp);
			  for (auto j = assign_list.begin(); j != assign_list.end(); j++) {
				  if (isSgAssignOp(*j)->get_rhs_operand() != NULL) {
					  Rose_STL_Container<SgNode*> pntr_arr_ref_exp_list = NodeQuery::querySubTree(isSgAssignOp(*j)->get_rhs_operand(), V_SgPntrArrRefExp);
					  for (auto k = pntr_arr_ref_exp_list.begin(); k != pntr_arr_ref_exp_list.end(); k++) {
						  if (isSgPntrArrRefExp(*k)->get_lhs_operand() != NULL) {
							  var_ref_exp_list = NodeQuery::querySubTree(isSgPntrArrRefExp(*k)->get_lhs_operand(), V_SgVarRefExp);
							  for (auto l = var_ref_exp_list.begin(); l != var_ref_exp_list.end(); l++) {
								  var_ref_exp_string_list.push_back(isSgVarRefExp(*l)->unparseToString());
								  pntr_arr_ref_exp_set.insert(isSgVarRefExp(*l)->unparseToString());
							  }
						  }
					  }
				  }
			  }
		  }
	  }

	  std::map<std::string, int> array_occurence_map;

	  for (auto i = pntr_arr_ref_exp_set.begin(); i != pntr_arr_ref_exp_set.end(); i++) {
		  int count = std::count(var_ref_exp_string_list.begin(), var_ref_exp_string_list.end(), (*i));
		  array_occurence_map[(*i)] = count;
	  }

	  return array_occurence_map;
  }

void StencilAnalyzer::CollectReadOnlyArrays(loop::LoopManager* loop_manager) {

	// 1. Find inner most loop
	// 2. Find all ptrArrs, add them to a set.
	//    Duplicates are eliminated.

	std::cout << "Panda: Analyzing read-only arrays...";

	  SgForStatement* inner_loop = loop_manager->compute_transformer()->loop_statements().back();
	  std::vector<SgVarRefExp*> read_only_arrays;
	  std::vector<std::string> read_only_arrays_names;

	  Rose_STL_Container<SgNode*> assign_list = NodeQuery::querySubTree(isSgForStatement(inner_loop)->get_loop_body(), V_SgAssignOp);

	  for (auto i = assign_list.begin(); i != assign_list.end(); i++) {
		  if (isSgAssignOp(*i)->get_rhs_operand() != NULL) {
			  Rose_STL_Container<SgNode*> pntr_arr_ref_exp_list = NodeQuery::querySubTree(isSgAssignOp(*i)->get_rhs_operand(), V_SgPntrArrRefExp);
			  for (auto j = pntr_arr_ref_exp_list.begin(); j != pntr_arr_ref_exp_list.end(); j++) {
				  if (isSgPntrArrRefExp(*j)->get_lhs_operand() != NULL) {
					  if (isSgPntrArrRefExp(*j)->get_lhs_operand()->variantT() == V_SgVarRefExp) {
						  auto read_only_array = isSgVarRefExp(isSgPntrArrRefExp(*j)->get_lhs_operand());
						  auto array_name = read_only_array->unparseToString();

						  bool IsPresent = (std::find(read_only_arrays_names.begin(), read_only_arrays_names.end(), array_name) != read_only_arrays_names.end());

						  if (!IsPresent) {
							  read_only_arrays_names.push_back(array_name);
							  read_only_arrays.push_back(read_only_array);
						  }
					  }
				  }
			  }
		  }
	  }

	  read_only_arrays_names.clear();

	  this->get_stencil()->set_read_only_arrays(read_only_arrays);

	  std::cout << "...done" << std::endl;
  }

void StencilAnalyzer::CollectScalars(SgProject* project, translator::PragmaManager* pragma_manager, loop::LoopManager* loop_manager) {

	// 1. Find inner most loop
	// 2. Find all SgVarRefExp on the rhs of the assignment
	// 3. Start excluding scalars that are not candidates

	std::cout << "Panda: Analyzing scalars...";

	SgForStatement* inner_loop = loop_manager->compute_transformer()->loop_statements().back();
	std::vector<SgVarRefExp*> scalar_candidates;

	Rose_STL_Container<SgNode*> assign_list = NodeQuery::querySubTree(isSgForStatement(inner_loop)->get_loop_body(), V_SgAssignOp);

	for (auto i = assign_list.begin(); i != assign_list.end(); i++) {
		if (isSgAssignOp(*i)->get_rhs_operand() != NULL) {
			Rose_STL_Container<SgNode*> var_list = NodeQuery::querySubTree(isSgAssignOp(*i)->get_rhs_operand(), V_SgVarRefExp);
			for (auto j = var_list.begin(); j != var_list.end(); j++) {
				scalar_candidates.push_back(isSgVarRefExp(*j));
			}
		}
	}

	// 4. Use information from domain_size to exclude
	//    the domain size variables (e.g. Nx, Ny)
	std::vector<std::string> domain_sizes = pragma_manager->domain_size_list();
	for (auto i = scalar_candidates.begin(); i != scalar_candidates.end();) {
		bool IsPresent = (std::find(domain_sizes.begin(), domain_sizes.end(), isSgVarRefExp(*i)->unparseToString()) != domain_sizes.end());

	    if (IsPresent) {
	    	i = scalar_candidates.erase(i);
	    } else {
	    	++i;
	    }
	}

	// 5. Use information about read-only arrays to exclude SgPntrArrRefExps
	std::vector<std::string> indirect_array_names;
	for (auto i = this->get_stencil()->read_only_arrays().begin(); i != this->get_stencil()->read_only_arrays().end(); i++) {
		indirect_array_names.push_back(isSgVarRefExp(*i)->unparseToString());
	}

	for (auto i = scalar_candidates.begin(); i != scalar_candidates.end();) {
		bool IsPresent = (std::find(indirect_array_names.begin(), indirect_array_names.end(), isSgVarRefExp(*i)->unparseToString()) != indirect_array_names.end());

	    if (IsPresent) {
	    	i = scalar_candidates.erase(i);
	    } else {
	    	++i;
	    }
	}

	// 6. Use information about index variable to exclude it
	//    from the list of candidates
	std::vector<std::string> index_variable = loop_manager->compute_transformer()->FindIndexVariable(inner_loop);
	for (auto i = scalar_candidates.begin(); i != scalar_candidates.end();) {
		bool IsPresent = (std::find(index_variable.begin(), index_variable.end(), isSgVarRefExp(*i)->unparseToString()) != index_variable.end());

	    if (IsPresent) {
	    	i = scalar_candidates.erase(i);
	    } else {
	    	++i;
	    }
	}

	this->get_stencil()->set_scalar_declarations(this->FindScalarDeclarations(project, scalar_candidates));

	// Cleanup
	assign_list.clear();
	indirect_array_names.clear();
	domain_sizes.clear();

	std::cout << "...done" << std::endl;
}

std::vector<SgVariableDeclaration*> StencilAnalyzer::FindScalarDeclarations(SgProject* project, std::vector<SgVarRefExp*> scalars) {
	Rose_STL_Container<SgNode*> global_variable_declaration_list = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	std::vector<SgVariableDeclaration*> scalar_declarations;

	for (auto i = global_variable_declaration_list.begin(); i != global_variable_declaration_list.end(); i++) {
		auto variable_name = isSgVariableDeclaration(*i)->get_variables().front()->unparseToString();

		for (auto j = scalars.begin(); j != scalars.end(); j++) {
			auto scalar_name = isSgVarRefExp(*j)->unparseToString();
			if (variable_name == scalar_name) {
				scalar_declarations.push_back(isSgVariableDeclaration(*i));
			}
		}
	}

	return scalar_declarations;
}

void StencilAnalyzer::CollectWriteArray(SgProject* project, loop::LoopManager* loop_manager) {

	// 1. Find inner most loop
	// 2. Find assignments
	// 3. Check lhs_operand, if it is V_SgPntrArrRefExp, then it is our write array
	// NOTE: This analysis must be improved in case there are multiple SgPntrArrRefExps

	std::cout << "Panda: Analyzing write array...";

	SgForStatement* inner_loop = loop_manager->compute_transformer()->loop_statements().back();

	  Rose_STL_Container<SgNode*> assign_list = NodeQuery::querySubTree(isSgForStatement(inner_loop)->get_loop_body(), V_SgAssignOp);

	  for (auto i = assign_list.begin(); i != assign_list.end(); i++) {
		  if (isSgAssignOp(*i)->get_lhs_operand() != NULL) {
			  if (isSgAssignOp(*i)->get_lhs_operand()->variantT() == V_SgPntrArrRefExp) {
				  this->get_stencil()->set_write_array(isSgPntrArrRefExp(isSgAssignOp(*i)->get_lhs_operand()));
				  this->get_stencil()->set_write_array_name(isSgPntrArrRefExp(isSgAssignOp(*i)->get_lhs_operand())->get_lhs_operand()->unparseToString());
			  }
		  }
	  }

	  std::cout << "done" << std::endl;

	  this->GetWriteArrayType(project);
}

void StencilAnalyzer::GetWriteArrayType(SgProject* project) {

	std::cout << "Panda: Locating write array data type...";

	Rose_STL_Container<SgNode*> array_declarations = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	for (auto i = array_declarations.begin(); i != array_declarations.end(); i++) {
		auto variable_name = isSgVariableDeclaration(*i)->get_variables().front()->unparseToString();

		if (variable_name == this->get_stencil()->write_array_name()) {
			auto variables = isSgVariableDeclaration(*i)->get_variables();
			for (auto j = variables.begin(); j != variables.end(); j++) {
				this->get_stencil()->set_write_array_type(isSgInitializedName(*j)->get_type());
			}
		}
	}

	std::cout << "done" << std::endl;
}

} // namespace
