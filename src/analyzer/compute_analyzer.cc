#include "compute_analyzer.h"

namespace analyzer
{

std::vector<SgAssignOp*> ComputeAnalyzer::Analyze(loop::LoopManager* loop_manager) {

	std::cout << "Panda: Analyzing compute loops...";

	SgForStatement* inner_most_loop = loop_manager->compute_transformer()->loop_statements().back();
	std::vector<SgForStatement*> inner_most_loop_list;

	std::vector<std::string> index_name_list = loop_manager->compute_transformer()->index_names();
	std::vector<SgAssignOp*> assign_list;

	// Get the actual compute expression
	if (inner_most_loop->get_loop_body() != NULL) {
		Rose_STL_Container<SgNode*> assign_op_list = NodeQuery::querySubTree(inner_most_loop->get_loop_body(), V_SgAssignOp);

		for (auto i = assign_op_list.begin(); i != assign_op_list.end(); i++) {
			if (isSgAssignOp(*i)->get_lhs_operand() != NULL) {
				if (isSgAssignOp(*i)->get_lhs_operand()->variantT() == V_SgPntrArrRefExp) {
					if (isSgPntrArrRefExp(isSgAssignOp(*i)->get_lhs_operand())->get_rhs_operand() != NULL) {
						bool IsPresent = (std::find(index_name_list.begin(), index_name_list.end(), isSgPntrArrRefExp(isSgAssignOp(*i)->get_lhs_operand())->get_rhs_operand()->unparseToString() ) != index_name_list.end());

						if (IsPresent) {
							assign_list.push_back(isSgAssignOp(*i));
						}
					}
				}

				// TODO: Add support for SgFunctionCallExp
				}
		}
	}

	std::cout << "done" << std::endl;

	return assign_list;
}

} //namespace
