#ifndef PANDA_ANALYZER_STENCIL_H
#define PANDA_ANALYZER_STENCIL_H

#include "../panda.h"

namespace analyzer
{

class Stencil {
  public:

	// Getters
	int stencil_points() const { return stencil_points_; }
	SgVariableDeclaration* target_array() const { return target_array_; }
	SgPntrArrRefExp* write_array() const { return write_array_; }
	SgType* write_array_type() const { return write_array_type_; }
    std::string write_array_name() const { return write_array_name_; }
	std::vector<SgVarRefExp*> const &read_only_arrays() const { return read_only_arrays_; }
	std::vector<SgVariableDeclaration*> const &scalar_decls() const { return scalar_decls_; }

	// Setters
	void set_stencil_points(int value) { stencil_points_ = value; }
	void set_target_array(SgVariableDeclaration* value) { target_array_ = value; }
	void set_write_array(SgPntrArrRefExp* value) { write_array_ = value; }
    void set_write_array_name(const std::string& value) { write_array_name_ = value; }
    void set_write_array_type(SgType* value) { write_array_type_ = value; }
	void set_read_only_arrays(const std::vector<SgVarRefExp*> &value) { read_only_arrays_ = value; }
	void set_scalar_declarations(const std::vector<SgVariableDeclaration*> &value) { scalar_decls_ = value; }

    ~Stencil() {}

  private:

    int stencil_points_;

    std::vector<SgVarRefExp*> read_only_arrays_;
    std::vector<SgVariableDeclaration*> scalar_decls_;

    SgType* write_array_type_;
    std::string write_array_name_;
    SgPntrArrRefExp* write_array_;
    SgVariableDeclaration* target_array_;
};

} // namespace analyzer

#endif // PANDA_ANALYZER_STENCIL_H
