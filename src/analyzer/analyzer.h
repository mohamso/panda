#ifndef PANDA_ANALYZER_ANALYZER_H
#define PANDA_ANALYZER_ANALYZER_H

#include "../panda.h"
#include "compute_analyzer.h"
#include "stencil_analyzer.h"

namespace analyzer
{

class Analyzer {
  public:

	Analyzer() { this->Initialize(); }

	// Getter
	ComputeAnalyzer* compute_analyzer() const { return compute_analyzer_; }
	StencilAnalyzer* stencil_analyzer() const { return stencil_analyzer_; }

	// Setter
	void set_compute_analyzer(ComputeAnalyzer* value) { compute_analyzer_ = value; }
	void set_stencil_analyzer(StencilAnalyzer* value) { stencil_analyzer_ = value; }

    ~Analyzer() {}

  private:
	void Initialize();

    ComputeAnalyzer* compute_analyzer_;
    StencilAnalyzer* stencil_analyzer_;
};

} // namespace analyzer

#endif // PANDA_ANALYZER_ANALYZER_H
