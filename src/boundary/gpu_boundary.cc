#include "gpu_boundary.h"

namespace boundary
{

std::vector<SgExprStatement*> GPUBoundary::CreateComputePackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

std::vector<SgFunctionDeclaration*> GPUBoundary::CreateComputePackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Analyzer* analyzer, domain::Decomposer* decomposer) {

	std::cout << "Panda: Generating Compute Pack Functions...";

	std::vector<SgFunctionDeclaration*> compute_pack_func_decls;

	//1. Create function parameter list

	// 2. Add paramters to the function parameter list

	// 3. Create compute pack function declarations

	// 4. Map direction to the function declaration
	//    Fill the body of the function declarations


	std::cout << "done" << std::endl;

	return compute_pack_func_decls;
}


std::vector<SgInitializedName*> GPUBoundary::BuildComputePackFunctionSignature(buffer::BufferManager* buffer_manager, variables::RootDirection* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

void GPUBoundary::CreateComputePackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Analyzer* analyzer, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) {

}

SgExprStatement* GPUBoundary::CreatePackStatement(buffer::RootBuffer* send_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) {

}

std::vector<SgExprStatement*> GPUBoundary::CreateUnpackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

std::vector<SgFunctionDeclaration*> GPUBoundary::CreateUnpackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

std::vector<SgInitializedName*> GPUBoundary::BuildUnpackFunctionSignature(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

void GPUBoundary::CreateUnpackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Stencil* stencil, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) {

}

SgExprStatement* GPUBoundary::CreateUnpackStatement(buffer::RootBuffer* recv_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) {

}

} // namespace
