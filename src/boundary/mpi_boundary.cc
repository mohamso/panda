#include "mpi_boundary.h"

namespace boundary
{

std::vector<SgExprStatement*> MPIBoundary::CreateComputePackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {
  return this->CreateComputePackFuncCallStmts(buffer_manager, direction, stencil, decomposer);
}

std::vector<SgFunctionDeclaration*> MPIBoundary::CreateComputePackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Analyzer* analyzer, domain::Decomposer* decomposer) {

	std::cout << "Panda: Generating Compute Pack Functions...";

	//1. Create function parameter list
	std::vector<SgFunctionParameterList*> func_parameter_lists;
	std::map<variables::RootDirection*, SgFunctionDeclaration*> direction_func_map;

	for (auto i = direction->directions().begin(); i != direction->directions().end(); i++) {
		variables::RootDirection* direction = (*i);

		if (!direction->IsBack() && !direction->IsSouth() && !direction->IsWest()) {
			func_parameter_lists.push_back(SageBuilder::buildFunctionParameterList());
			direction_func_map[direction] = NULL;
		}
	}

	// 2. Add paramters to the function parameter list
	std::vector<SgInitializedName*> signature_params;
	for (auto i = func_parameter_lists.begin(), j = direction_func_map.rbegin(); i != func_parameter_lists.end(), j != direction_func_map.rend(); i++, j++) {
		signature_params = this->BuildComputePackFunctionSignature(buffer_manager, j->first, analyzer->stencil_analyzer()->get_stencil(), decomposer);
		for (auto k = signature_params.begin(); k != signature_params.end(); k++) {
			SageInterface::appendArg(isSgFunctionParameterList(*i), isSgInitializedName(*k));
		}
	}

	// 3. Create compute pack function declarations
	std::vector<SgFunctionDeclaration*> pack_func_declarations;
	for (auto i = func_parameter_lists.begin(), j = direction_func_map.rbegin(); i != func_parameter_lists.end(), j != direction_func_map.rend(); i++, j++) {
	    pack_func_declarations.push_back(SageBuilder::buildDefiningFunctionDeclaration(this->pack_func_name() + j->first->merged_direction_name(), SageBuilder::buildVoidType(), isSgFunctionParameterList(*i)));
	 }

	// 4. Map direction to the function declaration
	//    Fill the body of the function declarations
	  for (auto i = direction_func_map.rbegin(), j = pack_func_declarations.begin(); i != direction_func_map.rend(), j != pack_func_declarations.end(); i++, j++) {
		direction_func_map[i->first] = isSgFunctionDeclaration(*j);
	    this->CreateComputePackFunctionDeclarationBody(buffer_manager, analyzer, i->first, i->second);
	  }

	// Clear to free up memory
	signature_params.clear();
	func_parameter_lists.clear();

	std::cout << "done" << std::endl;

	return pack_func_declarations;
}

std::vector<SgInitializedName*> MPIBoundary::BuildComputePackFunctionSignature(buffer::BufferManager* buffer_manager, variables::RootDirection* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

  std::vector<SgInitializedName*> signature_params;

  signature_params.push_back(SageBuilder::buildInitializedName(stencil->write_array_name(), stencil->write_array_type()));
  signature_params.push_back(SageBuilder::buildInitializedName(stencil->target_array()->get_variables().front()->unparseToString(), stencil->target_array()->get_decl_item(stencil->target_array()->get_variables().front()->unparseToString())->get_type()));

  if (!direction->IsFront()) {
	  signature_params.push_back(SageBuilder::buildInitializedName(buffer_manager->send_buffers().front()->name(), stencil->write_array_type()));
  }

  for (auto i = stencil->scalar_decls().begin(); i != stencil->scalar_decls().end(); i++) {
	  auto variable_name = isSgVariableDeclaration(*i)->get_variables().front()->unparseToString();
	  signature_params.push_back(SageBuilder::buildInitializedName(variable_name, isSgVariableDeclaration(*i)->get_decl_item(variable_name)->get_type()));
  }

  for (auto i = decomposer->subdomain_variables().begin(); i != decomposer->subdomain_variables().end(); i++) {
    auto variable_name = isSgVariableDeclaration(*i)->get_variables().front()->unparseToString();
    signature_params.push_back(SageBuilder::buildInitializedName(variable_name, isSgVariableDeclaration(*i)->get_decl_item(variable_name)->get_type()));
  }

  signature_params.push_back(SageBuilder::buildInitializedName(direction->p()->name(), SageBuilder::buildIntType()));


  return signature_params;
}

void MPIBoundary::CreateComputePackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Analyzer* analyzer, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) {

  // Push the function body scope on the stack
  SageBuilder::pushScopeStack(isSgScopeStatement(function_declaration->get_definition()->get_body()));

  // Insert j_off and k_off into the body
  auto j_off = this->get_index()->get_joff();
  auto k_off = this->get_index()->get_koff();
  SageInterface::appendStatement(j_off);
  SageInterface::appendStatement(k_off);

  // Insert p variable
  if (direction->IsFront() || direction->IsBack()) {
    SageInterface::appendStatement(this->CreatePackPVariable(direction->p()->name(), direction->boundary_direction(), "k"));
  }

  if (direction->IsNorth() || direction->IsSouth()) {
    SageInterface::appendStatement(this->CreatePackPVariable(direction->p()->name(), direction->boundary_direction(), "j"));
  }

  if (direction->IsEast() || direction->IsWest()) {
    SageInterface::appendStatement(this->CreatePackPVariable(direction->p()->name(), direction->boundary_direction(), "i"));
  }

  for (auto i = analyzer->compute_analyzer()->compute_expression().begin(); i != analyzer->compute_analyzer()->compute_expression().end(); i++) {
	  /* Find the name of the index variable
	  * Create a new idx variable with the same name
	  */
	  auto idx_variable = this->FindIndexVariable(isSgAssignOp(*i));

	  if (idx_variable == NULL) {
		  std::cerr << "Panda error: Could not locate the index variable" << std::endl;
		  exit(1);
	  }

	  auto index3D_variable = this->get_index()->CreateIndex3DVariable(idx_variable->unparseToString(), j_off, k_off);

	  if (!direction->IsFront() && !direction->IsBack()) {

		  // Get the inner most loop, and insert index2D variable
		  // Next, insert the send_buffer

		  SgExprStatement* compute_stmt = SageBuilder::buildExprStatement(SageBuilder::buildVarRefExp(isSgAssignOp(*i)->unparseToString()));
		  SgForStatement* for_loop = this->get_loop_generator()->CreateBoundaryForLoops(direction, compute_stmt, index3D_variable);
		  SgForStatement* inner_most_loop = this->get_loop_generator()->GetInnerMostLoop(for_loop);

		  SgScopeStatement* scope_statement = SageInterface::getScope(inner_most_loop);

		  if (isSgScopeStatement(scope_statement)) {
			  SgStatement* loop_body = SageInterface::getLoopBody(scope_statement);
			  SageBuilder::pushScopeStack(isSgScopeStatement(loop_body));
			  auto index2D_variable = this->get_index()->CreateParallelIndex2DVariable(direction);
			  SageInterface::prependStatement(index2D_variable);
			  SageInterface::appendStatement(this->CreatePackStatement(buffer_manager->send_buffers().front(), analyzer->stencil_analyzer()->get_stencil(), index2D_variable, index3D_variable));
			  SageBuilder::popScopeStack();
		  }

		  SageInterface::appendStatement(for_loop);
	  } else {
		  SgExprStatement* compute_stmt = SageBuilder::buildExprStatement(SageBuilder::buildVarRefExp(isSgAssignOp(*i)->unparseToString()));
		  SageInterface::appendStatement(this->get_loop_generator()->CreateBoundaryForLoops(direction, compute_stmt, index3D_variable));
	  }
  }


  // Pop function body off the stack
  SageBuilder::popScopeStack();
}

SgExprStatement* MPIBoundary::CreatePackStatement(buffer::RootBuffer* send_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) {

  SgExpression* to_buffer_statement = SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(send_buffer->name()), SageBuilder::buildVarRefExp(index2D_variable));
  SgExpression* from_buffer_statement = SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(stencil->write_array_name()), SageBuilder::buildVarRefExp(index3D_variable));

  return SageBuilder::buildAssignStatement(to_buffer_statement, from_buffer_statement);
}

std::vector<SgExprStatement*> MPIBoundary::CreateUnpackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

  //UnpackNorthSouth<real>(unew_sd, north_recv_buffer, nsdx, nsdy, nsdz, 0);

  std::cout << "Panda: Generating Unpack Function Calls...";

  std::vector<SgExprListExp*> expr_list;
  std::vector<variables::RootDirection*> directions;

  for (auto i = direction->directions().begin(); i != direction->directions().end(); i++) {
    variables::RootDirection* direction = (*i);

    if (!direction->IsFront() && !direction->IsBack()) {
      SgExprListExp* expr_list_exp = SageBuilder::buildExprListExp();
      SageInterface::appendExpression(expr_list_exp, stencil->write_array()->get_lhs_operand());
      directions.push_back(direction);
      expr_list.push_back(expr_list_exp);
    }
  }

  for (auto i = expr_list.begin(), j = buffer_manager->recv_buffers().begin(); i != expr_list.end(), j != buffer_manager->recv_buffers().end(); i++, j++) {
    buffer::RootBuffer* buffer = (*j);

    if (isSgVariableDeclaration(buffer->variable()) != NULL) {
      SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildVarRefExp(isSgVariableDeclaration(buffer->variable())));

      for (auto j = decomposer->subdomain_variables().begin(); j != decomposer->subdomain_variables().end(); j++) {
        SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildVarRefExp(isSgVariableDeclaration(*j)));
      }
    }
  }

  // Merge direction names e.g. East + West = EastWest
  this->MergeDirectionNames(direction);

  std::vector<SgExprStatement*> unpack_func_calls;
  for (auto i = expr_list.begin(), j = directions.begin(); i != expr_list.end(), j != directions.end(); i++, j++) {
    variables::RootDirection* direction = (*j);

    if (direction->IsEast() || direction->IsWest()) {
      SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildIntVal((int)direction->p()->value()));
      unpack_func_calls.push_back(SageBuilder::buildFunctionCallStmt(this->unpack_func_name() + direction->merged_direction_name(), SageBuilder::buildVoidType(), isSgExprListExp(*i)));
    }
    if (direction->IsNorth() || direction->IsSouth()) {
      SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildIntVal((int)direction->p()->value()));
      unpack_func_calls.push_back(SageBuilder::buildFunctionCallStmt(this->unpack_func_name() + direction->merged_direction_name(), SageBuilder::buildVoidType(), isSgExprListExp(*i)));
    }
  }

  std::cout << "done" << std::endl;

  return unpack_func_calls;

}

std::vector<SgFunctionDeclaration*> MPIBoundary::CreateUnpackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

  std::cout << "Panda: Generating Unpack Functions...";

  // 1. Create function parameter list
  std::vector<SgFunctionParameterList*> func_parameter_lists;
  std::map<variables::RootDirection*, SgFunctionDeclaration*> direction_func_map;
  for (auto i = direction->directions().begin(); i != direction->directions().end(); i++) {
    variables::RootDirection* direction = (*i);
    if (!direction->IsFront() && !direction->IsBack()) {
      if (!direction->IsSouth() && !direction->IsWest()) {
        func_parameter_lists.push_back(SageBuilder::buildFunctionParameterList());
        direction_func_map[direction] = NULL;
      }
    }
  }

  // 2. Add parameters to the function parameter list
  std::vector<SgInitializedName*> signature_params;
  for (auto i = func_parameter_lists.begin(); i != func_parameter_lists.end(); i++) {
    signature_params = this->BuildUnpackFunctionSignature(buffer_manager, direction, stencil, decomposer);
    for (auto j = signature_params.begin(); j != signature_params.end(); j++) {
      SageInterface::appendArg(isSgFunctionParameterList(*i), isSgInitializedName(*j));
    }
  }

  // 3. Create Unpack function declarations
  std::vector<SgFunctionDeclaration*> unpack_func_declarations;
  for (auto i = func_parameter_lists.begin(), j = direction_func_map.rbegin(); i != func_parameter_lists.end(), j != direction_func_map.rend(); i++, j++) {
    unpack_func_declarations.push_back(SageBuilder::buildDefiningFunctionDeclaration(this->unpack_func_name() + j->first->merged_direction_name(), SageBuilder::buildVoidType(), isSgFunctionParameterList(*i)));
  }

  // 4. Map direction to the function declaration
  //    Fill the body of the function declarations
  for (auto i = direction_func_map.rbegin(), j = unpack_func_declarations.begin(); i != direction_func_map.rend(), j != unpack_func_declarations.end(); i++, j++) {
    direction_func_map[i->first] = isSgFunctionDeclaration(*j);
    this->CreateUnpackFunctionDeclarationBody(buffer_manager, stencil, i->first, i->second);
  }

  // Free
  signature_params.clear();

  std::cout << "done" << std::endl;

  return unpack_func_declarations;
}

std::vector<SgInitializedName*> MPIBoundary::BuildUnpackFunctionSignature(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

  std::vector<SgInitializedName*> signature_params;

  signature_params.push_back(SageBuilder::buildInitializedName(stencil->write_array_name(), stencil->write_array_type()));
  signature_params.push_back(SageBuilder::buildInitializedName(buffer_manager->recv_buffers().front()->name(), stencil->write_array_type()));

  for (auto i = decomposer->subdomain_variables().begin(); i != decomposer->subdomain_variables().end(); i++) {
    auto variable_name = isSgVariableDeclaration(*i)->get_variables().front()->unparseToString();
    signature_params.push_back(SageBuilder::buildInitializedName(variable_name, isSgVariableDeclaration(*i)->get_decl_item(variable_name)->get_type()));
  }

  signature_params.push_back(SageBuilder::buildInitializedName(direction->directions().front()->p()->name(), SageBuilder::buildIntType()));

  return signature_params;
}

void MPIBoundary::CreateUnpackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Stencil* stencil, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) {

  // Push the function body scope on the stack
  SageBuilder::pushScopeStack(isSgScopeStatement(function_declaration->get_definition()->get_body()));

  // Insert j_off and k_off into the body
  auto j_off = this->get_index()->get_joff();
  auto k_off = this->get_index()->get_koff();
  SageInterface::appendStatement(j_off);
  SageInterface::appendStatement(k_off);

  // Insert p variable
  if (direction->IsFront() || direction->IsBack()) {
    SageInterface::appendStatement(this->CreateUnpackPVariable(direction->p()->name(), direction->boundary_direction(), "k"));
  }

  if (direction->IsNorth() || direction->IsSouth()) {
    SageInterface::appendStatement(this->CreateUnpackPVariable(direction->p()->name(), direction->boundary_direction(), "j"));
  }

  if (direction->IsEast() || direction->IsWest()) {
    SageInterface::appendStatement(this->CreateUnpackPVariable(direction->p()->name(), direction->boundary_direction(), "i"));
  }


  auto index3D_variable = this->get_index()->CreateIndex3DVariable("index3D", j_off, k_off);

  if (!direction->IsFront() && !direction->IsBack()) {
	  auto index2D_variable = this->get_index()->CreateParallelIndex2DVariable(direction);

	  SgExprStatement* unpack_stmt = this->CreateUnpackStatement(buffer_manager->recv_buffers().front(), stencil, index2D_variable, index3D_variable);
	  SgForStatement* for_loop = this->get_loop_generator()->CreateBoundaryForLoops(direction, unpack_stmt, index3D_variable);
	  SgForStatement* inner_most_loop = this->get_loop_generator()->GetInnerMostLoop(for_loop);

	  SgScopeStatement* scope_statement = SageInterface::getScope(inner_most_loop);

	  if (isSgScopeStatement(scope_statement)) {
		  SgStatement* loop_body = SageInterface::getLoopBody(scope_statement);
		  SageBuilder::pushScopeStack(isSgScopeStatement(loop_body));
		  auto index2D_variable = this->get_index()->CreateParallelIndex2DVariable(direction);
		  SageInterface::prependStatement(index2D_variable);
		  //SageInterface::appendStatement(unpack_stmt);
		  SageBuilder::popScopeStack();
	  }

	  SageInterface::appendStatement(for_loop);
  }

  // Pop function body off the stack
  SageBuilder::popScopeStack();
}

SgExprStatement* MPIBoundary::CreateUnpackStatement(buffer::RootBuffer* recv_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) {

  SgExpression* to_buffer_statement = SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(recv_buffer->name()), SageBuilder::buildVarRefExp(index2D_variable));
  SgExpression* from_buffer_statement = SageBuilder::buildPntrArrRefExp(SageBuilder::buildVarRefExp(stencil->write_array_name()), SageBuilder::buildVarRefExp(index3D_variable));

  return SageBuilder::buildAssignStatement(from_buffer_statement, to_buffer_statement);
}

} //namespace boundary
