#include "boundary.h"

namespace boundary
{
Boundary::~Boundary() {}

std::vector<SgIfStmt*> Boundary::CreateDirectionIfStatements(variables::Direction* direction) {

  std::vector<SgExpression*> direction_expression_list = direction->CollectDirectionExpressions();

  std::vector<SgIfStmt*> if_stmts;

  for (auto i = direction_expression_list.begin(); i != direction_expression_list.end(); i++) {
    if_stmts.push_back(this->CreateDirectionIfStatement(isSgExpression(*i)));
  }

  return if_stmts;
}

std::vector<SgIfStmt*> Boundary::CreatePackUnpackDirectionIfStatements(variables::Direction* direction) {

  std::vector<SgExpression*> direction_expression_list = direction->CollectDirectionExpressions();
  std::vector<SgIfStmt*> if_stmts;

  for (auto i = direction_expression_list.begin(), j = direction->directions().begin(); i != direction_expression_list.end(), j != direction->directions().end(); i++, j++) {
    variables::RootDirection* direction = (*j);

    if (!direction->IsBack() && !direction->IsFront()) {
      if_stmts.push_back(this->CreateDirectionIfStatement(isSgExpression(*i)));
    }
  }

  return if_stmts;
}

std::vector<SgExprStatement*> Boundary::CreateComputePackFuncCallStmts(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

  // Append write-array to the arg. list
  std::vector<SgExprListExp*> expr_list;

  //ComputeFrontBack<real>(unew_sd, uold_sd, <dir_t_send_buffer>, kC0, kC1, nsdx, nsdy, nsdz, 0);

  for (auto i = direction->directions().begin(); i != direction->directions().end(); i++) {
    SgExprListExp* expr_list_exp = SageBuilder::buildExprListExp();
    SageInterface::appendExpression(expr_list_exp, stencil->write_array()->get_lhs_operand());
    expr_list.push_back(expr_list_exp);
  }

  // Append read-only arrays to the arg. list
  for (auto i = expr_list.begin(); i != expr_list.end(); i++) {
    for (auto j = stencil->read_only_arrays().begin(); j != stencil->read_only_arrays().end(); j++) {
      SageInterface::appendExpression(isSgExprListExp(*i), isSgVarRefExp(*j));
    }
  }

  // Append send_buffers to the arg. list
  // Not needed for front and back
  for (auto i = expr_list.begin(), j = direction->directions().begin(), k = buffer_manager->send_buffers().begin();
       i != expr_list.end(), j != direction->directions().end(), k != buffer_manager->send_buffers().end(); i++, j++, k++) {
    variables::RootDirection* direction = (*j);
    buffer::RootBuffer* buffer = (*k);
    if (direction->IsFront() || direction->IsBack()) {
    } else {
      SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildVarRefExp(buffer->variable()));
    }
  }

  // Append scalars to the arg. list
  for (auto i = expr_list.begin(); i != expr_list.end(); i++) {
    for (auto j = stencil->scalar_decls().begin(); j != stencil->scalar_decls().end(); j++) {
      SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildVarRefExp(isSgVariableDeclaration(*j)));
    }
  }

  // Append subdomain variables to the arg. list
  for (auto i = expr_list.begin(); i != expr_list.end(); i++) {
    for (auto j = decomposer->subdomain_variables().begin(); j != decomposer->subdomain_variables().end(); j++) {
      SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildVarRefExp(isSgVariableDeclaration(*j)));
    }
  }

  // Merge direction names e.g. East + West = EastWest
  this->MergeDirectionNames(direction);

  // Append the direction based p value to the arg. list
  std::vector<SgExprStatement*> compute_pack_func_call_stmt;

  for (auto i = expr_list.begin(), j = direction->directions().begin(); i != expr_list.end(), j != direction->directions().end(); i++, j++) {
    variables::RootDirection* direction = (*j);
    SageInterface::appendExpression(isSgExprListExp(*i), SageBuilder::buildIntVal((int)direction->p()->value()));

    // Make the first letter in the direction name to upper case
//    auto dir_name = direction->name();
 //   dir_name[0] = std::toupper(dir_name[0]);

    compute_pack_func_call_stmt.push_back(SageBuilder::buildFunctionCallStmt(this->pack_func_name() + direction->merged_direction_name(), SageBuilder::buildVoidType(), isSgExprListExp(*i)));
  }

  return compute_pack_func_call_stmt;
}

SgIfStmt* Boundary::CreateDirectionIfStatement(SgExpression* direction_expression) {

  SgExpression* condition_expression = SageBuilder::buildNotEqualOp(direction_expression, SageBuilder::buildIntVal(-1));

  return SageBuilder::buildIfStmt(condition_expression, SageBuilder::buildBasicBlock(), NULL);
}

void Boundary::MergeDirectionNames(variables::Direction* direction) {

  for (auto i = direction->directions().begin(); i != direction->directions().end(); i++) {
    variables::RootDirection* opposite_direction = (*i);

    if (opposite_direction->IsEast()) {
      auto opposite_dir_name = opposite_direction->name();
      opposite_dir_name[0] = std::toupper(opposite_dir_name[0]);

      for (auto j = direction->directions().begin(); j != direction->directions().end(); j++) {
        variables::RootDirection* direction_object = (*j);

        if (direction_object->IsWest()) {
          auto local_dir_name = direction_object->name();
          local_dir_name[0] = std::toupper(local_dir_name[0]);
          auto function_name = opposite_dir_name + local_dir_name;
          direction_object->set_merged_name(function_name);
          opposite_direction->set_merged_name(function_name);
        }
      }
    }

    if (opposite_direction->IsNorth()) {
      auto opposite_dir_name = opposite_direction->name();
      opposite_dir_name[0] = std::toupper(opposite_dir_name[0]);

      for (auto j = direction->directions().begin(); j != direction->directions().end(); j++) {
        variables::RootDirection* direction_object = (*j);

        if (direction_object->IsSouth()) {
          auto local_dir_name = direction_object->name();
          local_dir_name[0] = std::toupper(local_dir_name[0]);
          auto function_name = opposite_dir_name + local_dir_name;
          direction_object->set_merged_name(function_name);
          opposite_direction->set_merged_name(function_name);
        }
      }
    }

    if (opposite_direction->IsFront()) {
        auto opposite_dir_name = opposite_direction->name();
        opposite_dir_name[0] = std::toupper(opposite_dir_name[0]);

        for (auto j = direction->directions().begin(); j != direction->directions().end(); j++) {
          variables::RootDirection* direction_object = (*j);

          if (direction_object->IsBack()) {
            auto local_dir_name = direction_object->name();
            local_dir_name[0] = std::toupper(local_dir_name[0]);
            auto function_name = opposite_dir_name + local_dir_name;
            direction_object->set_merged_name(function_name);
            opposite_direction->set_merged_name(function_name);
          }
        }
    }
  }
}

SgVariableDeclaration* Boundary::CreatePackPVariable(const std::string& p_name, SgVariableDeclaration* subdomain, const std::string& variable_name) {

  SgExpression* test = SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(p_name), SageBuilder::buildIntVal(1));
  SgExpression* true_test = SageBuilder::buildVarRefExp(subdomain);
  SgExpression* false_test = SageBuilder::buildIntVal(1);

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  return SageBuilder::buildVariableDeclaration(variable_name, SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(conditional_expression));
}

SgVariableDeclaration* Boundary::CreateUnpackPVariable(const std::string& p_name, SgVariableDeclaration* subdomain, const std::string& variable_name) {

  SgExpression* test = SageBuilder::buildLessThanOp(SageBuilder::buildVarRefExp(p_name), SageBuilder::buildIntVal(1));
  SgExpression* true_test = SageBuilder::buildAddOp(SageBuilder::buildVarRefExp(subdomain), SageBuilder::buildIntVal(1));
  SgExpression* false_test = SageBuilder::buildIntVal(0);

  SgExpression* conditional_expression = SageBuilder::buildConditionalExp(test, true_test, false_test);

  return SageBuilder::buildVariableDeclaration(variable_name, SageBuilder::buildIntType(), SageBuilder::buildAssignInitializer(conditional_expression));
}

SgVarRefExp* Boundary::FindIndexVariable(SgAssignOp* assign_op) {

	SgVarRefExp* index_variable;

	if (assign_op->get_lhs_operand() != NULL) {
		if (assign_op->get_lhs_operand()->variantT() == V_SgPntrArrRefExp) {
			if (isSgPntrArrRefExp(assign_op->get_lhs_operand())->get_rhs_operand() != NULL) {
				if (isSgPntrArrRefExp(assign_op->get_lhs_operand())->get_rhs_operand()->variantT() == V_SgVarRefExp) {

					index_variable = isSgVarRefExp(isSgPntrArrRefExp(assign_op->get_lhs_operand())->get_rhs_operand());
				}
			}
		}
	}

	return index_variable;
}
} //namespace
