#ifndef PANDA_BOUNDARY_HYBRID_BOUNDARY_H
#define PANDA_BOUNDARY_HYBRID_BOUNDARY_H

#include "boundary.h"

namespace boundary
{
  class HybridBoundary: public Boundary {
    public:

	// Pack functions
	std::vector<SgExprStatement*> CreateComputePackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer);
	std::vector<SgFunctionDeclaration*> CreateComputePackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Analyzer* stencil, domain::Decomposer* decomposer);
	void CreateComputePackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Analyzer* analyzer, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration);
	std::vector<SgInitializedName*> BuildComputePackFunctionSignature(buffer::BufferManager* buffer_manager, variables::RootDirection* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer);
	SgExprStatement* CreatePackStatement(buffer::RootBuffer* send_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable);

	// Unpack functions
	std::vector<SgExprStatement*> CreateUnpackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer);
	std::vector<SgInitializedName*> BuildUnpackFunctionSignature(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer);
	std::vector<SgFunctionDeclaration*> CreateUnpackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer);
	void CreateUnpackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Stencil* stencil, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration);
	SgExprStatement* CreateUnpackStatement(buffer::RootBuffer* recv_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable);

      ~HybridBoundary() {}
  };
} // namespace boundary

#endif //PANDA_BOUNDARY_HYBRID_BOUNDARY_H
