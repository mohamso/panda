#ifndef PANDA_BOUNDARY_BOUNDARY_H
#define PANDA_BOUNDARY_BOUNDARY_H

#include "../panda.h"
#include <algorithm>
#include <map>
#include <cctype>
#include "../memory/buffer_manager.h"
#include "../domain/decomposer.h"
#include "../analyzer/analyzer.h"
#include "../loop/loop_generator.h"
#include "../variables/direction.h"
#include "../variables/index.h"

namespace boundary
{
class Boundary {
	public:

	  Boundary() {
		  this->index_ = new variables::Index();
		  this->loop_generator_ = new loop::LoopGenerator();
	  }

	  virtual variables::Index* get_index() const { return index_; }
	  virtual loop::LoopGenerator* get_loop_generator() const { return loop_generator_; }

	  // Pack functions
	  virtual std::vector<SgExprStatement*> CreateComputePackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* analyzer, domain::Decomposer* decomposer) = 0;
	  virtual std::vector<SgFunctionDeclaration*> CreateComputePackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Analyzer* stencil, domain::Decomposer* decomposer) = 0;
	  virtual void CreateComputePackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Analyzer* analyzer, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) = 0;
	  virtual std::vector<SgInitializedName*> BuildComputePackFunctionSignature(buffer::BufferManager* buffer_manager, variables::RootDirection* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) = 0;
	  virtual SgExprStatement* CreatePackStatement(buffer::RootBuffer* send_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) = 0;

	  // Unpack functions
	  virtual std::vector<SgExprStatement*> CreateUnpackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) = 0;
	  virtual std::vector<SgInitializedName*> BuildUnpackFunctionSignature(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) = 0;
	  virtual std::vector<SgFunctionDeclaration*> CreateUnpackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) = 0;
	  virtual void CreateUnpackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Stencil* stencil, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) = 0;
	  virtual SgExprStatement* CreateUnpackStatement(buffer::RootBuffer* recv_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) = 0;

	  virtual void MergeDirectionNames(variables::Direction* direction);

	  virtual std::string pack_func_name() const { return "ComputePack"; }
	  virtual std::string unpack_func_name() const { return "Unpack"; }

	  virtual std::vector<SgExprStatement*> CreateComputePackFuncCallStmts(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer);
	  virtual std::vector<SgIfStmt*> CreateDirectionIfStatements(variables::Direction* direction);
	  virtual std::vector<SgIfStmt*> CreatePackUnpackDirectionIfStatements(variables::Direction* direction);
	  virtual SgIfStmt* CreateDirectionIfStatement(SgExpression* direction_expression);

	  virtual SgVariableDeclaration* CreatePackPVariable(const std::string& p_name, SgVariableDeclaration* subdomain, const std::string& variable_name);
	  virtual SgVariableDeclaration* CreateUnpackPVariable(const std::string& p_name, SgVariableDeclaration* subdomain, const std::string& variable_name);

	  virtual SgVarRefExp* FindIndexVariable(SgAssignOp* assign_op);

	  virtual ~Boundary();

	private:
	  variables::Index* index_;
	  loop::LoopGenerator* loop_generator_;

  };

} // namespace boundary

#endif //PANDA_BOUNDARY_BOUNDARY_H
