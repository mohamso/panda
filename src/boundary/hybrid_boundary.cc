#include "hybrid_boundary.h"

namespace boundary
{

std::vector<SgExprStatement*> HybridBoundary::CreateComputePackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

std::vector<SgFunctionDeclaration*> HybridBoundary::CreateComputePackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Analyzer* stencil, domain::Decomposer* decomposer) {

	std::cout << "Panda: Generating Compute Pack Functions...";

	std::vector<SgFunctionDeclaration*> compute_pack_func_decls;

	//1. Create function parameter list

	// 2. Add paramters to the function parameter list

	// 3. Create compute pack function declarations

	// 4. Map direction to the function declaration
	//    Fill the body of the function declarations


	std::cout << "done" << std::endl;

	return compute_pack_func_decls;
}

std::vector<SgInitializedName*> HybridBoundary::BuildComputePackFunctionSignature(buffer::BufferManager* buffer_manager, variables::RootDirection* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

void HybridBoundary::CreateComputePackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Analyzer* analyzer, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) {

}

SgExprStatement* HybridBoundary::CreatePackStatement(buffer::RootBuffer* send_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) {

}


std::vector<SgExprStatement*> HybridBoundary::CreateUnpackFunctionCalls(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

std::vector<SgFunctionDeclaration*> HybridBoundary::CreateUnpackFunctions(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

std::vector<SgInitializedName*> HybridBoundary::BuildUnpackFunctionSignature(buffer::BufferManager* buffer_manager, variables::Direction* direction, analyzer::Stencil* stencil, domain::Decomposer* decomposer) {

}

void HybridBoundary::CreateUnpackFunctionDeclarationBody(buffer::BufferManager* buffer_manager, analyzer::Stencil* stencil, variables::RootDirection* direction, SgFunctionDeclaration* function_declaration) {

}

SgExprStatement* HybridBoundary::CreateUnpackStatement(buffer::RootBuffer* recv_buffer, analyzer::Stencil* stencil, SgVariableDeclaration* index2D_variable, SgVariableDeclaration* index3D_variable) {

}

}
