#include "pragma_manager.h"

namespace translator
{

std::vector<SgPragmaDeclaration*> PragmaManager::CollectPragmaDeclarations(SgProject* project) {

	std::vector<SgPragmaDeclaration*> declaration_list;

	Rose_STL_Container<SgNode*> pragma_declarations = NodeQuery::querySubTree(project, V_SgPragmaDeclaration);

	for (auto i = pragma_declarations.begin(); i != pragma_declarations.end(); i++) {
		declaration_list.push_back(isSgPragmaDeclaration(*i));
	}

	return declaration_list;
}

std::vector<std::string> PragmaManager::CollectPragmaDeclarationStrings() {

	std::vector<std::string> pragma_string_list;

	for (auto i = this->pragma_declaration_list().begin(); i != this->pragma_declaration_list().end(); i++) {
		pragma_string_list.push_back(isSgPragmaDeclaration(*i)->get_pragma()->get_pragma());
	}

	return pragma_string_list;
}

void PragmaManager::ValidateAllPragmas() {

	std::vector<std::string> pragma_string_list = this->pragma_declaration_string_list();

	bool IsDecomposePragmaPresent = false;

	for (auto i = pragma_string_list.begin(); i != pragma_string_list.end(); i++) {

		IsDecomposePragmaPresent = (((*i).find(this->decompose()) != std::string::npos));

		if (IsDecomposePragmaPresent) {
			break;
		}
	}

	if (!IsDecomposePragmaPresent) {
			std::cerr << "Panda error: Could Not Find " << this->decompose() << std::endl;
			std::cerr << "Panda error: Translation will abort." << std::endl;
			exit(1);
	  }

	// Remove Decompose Pragma From The List
	for(auto i = pragma_string_list.begin(); i != pragma_string_list.end();) {

		bool IsPresent = (((*i).find(this->decompose()) != std::string::npos));

		if (IsPresent) {
			pragma_string_list.erase(i);
		} else {
			i++;
		}
	}

	// Check for Pragma Distributed and Pragma Initialize
	std::vector<std::string> correct_pragma_list = this->pragma_list();

	for (auto i = correct_pragma_list.begin(); i != correct_pragma_list.end(); i++) {

		bool IsPresent = (std::find(pragma_string_list.begin(), pragma_string_list.end(), (*i)) != pragma_string_list.end());

		if (!IsPresent) {
			std::cerr << "Panda error: Could Not Find " << (*i) << std::endl;
			std::cerr << "Panda error: Translation will abort." << std::endl;
			exit(1);
		}
	}
}

std::vector<std::string> PragmaManager::ValidateDecomposeClause(SgProject* project) {

	std::vector<std::string> decompose_pragma_list = this->CollectDecomposePragma();

	std::vector<std::string> clause_list = this->CollectDecomposeClause(decompose_pragma_list);

	if (clause_list.size() == 0) {
		std::cerr << "Panda error: Domain Clause Is Empty" << std::endl;
		std::cerr << "Panda error: Translation will abort." << std::endl;
		exit(1);
	}

	// Check if the domains in the clause list exists in the document
	Rose_STL_Container<SgNode*> domain_name_declaration = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	// Convert gathers Variable Declarations Names To String
	std::vector<std::string> target_list;

	for (auto i = domain_name_declaration.begin(); i != domain_name_declaration.end(); i++) {
		target_list.push_back(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString());
	}

	for(auto i = clause_list.begin(); i != clause_list.end(); i++) {
		bool IsPresent = (std::find(target_list.begin(), target_list.end(), (*i)) != target_list.end());

		if(!IsPresent) {
			std::cerr << "Panda error: Domain Name(s) Specified In The Decompose Clause Does Not Exist" << std::endl;
			std::cerr << "Panda error: Translation will abort." << std::endl;
			exit(1);
		}
	}

	return clause_list;
}

std::vector<std::string> PragmaManager::ValidateDomainSizeClause(SgProject* project) {

	std::vector<std::string> decompose_pragma_list = this->CollectDecomposePragma();
	std::vector<std::string> clause_list = this->CollectDomainSizeClause(decompose_pragma_list);

	if (clause_list.size() == 0) {
		std::cerr << "Panda error: Domain Size Clause Is Empty" << std::endl;
		std::cerr << "Panda error: Translation will abort." << std::endl;
		exit(1);
	}

	// Check if the domain sizes specified in the clause list exists in the document
	Rose_STL_Container<SgNode*> domain_size_declaration = NodeQuery::querySubTree(project, V_SgVariableDeclaration);

	// Convert gathers Variable Declarations Names To String
	std::vector<std::string> target_list;

	for (auto i = domain_size_declaration.begin(); i != domain_size_declaration.end(); i++) {
		target_list.push_back(isSgVariableDeclaration(*i)->get_variables().front()->unparseToString());
	}

	// Look If the specified domain sizes exists in the domain
	for(auto i = clause_list.begin(); i != clause_list.end(); i++) {
		bool IsPresent = (std::find(target_list.begin(), target_list.end(), (*i)) != target_list.end());

		if (!IsPresent) {
			std::cerr << "Panda error: Variables Specified In The Domain Size Clause Does Not Exist" << std::endl;
			std::cerr << "Panda error: Translation will abort." << std::endl;
			exit(1);
		}
	}

	return clause_list;
}

std::vector<std::string> PragmaManager::CollectDecomposeClause(std::vector<std::string> decompose_pragma_list) {

	std::string decompose_pattern = "pandadecompose\\((\\D.*?)\\)";
	std::vector<std::string> clause_list;

	for (auto i = decompose_pragma_list.begin(); i != decompose_pragma_list.end(); i++) {
		std::string decompose_clause = (*i);

		decompose_clause.erase(std::remove(decompose_clause.begin(), decompose_clause.end(), ' '), decompose_clause.end());
		boost::cmatch decompose_match = Regex::PragmaClauseMatcher(decompose_clause, decompose_pattern);

		std::string domain_string = decompose_match[1];

		boost::split(clause_list, domain_string, boost::is_any_of(", "), boost::token_compress_on);
	}

	return clause_list;
}

std::vector<std::string> PragmaManager::CollectDomainSizeClause(std::vector<std::string> decompose_pragma_list) {

	std::string decompose_pattern = "domainsize\\((\\D.*?)\\)";

	std::vector<std::string> clause_list;

	for (auto i = decompose_pragma_list.begin(); i != decompose_pragma_list.end(); i++) {
		std::string decompose_clause = (*i);

		decompose_clause.erase(std::remove(decompose_clause.begin(), decompose_clause.end(), ' '), decompose_clause.end());
		boost::cmatch decompose_match = Regex::PragmaClauseMatcher(decompose_clause, decompose_pattern);

		std::string domain_size_string = decompose_match[1];

		boost::split(clause_list, domain_size_string, boost::is_any_of(", "), boost::token_compress_on);
	}

	return clause_list;
}

std::vector<std::string> PragmaManager::CollectDecomposePragma() {

	std::vector<std::string> decompose_pragma_list;

	for (auto i = this->pragma_declaration_string_list().begin(); i != this->pragma_declaration_string_list().end(); i++) {

		if ((((*i).find(this->decompose()) != std::string::npos))) {
			decompose_pragma_list.push_back((*i));
		}
	}

	return decompose_pragma_list;
}

/**
 * @brief This functions checks if the reduction
 * pragma is present or not.
 *
 */
bool PragmaManager::IsReductionPragmaPresent() {

	bool IsPresent = false;

	for (auto i = this->pragma_declaration_string_list().begin(); i != this->pragma_declaration_string_list().end(); i++) {
		IsPresent = (((*i).find(this->reduction()) != std::string::npos));

		if (IsPresent) {
			break;
		}
	}

	return IsPresent;
}

/**
 * @brief This functions stores all reduction
 * pragmas in the document.
 *
 */
void PragmaManager::StoreReductionPragmas() {

	std::vector<std::string> reduction_pragmas;

	bool IsPresent = false;

	for (auto i = this->pragma_declaration_string_list().begin(); i != this->pragma_declaration_string_list().end(); i++) {
		IsPresent = (((*i).find(this->reduction()) != std::string::npos));

		if (IsPresent) {
			reduction_pragmas.push_back((*i));
		}
	}

	this->set_reduction_pragma_list(reduction_pragmas);
}

/**
 * @brief This functions checks if the reduction clause is empty or not.
 * If the reduction clause is not empty, the clauses are stored.
 *
 * Translation will abort if the reduction clause is empty.
 *
 */
void PragmaManager::ValidateReductionClauseOrDie() {

	std::vector<std::string> clause_list = this->CollectReductionClause(this->reduction_pragmas());

	if (clause_list.size() == 0) {
		std::cerr << "Panda error: Reduction Clause Is Empty" << std::endl;
		std::cerr << "Panda error: Translation will abort." << std::endl;
		exit(1);
	} else {
		this->set_reduction_clause_list(clause_list);
	}
}

std::vector<std::string> PragmaManager::CollectReductionClause(std::vector<std::string> reduction_pragma_list) {

	std::string reduction_pattern = "pandareduction\\((\\D.*?)\\)";
	std::vector<std::string> clause_list;

	for (auto i = reduction_pragma_list.begin(); i != reduction_pragma_list.end(); i++) {
		std::string reduction_clause = (*i);

		reduction_clause.erase(std::remove(reduction_clause.begin(), reduction_clause.end(), ' '), reduction_clause.end());
		boost::cmatch reduction_match = Regex::PragmaClauseMatcher(reduction_clause, reduction_pattern);

		std::string domain_string = reduction_match[1];

		clause_list.push_back(domain_string);
	}

	return clause_list;
}

void PragmaManager::Initialize() {
	this->pragma_list_.push_back("panda initialize");
	this->pragma_list_.push_back("panda distributed");
}

}
