#include "translator_manager.h"

namespace translator
{
void TranslatorManager::Translate() {

  // Use the Pragma Manager To Look for Pragmas
  this->VerifyPragmaDeclarations();

  // Map from pragma clause to Panda Objects (domain objects)
  this->MapPragmaToDomainObjects();

  // Decompose the domain into subdomains
  this->Decompose();

  // Insert variables needed by MPI_Init, MPI_Comm_rank and MPI_comm_size
  this->CreateRequiredMPIFunctions();

  // Map subdomains to their respected ranks
  // i.e. insert rank_x, start_x, nsdx etc.
  this->SubdomainRankMapping();

  // Transform Global Domain
  this->TransformGlobalDomains();

  // Transform Initialize Loops
  this->TransformInitializeLoops();

  // Analyze the compute loop
  this->AnalyzeStencil();

  // Create direction variable and insert mapping directions
  this->CreateDirections();

  // Insert MPI Variables (tag, request, counter)
  this->CreateMPIVariables();

  // Create send/recv buffers
  this->CreateSendAndReceiveBuffers();

  // Transform compute loops
  this->TransformComputeLoops();

  // Create ComputePack function calls
  // Create ComputePack functions
  // Create MPI_Isend function calls
  // Insert function calls into the
  // direction if statements
  this->CreateBoundaryComputeFunctions();

  // Generate and insert MPI_Irecvs
  // and insert it in the time loop
  this->GenerateAndInsertMPIIrecvs();

  // Insert MPI_Waitall synchronization
  this->InsertSynchronization();

  // Create Unpack call functions
  // Create Unpack functions
  // Insert Unpack call functions after
  // the synchronization point
  // Pack functions are generated with the MPI_Isends
  this->CreateUnpackFunctions();

  // Check for Reduction
  this->Reduction();

  // Free allocated memory
  this->FreeAllocatedMemory();
}

void TranslatorManager::VerifyPragmaDeclarations() {

  // Collect Pragma Declarations
  std::vector<SgPragmaDeclaration*> declaration_list = this->pragma_manager()->CollectPragmaDeclarations(this->project());
  this->pragma_manager()->set_pragma_declaration_list(declaration_list);

  // Collect Pragma Declarations as strings
  std::vector<std::string> pragma_string_list = this->pragma_manager()->CollectPragmaDeclarationStrings();
  this->pragma_manager()->set_pragma_declaration_string_list(pragma_string_list);

  // Validate the pragmas
  std::cout << "Panda: Validating pragmas...";
  this->pragma_manager()->ValidateAllPragmas();
  std::cout << "valid pragmas found." << std::endl;

  // Validate decompose pragma clause
  std::cout << "Panda: Validating decompose input...";
  std::vector<std::string> domain_name_list = this->pragma_manager()->ValidateDecomposeClause(this->project());
  this->pragma_manager()->set_domain_name_list(domain_name_list);
  std::cout << "done." << std::endl;

  // Validate domain size clause
  std::cout << "Panda: Validating domainsize input...";
  std::vector<std::string> domain_size_list = this->pragma_manager()->ValidateDomainSizeClause(this->project());
  this->pragma_manager()->set_domain_size_list(domain_size_list);
  std::cout << "done" << std::endl;
}

void TranslatorManager::MapPragmaToDomainObjects() {

  // Find the declarations of the global domain objects in the source code
  std::map<std::string, SgVariableDeclaration*> domain_name_map = this->domain_manager()->MapDomainVariableDeclarations(this->project(), this->pragma_manager());
  this->domain_manager()->set_domain_name_map(domain_name_map);

  // Find Domain Malloc Statements
  std::map<std::string, SgExpression*> domain_malloc_map = this->domain_manager()->MapDomainMallocExpressions(this->project());
  this->domain_manager()->set_domain_malloc_map(domain_malloc_map);

  std::map<std::string, std::vector<SgVarRefExp*>> domain_to_domain_size_map = this->domain_manager()->MapDomainToDomainSize(this->pragma_manager());
  this->domain_manager()->set_domain_to_domain_size_map(domain_to_domain_size_map);

  // Create Global domain declaration variable List
  // should look like this:
  // double *u_new = (double *)(malloc((((sizeof(double ) *(Nx + 2)) *(Ny + 2)) *(Nz + 2))));
  std::vector<SgVariableDeclaration*> domain_variable_list = this->domain_manager()->DomainVariableDeclaration(this->project(), this->pragma_manager());
  this->domain_manager()->set_domain_variables(domain_variable_list);
}

void TranslatorManager::Decompose() {

  // Create px, py, pz (processor geometry variables)
  // these are needed for nsdx, nsdy, nsdz
  this->domain_manager()->decomposer()->InsertGeometryVariables(this->project());

  // Insert statements to read px, py, pz from the command line
  this->domain_manager()->decomposer()->InsertGeometryVariableCommandLineInterface(this->project());

  // Map px to nsdx, py to nsdy, pz to nsdz
  std::map<SgVarRefExp*, SgVariableDeclaration*> domain_size_geometry_map = this->domain_manager()->decomposer()->MapDomainSizeToProcessorGeometry(this->pragma_manager());
  this->domain_manager()->decomposer()->set_domain_size_geometry_map(domain_size_geometry_map);
}

void TranslatorManager::CreateRequiredMPIFunctions() {

  this->mpi_manager()->InsertHeader(this->project());
  this->mpi_manager()->InsertFinalize(this->project());

  this->mpi_manager()->InsertSizeVariable(this->project());
  this->mpi_manager()->InsertRankVariable();

  this->mpi_manager()->InsertMPIInit(this->project());
  this->mpi_manager()->InsertMPICommRank(this->project());
  this->mpi_manager()->InsertMPICommSize(this->project());
}

void TranslatorManager::SubdomainRankMapping() {
  // Insert rank_x, rank_y, rank_z
  this->domain_manager()->decomposer()->InsertRankMappingVariables(this->project(), this->pragma_manager(), this->mpi_manager()->rank_variable());

  // Bind rank_x, rank_y, rank_z with the global domain sizes (Nx, Ny, Nz)
  std::map<SgVariableDeclaration*, std::string> rank_domain_size_map = this->domain_manager()->decomposer()->BindRankMappingVariablesToDomainSize(this->mpi_manager()->rank_variable(), this->pragma_manager());

  // Insert start_x, start_y, start_z
  this->domain_manager()->decomposer()->InsertStartMappingVariables(this->mpi_manager()->rank_variable(), rank_domain_size_map);

  // Insert nsdx, nsdy, nsdz
  std::vector<SgVariableDeclaration*> subdomain_variable_list = this->domain_manager()->decomposer()->InsertSubdomainSizeVariables(this->scope(), this->pragma_manager(), this->mpi_manager()->rank_variable());
  this->domain_manager()->decomposer()->set_subdomain_variables(subdomain_variable_list);

  // Generate j_off and k_off
  SgVariableDeclaration* j_off = this->boundary()->get_index()->GenerateJoffVariables(this->domain_manager()->decomposer());
  this->boundary()->get_index()->set_joff(j_off);

  SgVariableDeclaration* k_off = this->boundary()->get_index()->GenerateKoffVariable(this->domain_manager()->decomposer());
  this->boundary()->get_index()->set_koff(k_off);
}

void TranslatorManager::CreateDirections() {

  // Insert directions before the first domain declaration
  this->mpi_manager()->InsertDirectionVariable(this->domain_manager());

  this->mpi_manager()->InsertDirectionEnumerations(this->project());
}

void TranslatorManager::CreateMPIVariables() {

  this->mpi_manager()->InsertTagVariables(this->mode()->IsHybridMode());

  if (this->mode()->IsHybridMode()) {
    std::cout << "GENERATION OF REQUEST VARIABLE FOR HYBRID MODE NOT IMPLEMENTED YET" << std::endl;
  } else {
    this->mpi_manager()->InsertRequestCountVariable();
    this->mpi_manager()->InsertMPIRequestVariable(this->project());
  }
}

void TranslatorManager::TransformGlobalDomains() {

  // Transform rhs (Nx->nsdx) etc.
  this->domain_manager()->TransformGlobalDomainSize(this->project(), this->pragma_manager());

  // Transform original global names
  this->domain_manager()->TransformGlobalDomain(this->pragma_manager());

  std::vector<std::string> subdomain_name_list = this->domain_manager()->CreateSubdomainNameList(this->pragma_manager());
  this->domain_manager()->set_subdomain_name_list(subdomain_name_list);

  std::vector<SgVariableDeclaration*> subdomain_variables = this->domain_manager()->CollectSubdomainVariables(this->project());
  this->domain_manager()->set_subdomain_variables(subdomain_variables);
}

void TranslatorManager::TransformInitializeLoops() {

  std::cout << "Panda: Transforming initialize loops...";

  std::vector<SgForStatement*> for_loop_list =
    this->loop_manager()->CollectLoopStatements(this->pragma_manager()->pragma_declaration_list(), this->pragma_manager()->initialize());

  this->loop_manager()->initialize_transformer()->set_loop_statements(for_loop_list);

  this->loop_manager()->initialize_transformer()->TransformLoopTest(this->domain_manager()->decomposer());

  this->loop_manager()->initialize_transformer()->TransformLoopBody(this->domain_manager()->decomposer(), this->pragma_manager());

  std::cout << "done" << std::endl;
}

void TranslatorManager::AnalyzeStencil() {

  std::vector<SgForStatement*> for_loop_list = this->loop_manager()->CollectLoopStatements(this->pragma_manager()->pragma_declaration_list(), this->pragma_manager()->distributed());
  this->loop_manager()->compute_transformer()->set_loop_statements(for_loop_list);

  std::map<std::string, int> stencil_rank_map = this->analyzer()->stencil_analyzer()->Analyze(this->loop_manager(), this->domain_manager()->decomposer());
  std::vector<std::pair<int, std::string>> sorted_rank_pair = this->analyzer()->stencil_analyzer()->SortStencilRankMap(stencil_rank_map);

  std::cout << "Panda: Determining stencil reach...";
  int stencil_points = this->analyzer()->stencil_analyzer()->DetermineStencilReach(sorted_rank_pair);
  this->analyzer()->stencil_analyzer()->get_stencil()->set_stencil_points(stencil_points);
  std::cout << "done" << std::endl;

  SgVariableDeclaration* target_array = this->analyzer()->stencil_analyzer()->DetermineTargetArray(this->domain_manager(), sorted_rank_pair);
  this->analyzer()->stencil_analyzer()->get_stencil()->set_target_array(target_array);

  this->analyzer()->stencil_analyzer()->CollectReadOnlyArrays(this->loop_manager());

  this->analyzer()->stencil_analyzer()->CollectWriteArray(this->project(), this->loop_manager());

  this->analyzer()->stencil_analyzer()->CollectScalars(this->project(), this->pragma_manager(), this->loop_manager());

  if (stencil_points > 7) {
    std::cout << "Panda error: The current version of Panda supports only 7-point stencils" << std::endl;
    std::cout << "Panda error: Translation will now abort." << std::endl;
    exit(1);
  }
}

void TranslatorManager::CreateSendAndReceiveBuffers() {

  // Create size_t dir<T>_num_bytes expressions
  this->buffer_manager()->CollectNumBytesDeclarations(this->project(), this->domain_manager(), this->mpi_manager()->direction_variable());

  // Insert size_t dir<T>_num_bytes expressions first
  // Next, create and insert send and recv buffers
  this->buffer_manager()->InsertSendRecvBuffers(this->domain_manager());
}

void TranslatorManager::TransformComputeLoops() {

  this->loop_manager()->compute_transformer()->TransformLoopTest(this->domain_manager()->decomposer());
  this->loop_manager()->compute_transformer()->TransformLoopBody(this->domain_manager()->decomposer(), this->pragma_manager());
  this->loop_manager()->compute_transformer()->ComputeInteriorPoints(this->domain_manager()->decomposer());
}

void TranslatorManager::GenerateAndInsertMPIIrecvs() {

  // 1. Create MPI_Irecv calls
  if (this->mode()->IsHybridMode()) {
    std::cout << "GENERATION OF MPI_Irecv FOR HYBRID MODE NOT IMPLEMENTED YET" << std::endl;
  } else {
    this->mpi_manager()->CollectMPIReceiveStatements(this->buffer_manager(), this->domain_manager()->decomposer(), this->analyzer()->stencil_analyzer()->get_stencil());
  }

  /* 2. Create if (direction[<T>] != 1) statements
   *    and insert MPI_Irecv and request_count calls into the
   *    if direction statements
   */
  std::vector<SgIfStmt*> irecv_if_stmts = this->boundary()->CreateDirectionIfStatements(this->mpi_manager()->direction_variable());

  for (auto i = irecv_if_stmts.begin(), j = this->mpi_manager()->mpi_recv_variable()->mpi_recv_statements().begin();
       i != irecv_if_stmts.end(), j != this->mpi_manager()->mpi_recv_variable()->mpi_recv_statements().end(); i++, j++) {
    SgIfStmt* if_stmt = (*i);
    SageInterface::appendStatement(isSgExprStatement(*j), isSgScopeStatement(if_stmt->get_true_body()));
    SageInterface::appendStatement(isSgExprStatement(this->mpi_manager()->request_count_variable()->CreateRequestCountPlusPlusStatement()), isSgScopeStatement(if_stmt->get_true_body()));
  }

  this->mpi_manager()->mpi_recv_variable()->set_mpi_recv_if_stmts(irecv_if_stmts);

  // Get insertion point
  SgExprStatement* insertion_point = this->mpi_manager()->request_count_variable()->statement();

  // Insert if-statements into the outer loop
  SgScopeStatement* scope_statement = SageInterface::getScope(this->loop_manager()->compute_transformer()->OuterMostLoop());

  if (isSgScopeStatement(scope_statement)) {
    SgStatement* loop_body = SageInterface::getLoopBody(scope_statement);
    SageBuilder::pushScopeStack(isSgScopeStatement(loop_body));

    for (auto i = irecv_if_stmts.begin(); i != irecv_if_stmts.end(); i++) {
      SgIfStmt* if_stmt = (*i);
      SageInterface::insertStatementAfter(insertion_point, if_stmt);
    }

    SageBuilder::popScopeStack();
  }
}

void TranslatorManager::CreateBoundaryComputeFunctions() {

  // 1. Create ComputePack function Calls
  std::vector<SgExprStatement*> compute_pack_function_calls = this->boundary()->CreateComputePackFunctionCalls(this->buffer_manager(),
      this->mpi_manager()->direction_variable(), this->analyzer()->stencil_analyzer()->get_stencil(), this->domain_manager()->decomposer());

  // 2. Fetch compute_expression from the inner most loop
  //    needed to generate ComputePack functions
  //    insert functions into the source file
  std::vector<SgAssignOp*> compute_expression_list = this->analyzer()->compute_analyzer()->Analyze(this->loop_manager());
  this->analyzer()->compute_analyzer()->set_compute_expression(compute_expression_list);

  std::vector<SgFunctionDeclaration*> compute_pack_functions = this->boundary()->CreateComputePackFunctions(this->buffer_manager(), this->mpi_manager()->direction_variable(), this->analyzer(), this->domain_manager()->decomposer());

  // 3. Insert boundary compute functions and function calls
  this->InsertBoundaryComputePackFunctions(compute_pack_functions, compute_pack_function_calls);
}

void TranslatorManager::InsertBoundaryComputePackFunctions(std::vector<SgFunctionDeclaration*> compute_pack_funcs, std::vector<SgExprStatement*> compute_pack_func_calls) {

  // 1. Insert Pack functions
  std::cout << "Panda: Inserting compute pack functions...";
  SgGlobal* global_scope = SageInterface::getFirstGlobalScope(this->project());
  SageBuilder::pushScopeStack(isSgScopeStatement(global_scope));

  for (auto i = compute_pack_funcs.begin(); i != compute_pack_funcs.end(); i++) {
    SageInterface::insertStatementBefore(SageInterface::findMain(this->project()), isSgFunctionDeclaration(*i));
  }
  std::cout << "done" << std::endl;

  std::cout << "Panda: Inserting compute function calls...";

  /* 2. Generate direction if-statements and insert
   *    ComputePack function calls into the if-statements
   */
  std::vector<SgIfStmt*> isend_if_stmts = this->boundary()->CreateDirectionIfStatements(this->mpi_manager()->direction_variable());
  for (auto i = isend_if_stmts.begin(), j = compute_pack_func_calls.begin(); i != isend_if_stmts.end(), j != compute_pack_func_calls.end(); i++, j++) {
    SageInterface::appendStatement(isSgExprStatement(*j), isSgScopeStatement(isSgIfStmt(*i)->get_true_body()));
  }

  std::cout << "done" << std::endl;

  /* 3. Generate MPI_Isends function calls and request_count++
   *    and insert these into the direction if-statements
   */
  if (this->mode()->IsHybridMode()) {
    std::cout << "GENERATION OF MPI_Isend FOR HYBRID MODE NOT IMPLEMENTED YET" << std::endl;
  } else {
    this->mpi_manager()->CollectMPIISendStatements(this->buffer_manager(), this->domain_manager()->decomposer(), this->analyzer()->stencil_analyzer()->get_stencil());
  }

  /* 4. Insert the if-statements containing compute function calls
   *    MPI_Isends, and request_count++ into the document
   */
  for (auto i = isend_if_stmts.begin(), j = this->mpi_manager()->mpi_send_variable()->mpi_send_statements().begin();
       i != isend_if_stmts.end(), j != this->mpi_manager()->mpi_send_variable()->mpi_send_statements().end(); i++, j++) {
    SageInterface::appendStatement(isSgExprStatement(*j), isSgScopeStatement(isSgIfStmt(*i)->get_true_body()));
    SageInterface::appendStatement(isSgExprStatement(this->mpi_manager()->request_count_variable()->CreateRequestCountPlusPlusStatement()), isSgScopeStatement(isSgIfStmt(*i)->get_true_body()));
  }

  /* 5. Insert request_count = 0; in the outer most loop
   *    This defines the insertion point.
   */
  this->mpi_manager()->InsertRequestCountStatement(this->loop_manager()->compute_transformer()->OuterMostLoop());
  SgExprStatement* insertion_point = this->mpi_manager()->request_count_variable()->statement();
  SgScopeStatement* scope_statement = SageInterface::getScope(this->loop_manager()->compute_transformer()->OuterMostLoop());

  if (isSgScopeStatement(scope_statement)) {
    SgStatement* loop_body = SageInterface::getLoopBody(scope_statement);
    SageBuilder::pushScopeStack(isSgScopeStatement(loop_body));

    for (auto i = isend_if_stmts.begin(); i != isend_if_stmts.end(); i++) {
      SageInterface::insertStatementAfter(insertion_point, isSgIfStmt(*i));
    }

    SageBuilder::popScopeStack();
  }
}

void TranslatorManager::InsertSynchronization() {

  SgStatement* mpi_waitall_stmt;

  if (this->mode()->IsHybridMode()) {
    std::cout << "SYNCHRONIZATION NOT YET IMPLEMENTED" << std::endl;
  } else {
    mpi_waitall_stmt = this->mpi_manager()->mpi_sync_variable()->CreateMPISynchronizeStatement
                       (this->mpi_manager()->request_count_variable()->variable(), this->mpi_manager()->mpi_request_variable()->variable());
  }

  // Get insertion point
  std::cout << "Panda: Inserting MPI_Waitall...";
  std::vector<SgForStatement*> compute_loop_list = this->loop_manager()->compute_transformer()->stencil_loops();
  SageInterface::insertStatementAfter(compute_loop_list.front(), mpi_waitall_stmt);
  this->mpi_manager()->mpi_sync_variable()->set_statement(mpi_waitall_stmt);

  std::cout << "done" << std::endl;
}

void TranslatorManager::CreateUnpackFunctions() {

  // 1. Create Unpack function calls
  std::vector<SgExprStatement*> unpack_func_calls = this->boundary()->CreateUnpackFunctionCalls(this->buffer_manager(),
      this->mpi_manager()->direction_variable(), this->analyzer()->stencil_analyzer()->get_stencil(), this->domain_manager()->decomposer());

  // 2. Create Unpack functions
  //    The generation of the function calls triggers the
  //    generation of the actual function
  std::vector<SgFunctionDeclaration*> unpack_functions = this->boundary()->CreateUnpackFunctions(this->buffer_manager(), this->mpi_manager()->direction_variable(), this->analyzer()->stencil_analyzer()->get_stencil(), this->domain_manager()->decomposer());

  // 3. Insert unpack functions and function calls
  this->InsertUnpackFunctions(unpack_functions, unpack_func_calls);
}

void TranslatorManager::InsertUnpackFunctions(std::vector<SgFunctionDeclaration*> unpack_functions, std::vector<SgExprStatement*> unpack_func_calls) {

  // 1. Insert Unpack functions
  std::cout << "Panda: Inserting Unpack functions...";
  SgGlobal* global_scope = SageInterface::getFirstGlobalScope(this->project());
  SageBuilder::pushScopeStack(isSgScopeStatement(global_scope));

  for (auto i = unpack_functions.begin(); i != unpack_functions.end(); i++) {
    SageInterface::insertStatementBefore(SageInterface::findMain(this->project()), isSgFunctionDeclaration(*i));
  }

  std::cout << "done" << std::endl;

  // 2. Insert Unpack function calls
  std::vector<SgIfStmt*> unpack_if_stmts = this->boundary()->CreatePackUnpackDirectionIfStatements(this->mpi_manager()->direction_variable());

  // 3. Insert function calls into if-statements
  for (auto i = unpack_if_stmts.begin(), j = unpack_func_calls.begin(); i != unpack_if_stmts.end(), j != unpack_func_calls.end(); i++, j++) {
    SgIfStmt* if_stmt = (*i);
    SageInterface::appendStatement(isSgExprStatement(*j), isSgScopeStatement(if_stmt->get_true_body()));
  }

  // 4. Insert if-statements after the last MPI_Waitall
  // Communication optimization: insert east, west, north, south
  // This is only relevant for GPU and Hybrid code (does not matter for pure MPI)
  SgStatement* insertion_point = this->mpi_manager()->mpi_sync_variable()->statement();

  std::cout << "Panda: Inserting Unpack function calls...";

  SgScopeStatement* scope_statement = SageInterface::getScope(this->loop_manager()->compute_transformer()->OuterMostLoop());

  if (isSgScopeStatement(scope_statement)) {
    SgStatement* loop_body = SageInterface::getLoopBody(scope_statement);
    SageBuilder::pushScopeStack(isSgScopeStatement(loop_body));

    for (auto i = unpack_if_stmts.rbegin(); i != unpack_if_stmts.rend(); i++) {
      SgIfStmt* if_stmt = (*i);
      SageInterface::insertStatementAfter(insertion_point, if_stmt);
    }

    SageBuilder::popScopeStack();
  }

  std::cout << "done" << std::endl;
}

/**
 * @brief A function that checks if the reduction pragma is present in the
 * document. If it is, then the clause information is validated.
 * Upon successful validation, the Reduction class is called to extract
 * clause information, which is needed in order to perform
 * correct source-to-source transformations.
 */
void TranslatorManager::Reduction() {

	if (this->pragma_manager()->IsReductionPragmaPresent()) {

		std::cout << "Panda: Found Reduction Pragma" << std::endl;

		this->pragma_manager()->StoreReductionPragmas();
		this->pragma_manager()->ValidateReductionClauseOrDie();

		this->reduction()->ExtractOperatorOrDie(this->pragma_manager()->reduction_clause_list());
		this->reduction()->ExtractListOrDie(this->pragma_manager()->reduction_clause_list());
		this->reduction()->VerifyExtractedVariablesOrDie(this->project());
		this->reduction()->VerifyPresenceOfExtractedVariablesOrDie(this->loop_manager(), this->pragma_manager());

		this->reduction()->PerformReductionTransformations(this->project(), this->loop_manager(), this->domain_manager()->decomposer(), this->pragma_manager());
  }
}

/**
 * @brief A function that calls the buffer manager whom is responsible
 * for freeing up generated buffers. Typically, this means send/recv
 * buffers on the host and the device.
 *
 */
void TranslatorManager::FreeAllocatedMemory() {

  if (this->mode()->IsMPIMode()) {
	  this->buffer_manager()->FreeHostBuffer(this->mpi_manager()->mpi_finalize_variable(), this->scope());
  }

  if (this->mode()->IsGPUMode()) {
	  std::cerr << "Panda: Freeing of GPU buffers not yet implemented." << std::endl;
  }

  if (this->mode()->IsHybridMode()) {
	  std::cerr << "Panda: Free not GPU and CPU buffers not yet implemented." << std::endl;
  }
}

void TranslatorManager::Initialize(SgProject* project, SgScopeStatement* scope, TranslatorMode* mode) {
  this->set_project(project);
  this->set_scope(scope);
  this->set_mpi_manager(new mpi::MPIManager());
  this->set_domain_manager(new domain::DomainManager());
  this->set_pragma_manager(new PragmaManager());
  this->set_buffer_manager(new buffer::BufferManager());
  this->set_loop_manager(new loop::LoopManager());
  this->set_analyzer(new analyzer::Analyzer());
  this->set_reduction(new reduction::Reduction());
  this->set_mode(mode);

  if (mode->IsMPIMode()) {
    this->set_boundary(new boundary::MPIBoundary());
  }

  if (mode->IsGPUMode()) {
    this->set_boundary(new boundary::GPUBoundary());
  }

  if (mode->IsHybridMode()) {
    this->set_boundary(new boundary::HybridBoundary());
  }
}

}
