#ifndef PANDA_TRANSLATOR_PRAGMA_MANAGER_H
#define PANDA_TRANSLATOR_PRAGMA_MANAGER_H

#include "../panda.h"
#include "regex.h"
#include <map>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/predicate.hpp>

namespace translator
{
class PragmaManager : public AstAttribute {
public:
	PragmaManager() { this->Initialize(); }

	// Getters
	std::string distributed() const { return "panda distributed"; }
	std::string initialize() const { return "panda initialize"; }
	std::string decompose() const { return "panda decompose"; }
	std::string reduction() const { return "panda reduction"; }

	std::vector<std::string> const &pragma_list() const { return pragma_list_; }
	std::vector<std::string> const &domain_name_list() const { return domain_name_list_; }
	std::vector<std::string> const &domain_size_list() const { return domain_size_list_; }
	std::vector<std::string> const &reduction_pragmas() const { return reduction_pragma_list_; }
	std::vector<std::string> const &reduction_clause_list() const { return reduction_clause_list_; }

	std::vector<SgPragmaDeclaration*> const &pragma_declaration_list() const { return pragma_declaration_list_; }
	std::vector<std::string> const &pragma_declaration_string_list() const { return pragma_declaration_string_list_; }

	// Setters
	void set_domain_name_list(const std::vector<std::string> &value) { domain_name_list_ = value; }
	void set_domain_size_list(const std::vector<std::string> &value) { domain_size_list_ = value; }
	void set_reduction_pragma_list(const std::vector<std::string> &value) { reduction_pragma_list_ = value; }
	void set_reduction_clause_list(const std::vector<std::string> &value) { reduction_clause_list_ = value; }
	void set_pragma_declaration_list(const std::vector<SgPragmaDeclaration*> &value) { pragma_declaration_list_ = value; }
	void set_pragma_declaration_string_list(const std::vector<std::string> &value) { pragma_declaration_string_list_ = value; }

	// Functions
	std::vector<SgPragmaDeclaration*> CollectPragmaDeclarations(SgProject* project);
	std::vector<std::string> CollectPragmaDeclarationStrings();

	void ValidateAllPragmas();
	std::vector<std::string> ValidateDecomposeClause(SgProject* project);
	std::vector<std::string> ValidateDomainSizeClause(SgProject* project);

	bool IsReductionPragmaPresent();
	void ValidateReductionClauseOrDie();
	std::vector<std::string> CollectReductionClause(std::vector<std::string> reduction_pragma_list);
	void StoreReductionPragmas();

	~PragmaManager() {}

private:
	void Initialize();

	std::vector<std::string> CollectDecomposePragma();
	std::vector<std::string> CollectDecomposeClause(std::vector<std::string> decompose_pragma_list);
	std::vector<std::string> CollectDomainSizeClause(std::vector<std::string> decompose_pragma_list);

	std::vector<SgPragmaDeclaration*> pragma_declaration_list_;
	std::string decompose_pragma_;
	std::vector<std::string> pragma_declaration_string_list_;
	std::vector<std::string> pragma_list_;
	std::vector<std::string> domain_name_list_;
	std::vector<std::string> domain_size_list_;
	std::vector<std::string> reduction_pragma_list_;
	std::vector<std::string> reduction_clause_list_;

};

} // namespace

#endif // PANDA_TRANSLATOR_PRAGMA_MANAGER_H
