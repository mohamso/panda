#ifndef PANDA_TRANSLATOR_TRANSLATORMANAGER_H
#define PANDA_TRANSLATOR_TRANSLATORMANAGER_H

#include "../panda.h"
#include "pragma_manager.h"
#include "modes.h"

#include "../mpi/mpi_manager.h"
#include "../memory/buffer_manager.h"

#include "../boundary/mpi_boundary.h"
#include "../boundary/gpu_boundary.h"
#include "../boundary/hybrid_boundary.h"

#include "../loop/loop_manager.h"
#include "../analyzer/analyzer.h"

#include "../reduction/reduction.h"

#include <map>

namespace translator
{
  class TranslatorManager {
  public:

    TranslatorManager(SgProject* project, SgScopeStatement* scope, TranslatorMode* mode) { this->Initialize(project, scope, mode); }
    
    // Getter
    SgProject* project() const { return project_; }
    SgScopeStatement* scope() const { return scope_; }
    mpi::MPIManager* mpi_manager() const { return mpi_manager_; }
    domain::DomainManager* domain_manager() const { return domain_manager_; }
    PragmaManager* pragma_manager() const { return pragma_manager_; }
    buffer::BufferManager* buffer_manager() const { return buffer_manager_; }
    boundary::Boundary* boundary() const { return boundary_; }
    loop::LoopManager* loop_manager() const { return loop_manager_; }
    analyzer::Analyzer* analyzer() const { return analyzer_; }
    reduction::Reduction* reduction() const { return reduction_; }
    TranslatorMode* mode() const { return mode_; }

    // Setter
    void set_project(SgProject* project) { project_ = project; }
    void set_scope(SgScopeStatement* scope) { scope_ = scope; }
    void set_mpi_manager(mpi::MPIManager* mpi_manager) { mpi_manager_ = mpi_manager; }
    void set_domain_manager(domain::DomainManager* domain_manager) { domain_manager_ = domain_manager; }
    void set_pragma_manager(PragmaManager* pragma_manager) { pragma_manager_ = pragma_manager; }
    void set_buffer_manager(buffer::BufferManager* buffer_manager) { buffer_manager_ = buffer_manager; }
    void set_loop_manager(loop::LoopManager* loop_manager) { loop_manager_ = loop_manager; }
    void set_analyzer(analyzer::Analyzer* value) { analyzer_ = value; }
    void set_reduction(reduction::Reduction* value) { reduction_ = value; }
    void set_boundary(boundary::Boundary* value) { boundary_ = value; }
    void set_mode(TranslatorMode* value) { mode_ = value; }

    // Functions
    void Translate();

  private:
    SgProject* project_;
    SgScopeStatement* scope_;
    mpi::MPIManager* mpi_manager_;
    domain::DomainManager* domain_manager_;
    PragmaManager* pragma_manager_;
    buffer::BufferManager* buffer_manager_;
    boundary::Boundary* boundary_;
    loop::LoopManager* loop_manager_;
    analyzer::Analyzer* analyzer_;
    reduction::Reduction* reduction_;
    TranslatorMode* mode_;

    void VerifyPragmaDeclarations();
    void MapPragmaToDomainObjects();
    void Initialize(SgProject* project, SgScopeStatement* scope, TranslatorMode* mode);
    void Decompose();
    void CreateRequiredMPIFunctions();
    void SubdomainRankMapping();
    void CreateDirections();
    void CreateSendAndReceiveBuffers();
    void CreateMPIVariables();
    void CreateUnpackFunctions();
    void InsertUnpackFunctions(std::vector<SgFunctionDeclaration*> unpack_functions, std::vector<SgExprStatement*> unpack_func_calls);
    void TransformGlobalDomains();
    void TransformInitializeLoops();
    void AnalyzeStencil();
    void TransformComputeLoops();
    void CreateBoundaryComputeFunctions();
    void InsertBoundaryComputePackFunctions(std::vector<SgFunctionDeclaration*> compute_pack_funcs, std::vector<SgExprStatement*> compute_pack_func_calls);
    void GenerateAndInsertMPIIrecvs();
    void InsertSynchronization();
    void Reduction();
    void FreeAllocatedMemory();
};

} // namespace translator

#endif // PANDA_TRANSLATOR_TRANSLATORMANAGER_H
