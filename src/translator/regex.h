#ifndef PANDA_TRANSLATOR_REGEX_H_
#define PANDA_TRANSLATOR_REGEX_H_

#include "../panda.h"
#include <boost/regex.hpp>

namespace translator
{
  class Regex {
  public:
    static boost::cmatch PragmaClauseMatcher(std::string pragma_string, std::string pragma_regex);   

  };
} // namespace translator

#endif //PANDA_TRANSLATOR_REGEX_H_
