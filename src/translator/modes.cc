#include "modes.h"

namespace translator
{
  void TranslatorMode::VerifyModeOrDie() {

	  if (this->IsMPIMode()) {
		  std::cout << "Panda: Pure MPI mode detected" << std::endl;
		  return;
	  }
	  else if (this->IsGPUMode()) {
		  std::cout << "Panda: GPU-only mode detected" << std::endl;
		  return;
	  }
	  else if (this->IsHybridMode()) {
		  std::cout << "Panda: Hybrid CPU+GPU mode detected" << std::endl;
		  return;
	  } else {
		  std::cout << "Panda: Invalid translation mode specified" << std::endl;
		  exit(1);
	  }
  }
}
