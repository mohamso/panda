#include "regex.h"

namespace translator
{
  boost::cmatch Regex::PragmaClauseMatcher(std::string pragma_string, std::string pragma_regex) {
    boost::regex pattern(pragma_regex);
    boost::cmatch match;
    boost::regex regex_pattern(pattern);
    boost::regex_search(pragma_string.c_str(), match, pattern);
    
    return match;
  }

} //namespace
