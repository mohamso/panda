#ifndef PANDA_TRANSLATOR_TRANSLATION_MODE_H
#define PANDA_TRANSLATOR_TRANSLATION_MODE_H

#include <cstdio>
#include <iostream>
#include <cstdlib>

namespace translator
{
  class TranslatorMode {
  public:

	  TranslatorMode() {
		  this->mpi_ = false;
		  this->gpu_ = false;
		  this->hybrid_ = false;
	  }

	  bool IsMPIMode() { return mpi_; }
	  bool IsGPUMode() { return gpu_; }
	  bool IsHybridMode() { return hybrid_; }

	  void set_mpi_mode(bool value) { this->mpi_ = value; }
	  void set_gpu_mode(bool value) { this->gpu_ = value; }
	  void set_hybrid_mode(bool value) { this->hybrid_ = value; }

	  void VerifyModeOrDie();

    ~TranslatorMode() {};

  private:
    bool mpi_;
    bool gpu_;
    bool hybrid_;
  };
} // namespace translator

#endif //PANDA_TRANSLATOR_TRANSLATION_MODE_H
