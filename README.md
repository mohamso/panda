The Panda Programming Model
===========================
Panda is a domain-specific programming model and translator that generates MPI code from serial C source. By using just a few different Panda pragmas, non-expert programmers can easily decompose and run their code on a cluster of supercomputers.
